Attribute VB_Name = "Module1"
Option Explicit

'#####################################################################################################################################
'# Function name: VDD_0p85_Calib_VB
'# Parameters: globals targets and margins
'# Description: calibrating the vdd 0p85 value
'# High Level Flow: initialize -> run codes till all found -> choose code for NVM and active -> run verify meas
'#####################################################################################################################################
Function VDD_0p85_Calib_VB(argc As Long, argv() As String) As Long

Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Calib_Code_NVM As New SiteDouble
Dim Calib_Voltage_NVM As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim TDO_Data(59) As String
Dim CalibCode_NVM_Found(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim SitesPerNVMCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim ActiveNVMCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long
Dim Code_Number_Var As Variant

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

VDD_0p85_Lookup_Array = Array(2, 1, 14, 15, 12, 13, 8, 26, 25)

ActiveSitesCounter = 0
test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

If Debug_Delay Then
    TheHdw.Wait (0.1)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

'Eden: add VDD DBG

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_ID_0_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_SCAN_VALS.Pat").Run("")
    
If Debug_Delay Then
   TheHdw.Wait (0.1)
Else
   TheHdw.Wait (0.1) 'Ameet change from defult delay to that 22.12.2021
End If

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")
RetVoltages.ResultType = tlResultTypeParametricValue

'##############################################         initialize           ##############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            CalibCode_NVM_Found(site) = False
            Calib_Voltage(site) = 0
            Calib_Voltage_NVM(site) = 0
            Calib_Code(site) = -1
            Calib_Code_NVM(site) = -1
            
        End If
    Next site
End With


'##############################################         VDD_0p85_active_code_ to end          ##############################################

For Each Code_Number_Var In VDD_0p85_Lookup_Array

    Code_Number = CDbl(Code_Number_Var)
    
    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    TheHdw.Wait (0.01) 'Ameet: increase from 0.001 22.12.2021
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.78)
    End If
    
    test_num = test_num + 1

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_GPIO4(site), 0.6, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                If Result(site) >= (VDD_0p85_Target_Volt - VDD_0p85_Lower_Margin) And Result(site) <= (VDD_0p85_Target_Volt + VDD_0p85_Upper_Margin) Then
                    If Abs(VDD_0p85_Target_Volt - Result(site)) < Abs(VDD_0p85_Target_Volt - Calib_Voltage(site)) Then 'Finding the correct code
                        Calib_Voltage(site) = Result(site)
                        Calib_Code(site) = Code_Number
                    End If
                End If
                
                If Result(site) >= (VDD_0p85_NVM_Target_Volt - VDD_0p85_NVM_Lower_Margin) And CalibCode_NVM_Found(site) = False Then
                    CalibCode_NVM_Found(site) = True
                    Calib_Voltage_NVM(site) = Result(site)
                    Calib_Code_NVM(site) = Code_Number
                End If
                
                If (Result(site) - VDD_0p85_Target_Volt) >= 0 Then  'We passed the highest target then calibrated
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number_Var

test_num = test_num + 1

'##############################################         VDD_0p85_active_vdd_trim_verify_nvm_calib_code            ##############################################

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_calc_calib_result_vdd_trim_verify_nvm.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code_NVM(site) <> -1 Then
            indexCode = Calib_Code_NVM(site)
            ActiveNVMCodes(indexCode) = True
            SitesPerNVMCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveNVMCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerNVMCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
                
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(indexCode) & ".Pat").Run("")
        'Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(indexCode) & ".Pat").Run("") 'Eden
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            If Calib_Code_NVM(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "vdd_trim_verify_nvm_calib_code", chan_num_Default, 0, Calib_Code_NVM(site), 26, unitNone, 0, unitNone, 0) 'here to place real value Ameet
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "vdd_trim_verify_nvm_calib_code", chan_num_Default, 0, Calib_Code_NVM(site), 26, unitNone, 0, unitNone, 0) 'here to place real value Ameet
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_vdd_trim_verify_nvm_calib_code = " & CStr(Calib_Code_NVM(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
       End If
   Next site
End With

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'Random code only for delay
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_1.Pat").Run("")

'####################################   vdd_trim_verify_nvm_calib_value     ####################################

If RunVerifyMeas Then
    If Debug_Delay Then
        TheHdw.Wait (0.1)
    Else
        TheHdw.Wait (Defult_Delay_sec)
    End If
            
    If Debug_Delay Then
        TheHdw.Wait (0.1)
    Else
        TheHdw.Wait (0.025) ' Need more than 5msec with 50% margin 10msec worked well loopx20
    End If
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.847)
    End If
    
Else
    Result = Calib_Voltage_NVM
End If

test_num = test_num + 1



With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            'If Result(site) >= (0.77 - 0.04 - 0.005) And Result(site) <= (0.87 + 0.055 + 0.005) Then
            If Abs(Result(site) - Calib_Voltage_NVM(site)) < VDD_0p85_NVM_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "vdd_trim_verify_nvm_calib_value", chan_num_GPIO4(site), 0.77 - 0.035 - 0.005, Result(site), 0.87 + 0.055 + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "vdd_trim_verify_nvm_calib_value", chan_num_GPIO4(site), 0.77 - 0.035 - 0.005, Result(site), 0.87 + 0.055 + 0.005, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_vdd_trim_verify_nvm_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


test_num = test_num + 1

'####################################   vdd_trim_active_calib_code     ####################################

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_calc_calib_result_vdd_trim_active.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
                
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(indexCode) & ".Pat").Run("")
        'Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(indexCode) & ".Pat").Run("") 'Added once after this loop
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
                        
            If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "vdd_trim_active_calib_code", chan_num_Default, 0, Calib_Code(site), 26, unitNone, 0, unitNone, 0) 'here to place real value Ameet
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "vdd_trim_active_calib_code", chan_num_Default, 0, Calib_Code(site), 26, unitNone, 0, unitNone, 0) 'here to place real value Ameet
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_vdd_trim_active_calib_code = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
       End If
   Next site
End With

test_num = test_num + 1

'###################################  Connecting all the TMS and then running the meas pattern for closing the lane outside (Doesn't matter meas of which code to run)    ###################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

If RunVerifyMeas Then

    If Debug_Delay Then
        TheHdw.Wait (0.1)
    Else
        TheHdw.Wait (0.015) ' Need more than 5msec with 50% margin 10msec worked well loopx20
    End If
    
    If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.888)
    End If
    
Else
    Result = Calib_Voltage
End If

'####################################   vdd_trim_active_calib_value     ####################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            'If Result(site) >= (VDD_0p85_Target_Volt - VDD_0p85_Lower_Margin - 0.005) And Result(site) <= (VDD_0p85_Target_Volt + VDD_0p85_Upper_Margin + 0.005) Then
            
            If Abs(Result(site) - Calib_Voltage(site)) < VDD_0p85_Active_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "vdd_trim_active_calib_value", chan_num_GPIO4(site), VDD_0p85_Target_Volt - VDD_0p85_Lower_Margin - 0.005, Result(site), VDD_0p85_Target_Volt + VDD_0p85_Upper_Margin + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "vdd_trim_active_calib_value", chan_num_GPIO4(site), VDD_0p85_Target_Volt - VDD_0p85_Lower_Margin - 0.005, Result(site), VDD_0p85_Target_Volt + VDD_0p85_Upper_Margin + 0.005, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_vdd_trim_active_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_calc_calib_result_vdd_trim_active.Pat").Run("")
'TheHdw.Wait (0.005)

'Measure VDD0.85V with VDD_CAP = 960mV
'Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.94)
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 0.96)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If
'
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_verify_VDD_CAP_960.Pat").Run("")
'TheHdw.Wait (0.005) Ameet commented out 22.12.2021

'#########################################      verify_VDD_CAP_960      #########################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0

Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.777)
End If
    

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
    
            If Result(site) >= VDD_0p85_VerifyVDD_Lower_Target_Volt And Result(site) <= VDD_0p85_VerifyVDD_Upper_Target_Volt Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verify_VDD_CAP_960", chan_num_GPIO4(site), VDD_0p85_VerifyVDD_Lower_Target_Volt, Result(site), VDD_0p85_VerifyVDD_Upper_Target_Volt, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                    
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verify_VDD_CAP_960", chan_num_GPIO4(site), VDD_0p85_VerifyVDD_Lower_Target_Volt, Result(site), VDD_0p85_VerifyVDD_Upper_Target_Volt, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_verify_VDD_CAP_960 = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
                
        End If
    Next site
End With

'Terminate the calibration

'Call ReadRegForcePrint2Datalog("4001001c")
'Call ReadRegForcePrint2Datalog("40060aec")
'Call ReadRegForcePrint2Datalog("40020aec")
'Call ReadRegForcePrint2Datalog("40060b8c")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_CALIB_DONE.Pat").Run("")

test_num = test_num + 1

'#########################################      def_psu_ret_ldo_digital_vdd_trim_idx      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = "" 'Do we really need that?
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 3)
            Else
                TDO_Data(site) = "110"
            End If
                
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
          
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 7 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_psu_ret_ldo_digital_vdd_trim_idx", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_psu_ret_ldo_digital_vdd_trim_idx", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_def_psu_ret_ldo_digital_vdd_trim_idx = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#########################################      def_psu_active_ldo_digital_vdd_trim_idx      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 3, 3)
            Else
                TDO_Data(site) = "111"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 7 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_psu_active_ldo_digital_vdd_trim_idx", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_psu_active_ldo_digital_vdd_trim_idx", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VDD_0p85_active_def_psu_active_ldo_digital_vdd_trim_idx = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: System_Clocks_Calib_VB
'# Parameters: argc As Long, argv() As String
'# Description: calibrating the system clocks - using the given pattern we run the inside calibration then we are reading from the DRAM all the values and compare to limits for pass/fail
'# High Level Flow: Running patterns -> for each test do for all sites -> read DRAM and compare to limits pass/fail -> fails will be declared at the end of test program
'#####################################################################################################################################
Function System_Clocks_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim Calib_Voltage As New SiteDouble
Dim TDO_Data(59) As String
Dim Freq_SOC(59) As Double
Dim Freq_RTC(59) As Double
Dim Freq_WURX30(59) As Double
Dim Freq_WURX100(59) As Double
Dim loop_index As Long

Dim calib_success_val(59) As Double
Dim calib_success_pass(59) As Double

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21
TheHdw.Wait (0.05) ' Tomer:26.1.22 added delay for clock stabalization

Call SitesFailes_Initialize

'Call Thehdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 0.96)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)  'was 0.85changed to 1V due to VDD_DBG sensitivity of this test

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.15)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_ID_10_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_CALIB_DONE.PAT").Run("")

'############################### mss_done ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                 TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else: 'offline tester mode
                TDO_Data(site) = "1"
            End If
                 
            If TDO_Data(site) = "1" Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "mss_done", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                
    
            Else 'TDO_Data(site) = "0"
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "mss_done", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_mss_done = " & CStr(TDO_Data(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'############################### calib_success & calib_success_pass ###############################

test_num = test_num + 1
    
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 5)
            Else
                TDO_Data(site) = "01111"
            End If
            
            calib_success_val(site) = Bin2DecDouble(TDO_Data(site))
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "calib_success", chan_num_TDO(site), 0, calib_success_val(site), 31, unitNone, 0, unitNone, 0)
            
            If TDO_Data(site) = "11111" Or TDO_Data(site) = "01111" Then      'will accept to fail only on the first clock wrting 1 to pass and -1 for fail
                calib_success_pass(site) = 1
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "calib_success_pass", chan_num_Default, 0, 1, 2, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            
            Else 'calib_succes_val != 15/31
                calib_success_pass(site) = 0
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestFail, parmLow, "calib_success_pass", chan_num_Default, 0, -1, 2, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_calib_success_pass = " & CDbl(calib_success_pass(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_calib_success = " & calib_success_val(site) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
            
        End If
    Next site
End With

test_num = test_num + 2

'############################### def_nvm_prog_soc_clk_freq ###############################
     
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
                If TheExec.TesterMode = testModeOnline Then
                    TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 134, 10)
                Else: 'offlineTester mode
                    TDO_Data(site) = "00110110001"
                End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 1023 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_nvm_prog_soc_clk_freq", chan_num_TDO(site), 0, Calib_Code(site), 1023, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_nvm_prog_soc_clk_freq", chan_num_TDO(site), 0, Calib_Code(site), 1023, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_nvm_prog_soc_clk_freq" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'############################### def_ret_soc_clk_div_ctl ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 144, 2)
            Else: 'offlinetester mode
                TDO_Data(site) = "01"
            End If

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_soc_clk_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_soc_clk_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_ret_soc_clk_div_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'############################### def_ret_soc_clk_cap_ctl ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 146, 3)
            Else:
                TDO_Data(site) = "010"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 7 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_soc_clk_cap_ctl", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                
            Else 'Calib_Code(site) is not in [0,7]
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_soc_clk_cap_ctl", chan_num_TDO(site), 0, Calib_Code(site), 7, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_ret_soc_clk_cap_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'############################### def_ret_soc_boot_clk_div_ctl ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 149, 2)
            Else 'offlintester mode
                TDO_Data(site) = "01"
            End If

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 3 Then ' Eden: it was 7 for 2 bit number
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_soc_boot_clk_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_soc_boot_clk_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_ret_soc_boot_clk_div_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1
            
'############################### soc_clk_freq ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 6, 32)
            Else 'offlineTester mode
                TDO_Data(site) = "00000000000011100001000100010011" ' for Offline debug
            End If
            Freq_SOC(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Freq_SOC(site) >= 800000# And Freq_SOC(site) <= 1000000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "soc_clk_freq", chan_num_TDO(site), 800000#, Freq_SOC(site), 1000000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "soc_clk_freq", chan_num_TDO(site), 800000#, Freq_SOC(site), 1000000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_soc_clk_freq = " & CStr(Freq_SOC(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'############################### def_ret_rtc_coarse_ctl ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 151, 4)
            Else
                TDO_Data(site) = "0100"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_rtc_coarse_ctl", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_rtc_coarse_ctl", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_ret_rtc_coarse_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
    
        End If
    Next site
End With

test_num = test_num + 1

'############################### rtc_clk_freq ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 38, 32)
            Else
                TDO_Data(site) = "00000000000000000000010011010011"
            End If
            
            Freq_RTC(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Freq_RTC(site) >= 750# And Freq_RTC(site) <= 1500# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "rtc_clk_freq", chan_num_TDO(site), 750#, Freq_RTC(site), 1500#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "rtc_clk_freq", chan_num_TDO(site), 750#, Freq_RTC(site), 1500#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_rtc_clk_freq = " & CStr(Freq_RTC(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With
        
test_num = test_num + 1
  
'############################### def_ret_nrgdet_adv_clk_coarse ###############################
  
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 155, 4)
            Else
                TDO_Data(site) = "1000"
            
            End If

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_nrgdet_adv_clk_coarse", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_nrgdet_adv_clk_coarse", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_def_ret_nrgdet_adv_clk_coarse = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'############################### ret_nrgdet_adv_clk_coarse ###############################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 159, 4)
            Else
                TDO_Data(site) = "1000"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "ret_nrgdet_adv_clk_coarse", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "ret_nrgdet_adv_clk_coarse", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
                    
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_ret_nrgdet_adv_clk_coarse = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With
        
test_num = test_num + 1

'############################### WURX_adv_30_clk_freq ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 70, 32)
            Else
                TDO_Data(site) = "00000000000000001010011100000110"
            End If
            
            Freq_WURX30(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Freq_WURX30(site) >= 35000# And Freq_WURX30(site) <= 50000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "WURX_adv_30_clk_freq", chan_num_TDO(site), 35000#, Freq_WURX30(site), 50000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "WURX_adv_30_clk_freq", chan_num_TDO(site), 35000#, Freq_WURX30(site), 50000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_WURX_adv_30_clk_freq = " & CStr(Freq_WURX30(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1
            
'############################### WURX_adv_100_clk_freq ###############################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
                
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 102, 32)
            Else
                TDO_Data(site) = "00000000000000011000101110001010"
            End If
            
            Freq_WURX100(site) = CDbl(Bin2Dec(TDO_Data(site)))

            If Freq_WURX100(site) >= 80000# And Freq_WURX100(site) <= 120000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "WURX_adv_100_clk_freq", chan_num_TDO(site), 80000#, Freq_WURX100(site), 120000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "WURX_adv_100_clk_freq", chan_num_TDO(site), 80000#, Freq_WURX100(site), 120000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_WURX_adv_100_clk_freq = " & CStr(Freq_WURX100(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: System_Clocks_Verify_VB                      | in develop
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function System_Clocks_Verify_VB_dev(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long

'Dim RetVoltages As New PinListData
'Dim AVG_samples As New PinListData
'Dim Result As New SiteDouble
Dim test_num As Long
'Dim Calib_Code(59) As Double
'Dim Calib_Voltage As New SiteDouble
'Dim TDO_Data(59) As String
'Dim loop_index As Long

Dim freq_count As Long
Dim num_periods As Double
Dim prog_period As Double
Dim Freq_Value As Double


TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21

'Keep same values as in system clocks
'Call Thehdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 0.96)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)  'was 0.85changed to 1V due to VDD_DBG sensitivity of this test


If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.05)
End If

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_ID_10_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_soc_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_rtc_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_wurx_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_CALIB_DONE.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_ID_10_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_SCAN_VALS.Pat").Run("")

'####################################       Verify SOC CLK        ####################################


Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_soc_clk.Pat").Run("")
'Call theHdw.Digital.FreqCtr.ClearChan(chan_num_GPIO4(0))
'Call theHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_soc_clk.Pat").Run("")
'
'With HPTInfo.Sites
'    For site = 0 To .ExistingCount - 1
'        If .site(site).Active = True Then
'
'            Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
'            'Call theHdw.Digital.Timing.chan(theHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T10", RetPeriod, 1)
'            RetPeriod = 1 / 1200000#
'
'
'
'           ' Call theHdw.Digital.FreqCtr.ClearPinSite("GPIO4_0", 0)
'            'freq_count = theHdw.Digital.FreqCtr.ReadPinSite("GPIO4_0", 0)
'
'            'thehdw.Pins("GPIO4").PPMU.Connect
'            freq_count = theHdw.Digital.FreqCtr.ReadChan(chan_num_GPIO4(site))
'            'freq_count = thehdw.Digital.FreqCtr.ReadPinSite("GPIO4_0", site)
'            num_periods = 3000
'            prog_period = RetPeriod
'            Freq_Value = freq_count / (prog_period * num_periods)
'
'
'        End If
'    Next site
'End With

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_rtc_clk.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_verify_freq_wurx_clk.Pat").Run("")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks_Verify\system_clocks_verify_CALIB_DONE.Pat").Run("")

End Function

'#####################################################################################################################################
'# Function name: System_Clocks_Verify_VB                      | in develop
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function System_Clocks_Verify_VB(argc As Long, argv() As String) As Long

'Dim RetPeriod As Double  ' return period
'Dim Vdd_CAP_String As String
'Dim Vdd_DBG_String As String
'Dim TCK_String As String
'
'Dim site As Long
'
''Dim RetVoltages As New PinListData
''Dim AVG_samples As New PinListData
''Dim Result As New SiteDouble
'Dim test_num As Long
''Dim Calib_Code(59) As Double
''Dim Calib_Voltage As New SiteDouble
''Dim TDO_Data(59) As String
''Dim loop_index As Long
'
'Dim freq_count As Long
'Dim num_periods As Double
'Dim prog_period As Double
'Dim Freq_Value As Double
'
'
'theHdw.Digital.HRAM.Size = 256
'Call theHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
'Call theHdw.Digital.HRAM.SetCapture(captSTV, False)
'Call theHdw.Digital.Patgen.ClearFailCount
'
'test_num = TheExec.sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21

'Keep same values as in system clocks
'Call Thehdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)  'was 0.85changed to 1V due to VDD_DBG sensitivity of this test


If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.05)
End If

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21

'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_ID_10_START.Pat").Load
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_SCAN_VALS.Pat").Load
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_soc_clk.Pat").Load
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_rtc_clk.Pat").Load
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_wurx_clk.Pat").Load
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_CALIB_DONE.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_ID_10_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_soc_clk.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_rtc_clk.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_wurx_clk.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_CALIB_DONE.Pat").Run("")


End Function
'#####################################################################################################################################
'# Function name: System_Clocks_Calib_Automated_VB                      | in develop
'# Parameters: argc As Long, argv() As String and CSV file
'# Description: using csv file as input for getting the fields names and locations and reading the fileds in for loops using data from the csv
'# High Level Flow: Running patterns -> for each line in csv read the wanted location -> write the result
'#####################################################################################################################################
Function System_Clocks_Calib_Automated_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim Calib_Voltage As New SiteDouble
Dim TDO_Data(59) As String
Dim Freq_SOC(59) As Double
Dim Freq_RTC(59) As Double
Dim Freq_WURX30(59) As Double
Dim Freq_WURX100(59) As Double
Dim loop_index As Long

Dim calib_success_val(59) As Double
Dim calib_success_pass(59) As Double

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (Vdd_Cap_Change_Delay) ' Found we need 25 to have stable result of SoC - I place 35 A.L. 26-7-21

Call SitesFailes_Initialize

'Call Thehdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 0.96)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)  'was 0.85changed to 1V due to VDD_DBG sensitivity of this test

If Debug_Delay Then     'EdenTomer: Do we need this delay?
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.05)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_ID_10_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_CALIB_DONE.PAT").Run("")
TheHdw.Wait (0.005) '(0.005)

'############################# import test parameters   ###########################

Dim Test_Name As String
Dim FilePath As String
Dim TestDataList(12) As String
Dim LineItems As Variant
Dim LineFromFile As Variant
Dim TestCount As Integer
Dim TEST_DATA_LIST(15) As TEST_DATA_LINE
Dim CurrentLine As Long


Test_Name = "SystemClocks"
FilePath = "..\Test_Files\" & Test_Name & ".txt"
TestCount = 0

Open FilePath For Input As #1

Do Until EOF(1)
    Line Input #1, LineFromFile
    LineItems = Split(LineFromFile, ",")
    TEST_DATA_LIST(TestCount).field_name = CStr(LineItems(0))
    TEST_DATA_LIST(TestCount).start_index = CDbl(LineItems(1))
    TEST_DATA_LIST(TestCount).length = CDbl(LineItems(2))
    TEST_DATA_LIST(TestCount).pass_type = CStr(LineItems(3))
    TEST_DATA_LIST(TestCount).LogTestParam = CStr(LineItems(4))
    
    'TEST_DATA_LIST(TestCount).pass_values_num = CDbl(LineItems(5))
    'TEST_DATA_LIST(TestCount).pass_values
    
    TEST_DATA_LIST(TestCount).pass_range_low = CDbl(LineItems(5))
    TEST_DATA_LIST(TestCount).pass_range_high = CDbl(LineItems(6))
    TEST_DATA_LIST(TestCount).lo_limit = CDbl(LineItems(7))
    TEST_DATA_LIST(TestCount).hi_limit = CDbl(LineItems(8))
    
    If CStr(LineItems(9)) = "unitNone" Then
        TEST_DATA_LIST(TestCount).MeasUnit = unitNone
        TEST_DATA_LIST(TestCount).ForceUnit = unitNone
    ElseIf CStr(LineItems(9)) = "unitHz" Then
        TEST_DATA_LIST(TestCount).MeasUnit = unitHz
        TEST_DATA_LIST(TestCount).ForceUnit = unitHz
    End If
    
    TEST_DATA_LIST(TestCount).ForceValue = CDbl(LineItems(10))
    TEST_DATA_LIST(TestCount).IOC = CDbl(LineItems(12))
    TEST_DATA_LIST(TestCount).offline_value = CStr(LineItems(13))
    
    TestCount = TestCount + 1
Loop

Close #1


'############################### main_run ###############################

For CurrentLine = 0 To TestCount - 1

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                TDO_Data(site) = ""
                
                If TheExec.TesterMode = testModeOnline Then
                     TDO_Data(site) = ReadChanelData(chan_num_TDO(site), CDbl(TEST_DATA_LIST(CurrentLine).start_index), CDbl(TEST_DATA_LIST(CurrentLine).length))
                Else: 'offline tester mode
                    TDO_Data(site) = TEST_DATA_LIST(CurrentLine).offline_value
                End If
                     
                If TDO_Data(site) <= CDbl(TEST_DATA_LIST(CurrentLine).pass_range_high) And TDO_Data(site) >= CDbl(TEST_DATA_LIST(CurrentLine).pass_range_low) Then
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, CStr(TEST_DATA_LIST(CurrentLine).field_name), chan_num_TDO(site), CDbl(TEST_DATA_LIST(CurrentLine).lo_limit#), CDbl(TDO_Data(site)), CDbl(TEST_DATA_LIST(CurrentLine).hi_limit#), TEST_DATA_LIST(CurrentLine).MeasUnit, 0, TEST_DATA_LIST(CurrentLine).MeasUnit, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                    
        
                Else 'TDO_Data(site) = "0"
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, TEST_DATA_LIST(CurrentLine).field_name, chan_num_TDO(site), TEST_DATA_LIST(CurrentLine).lo_limit, CDbl(TDO_Data(site)), TEST_DATA_LIST(CurrentLine).hi_limit, unitNone, 0, unitNone, 0)
                    Sites_Failes(site) = True
                End If
                
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " system_clocks_mss_done = " & CStr(TDO_Data(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
            End If
        Next site
    End With
Next CurrentLine

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Temp_Sens_DCDC_Calib_VB
'# Parameters:
'# Description: calibrating the DCDC temperture sensor
'# High Level Flow: initialize -> running all codes till all found -> writing result -> verify measurment
'#####################################################################################################################################
Function Temp_Sens_DCDC_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code_backup As New SiteDouble
Dim Calib_Voltage_backup As New SiteDouble
Dim Calib_Code As New SiteDouble
Dim Calib_Found(59) As Boolean
Dim Calib_backup_Found(59) As Boolean
Dim Calib_Max_value As New SiteDouble
Dim Calib_Max_code As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim CodeString As String
Dim Active_Sites(59) As Boolean
Dim loop_index As Long

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim Code_Number_Var As Variant
Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

Temp_Sens_DCDC_Lookup_Array = Array(25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower


If Debug_Delay Then
   TheHdw.Wait (0.1)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

For loop_index = 0 To Number_of_Sites - 1
    DCDC_Fails(loop_index) = False
Next loop_index


'Measureing code 0
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_ID_5_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_SCAN_VALS.PAT").Run("")

If Debug_Delay Then
   TheHdw.Wait (0.01)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging
If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")

'##########################################         initialize          ##########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Calib_Code(site) = -1 ' the highest
            Calib_Code_backup(site) = -1 ' the highest
            Calib_Found(site) = False
            Calib_backup_Found(site) = False
            Calib_Max_value(site) = -1
            Calib_Max_code(site) = -1
            
        End If
    Next site
End With

'##########################################         1st code to all found         ##########################################

For Each Code_Number_Var In Temp_Sens_DCDC_Lookup_Array
    
    Code_Number = CDbl(Code_Number_Var)

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    TheHdw.Wait (0.005)
    
    test_num = test_num + 1

    If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
    'thehdw.Pins("GPIO4").PPMU.Connect
    'thehdw.Wait (0.005)
    Call HPT_PPMU_ConnectAll("GPIO4")
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else: 'offline tester mode
        Result = OfflineFillArray(0.71)
    End If
    

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                If Calib_Max_value(site) < Result(site) And Result(site) >= Temp_Sens_DCDC_Lowest_Target Then
                    Calib_Max_code(site) = Code_Number
                    Calib_Max_value(site) = Result(site)
                End If
                
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & "  TEMP_SENSE_DCDC_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                If (Result(site) >= Temp_Sens_DCDC_Highest_Target) And (Calib_Found(site) = False) Then
                    Calib_Voltage(site) = Result(site)
                    Calib_Code(site) = Code_Number
                    Calib_Found(site) = True
                    CalibratedSitesCounter(site) = 1    'Will only stop before all codes if all sites got the optimal code
                ElseIf (Result(site) >= Temp_Sens_DCDC_Middle_Target) And (Calib_Found(site) = False) And (Calib_backup_Found(site) = False) Then
                    Calib_Voltage_backup(site) = Result(site)
                    Calib_Code_backup(site) = Code_Number
                    Calib_backup_Found(site) = True
                End If
                    
            End If
        Next site
    End With
Next Code_Number_Var


'##########################################         'Deciding what code and value to use, regular or backup          ##########################################

With HPTInfo.Sites
      For site = 0 To .ExistingCount - 1
          If .site(site).Active = True Then
              If (Calib_Found(site) = False) And (Calib_backup_Found(site) = True) Then
                  Calib_Voltage(site) = Calib_Voltage_backup(site)
                  Calib_Code(site) = Calib_Code_backup(site)
                  Calib_Found(site) = True
              ElseIf (Calib_Found(site) = False) And (Calib_backup_Found(site) = False) Then ' no measurement passed criteria we will take the last one as our result should be the highest
                  Calib_Voltage(site) = Calib_Max_value(site)
                  Calib_Code(site) = Calib_Max_code(site)
              End If
                  
          End If
      Next site
End With

test_num = test_num + 1

'##########################################     'Running the correct code per site to calibrate the voltage and measuring       ##########################################
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_calc_calib_result_TEMP_SENSE_DCDC.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)
'3
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

'4
For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode

'5
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            If Calib_Code(site) <> -1 Then  'Can't get here unless the voltage is negative!  now we do get here because we will choose only values greter then 600mV for the max code
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "TEMP_SENSE_DCDC_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "TEMP_SENSE_DCDC_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                DCDC_Fails(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & "  TEMP_SENSE_DCDC_TEMP_SENSE_DCDC_calib_code = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##########################################     TEMP_SENSE_DCDC_calib_value - verify       ##########################################
'6
TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015)
End If

If RunVerifyMeas Then

    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
   
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else: 'offline tester mode
        Result = OfflineFillArray(0.72)
    End If
Else
    Result = Calib_Voltage
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Abs(Result(site) - Calib_Voltage(site)) <= Temp_Sens_DCDC_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "TEMP_SENSE_DCDC_calib_value", chan_num_GPIO4(site), 0.64 - 0.04 - 0.005, Result(site), 0.64 + 0.26 + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "TEMP_SENSE_DCDC_calib_value", chan_num_GPIO4(site), 0.64 - 0.04 - 0.005, Result(site), 0.64 + 0.26 + 0.005, unitVolt, 0, unitAmp, 0)
                DCDC_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_DCDC_TEMP_SENSE_DCDC_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Abs(Result(site) - Calib_Voltage(site)) > Temp_Sens_DCDC_MeasVariance And Problem_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_DCDC_TEMP_SENSE_DCDC_calib_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Voltage(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_verify_temp_sens_dcdc_vsup_reg.PAT").Run("")  'Load
'TheHdw.Wait (0.005)

'##########################################     verify_temp_sens_dcdc_vsup_reg       ##########################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0

Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
'Call HPT_PPMU_DisconnectAll_GPIO4   'replacing the GPIO4 dissconnect
'Thehdw.Pins("GPIO4").PPMU.Disconnect        'Eden: Do we need that?
Call HPT_PPMU_DisconnectAll("GPIO4")

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else: 'offline tester mode
    Result = OfflineFillArray(0.75)
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If Result(site) >= Temp_Sens_DCDC_VsupReg_Lowest_Target - Temp_Sens_DCDC_VsupReg_Lower_Margin And Result(site) <= Temp_Sens_DCDC_VsupReg_Highest_Target + Temp_Sens_DCDC_VsupReg_Upper_Margin Then '' limit fixed to be 620 Ido & tomer 18-2-21 before 1st WS '' limit fixed to be 600 Ido & tomer 08-11-21 before P3 WS
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verify_temp_sens_dcdc_vsup_reg", chan_num_GPIO4(site), Temp_Sens_DCDC_VsupReg_Lowest_Target - Temp_Sens_DCDC_VsupReg_Lower_Margin, Result(site), Temp_Sens_DCDC_VsupReg_Highest_Target + Temp_Sens_DCDC_VsupReg_Upper_Margin, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verify_temp_sens_dcdc_vsup_reg", chan_num_GPIO4(site), Temp_Sens_DCDC_VsupReg_Lowest_Target - Temp_Sens_DCDC_VsupReg_Lower_Margin, Result(site), Temp_Sens_DCDC_VsupReg_Highest_Target + Temp_Sens_DCDC_VsupReg_Upper_Margin, unitVolt, 0, unitAmp, 0)
                DCDC_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_DCDC_verify_temp_sens_dcdc_vsup_reg = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")
'Terminate the calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_CALIB_DONE.PAT").Run("")

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40060B0C")

End Function

'#####################################################################################################################################
'# Function name: Radio_Refgen_0p8_Calib_VB
'# Parameters:
'# Description: calibrating the Radio_Refgen_0p8 to 800mV
'# High Level Flow: initialize -> run codes till all sites calibrated -> set result code -> run verify meas
'#####################################################################################################################################
Function Radio_Refgen_0p8_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim SitesPerCode(0 To 15, 0 To 59) As Long
Dim ActiveCodes(15) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1#)

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If


RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_ID_31_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_SCAN_VALS.PAT").Run("")


If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Calib_Voltage(site) = 0
            Calib_Code(site) = -1
            
        End If
    Next site
End With

'########################################       Running Codes 1 till all found code        ########################################

For Code_Number = RefGen_0p8_MinCode To RefGen_0p8_MaxCode

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1

    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.84) 'V
    End If
    
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If Active_Sites(site) = True Then
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "code_" & CStr(Code_Number), chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p8V_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                '0.805 < Vtarget <= 0.88 - Accepting variance of 0.01
                If (Abs(RefGen_0p8_Target_Volt - Result(site)) < Abs(RefGen_0p8_Target_Volt - Calib_Voltage(site)) And Result(site) >= RefGen_0p8_Target_Volt And Result(site) <= (RefGen_0p8_Target_Volt + RefGen_0p8_Upper_Margin)) Then   'Finding the correct code
                    Calib_Voltage(site) = Result(site)
                    Calib_Code(site) = Code_Number
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

'########################################       RADIO_0p8V_LDO_calib_code        ########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
        
                indexCode = Calib_Code(site)
                ActiveCodes(indexCode) = True
                SitesPerCode(indexCode, site) = chan_num_TMS(site)
                
        End If
    Next site
End With

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

'Running pattern per Code
For indexCode = 0 To 15
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
                
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode

'Print Results Per site

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
        
            If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p8V_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_0p8V_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p8V_RADIO_0p8V_LDO_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'########################################       RADIO_0p8V_LDO_calib_value - Verify        ########################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)
'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was no delay before
End If

If RunVerifyMeas Then
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)

    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.825) 'V
    End If
Else
    Result = Calib_Voltage
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If Abs(Calib_Voltage(site) - Result(site)) < RefGen_0p8_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p8V_LDO_calib_value", chan_num_GPIO4(site), 0.805 - 0.005, Result(site), 0.805 + 0.065 + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_0p8V_LDO_calib_value", chan_num_GPIO4(site), 0.805 - 0.005, Result(site), 0.805 + 0.065 + 0.005, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p8V_RADIO_0p8V_LDO_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Calib_Voltage(site) - Result(site)) > RefGen_0p8_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p8V_RADIO_0p8V_LDO_Verify_Variance = " & CStr(Abs(Calib_Voltage(site) - Result(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_calc_calib_result_RADIO_0p8V_LDO.Pat").Run("") 'NeedRemove?
'TheHdw.Wait (0.005)

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Refgen_0p6_Calib_VB
'# Parameters:
'# Description: calibrating the Radio_Refgen_0p6 to 600mV
'# High Level Flow: initialize -> run codes till all sites calibrated -> set result code -> run verify meas
'#####################################################################################################################################
Function Radio_Refgen_0p6_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Code_X2 As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long
Dim Calib_Code_X2_default As Double
Calib_Code_X2_default = 0

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0
test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower
TheHdw.Wait (0.005)


Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If


RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_ID_32_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_SCAN_VALS.PAT").Run("")

'####################################           initialize           ####################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Calib_Voltage(site) = 0
            Calib_Code(site) = -1
            Calib_Code_X2(site) = -1

        End If
    Next site
End With

'####################################           code_0_code_1 To All Found           ####################################


For Code_Number = RefGen_0p6_MinCode To RefGen_0p6_MaxCode
    
    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_config_code_0_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_meas_code_0_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
        
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.625) 'V
    End If
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then

                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "code_0_code_" & CStr(Code_Number), chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p6V_code_0_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                '       0.605 <= Vtarget <= 0.650
                If (Abs(RefGen_0p6_Target_Volt - Result(site)) < Abs(RefGen_0p6_Target_Volt - Calib_Voltage(site)) And Result(site) >= RefGen_0p6_Target_Volt) And Result(site) <= (RefGen_0p6_Target_Volt + RefGen_0p6_Upper_Margin) Then 'Finding the correct code
                    Calib_Voltage(site) = Result(site)
                    Calib_Code(site) = Code_Number
                    Calib_Code_X2(site) = 0
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

'####################################           RADIO_0p6V_SYM_LDO_calib_code           ####################################

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_calc_calib_result_RADIO_0p6V_SYM_LDO.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            If Calib_Code(site) <> -1 Then
            
                indexCode = Calib_Code(site)
                ActiveCodes(indexCode) = True
                SitesPerCode(indexCode, site) = chan_num_TMS(site)
                
            End If
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_config_code_" & CStr(Calib_Code_X2_default) & "_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_meas_code_" & CStr(Calib_Code_X2_default) & "_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p6V_SYM_LDO_ret_radio_refgen_0p6_trim_x2_calib_code", chan_num_Default, 0, Calib_Code_X2(site), 1, unitNone, 0, unitNone, 0)

            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_ret_radio_refgen_0p6_trim_x2_calib_code" & " = " & CStr(Calib_Code_X2(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
           
           If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "RADIO_0p6V_SYM_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_0p6V_SYM_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
           
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 2

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015)
End If

'####################################           RADIO_0p6V_SYM_LDO_calib_value           ####################################
If RunVerifyMeas Then
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
      
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.625) 'V
    End If
Else
    Result = Calib_Voltage
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Abs(Calib_Voltage(site) - Result(site)) < RefGen_0p6_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p6V_SYM_LDO_calib_value", chan_num_GPIO4(site), 0.605 - 0.005, Result(site), 0.605 + 0.045 + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_0p6V_SYM_LDO_calib_value", chan_num_GPIO4(site), 0.605 - 0.005, Result(site), 0.605 + 0.045 + 0.005, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Calib_Voltage(site) - Result(site)) > RefGen_0p6_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p6V_RADIO_0p74V_LDO_Verify_Variance = " & CStr(Abs(Calib_Voltage(site) - Result(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_calc_calib_result_RADIO_0p6V_SYM_LDO.Pat").Run("")
'TheHdw.Wait (0.005)

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Refgen_0p74_Calib_VB
'# Parameters:
'# Description: calibrating the Radio_Refgen_0p74 to 740mV
'# High Level Flow: initialize -> run codes till all sites calibrated -> set result code -> run verify meas
'#####################################################################################################################################
Function Radio_Refgen_0p74_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Code_X2 As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long
Dim Calib_Code_X2_default As Double

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

Calib_Code_X2_default = 0
ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging

'Measureing code 0
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_ID_33_START.PAT").Run("")    'EdenToTomer: DO we need that? Can we run or remove?
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_SCAN_VALS.PAT").Run("")

'#####################################      initialize and connecting       #####################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Calib_Voltage(site) = 0
            Calib_Code(site) = -1
            Calib_Code_X2(site) = 0
            
        End If
    Next site
End With

test_num = test_num + 1

'#####################################      code_0_code_1 To All found       #####################################

For Code_Number = RefGen_0p74_MinCode To RefGen_0p74_MaxCode
    
    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_config_code_0_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_meas_code_0_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
                
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.755) 'V
    End If

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "code_0_code_" & CStr(Code_Number), chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p74V_" & "code_0_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                '   0.745 <= Vtarget <= 0.790
                If (Abs(RefGen_0p74_Target_Volt - Result(site)) < Abs(RefGen_0p74_Target_Volt - Calib_Voltage(site)) And Result(site) >= RefGen_0p74_Target_Volt) And Result(site) <= (RefGen_0p74_Target_Volt + RefGen_0p74_Upper_Margin) Then 'Finding the correct code
                    Calib_Voltage(site) = Result(site)
                    Calib_Code(site) = Code_Number
                    Calib_Code_X2(site) = 0
                    CalibratedSitesCounter(site) = 1
                End If
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

'#####################################      RADIO_0p74V_LDO_calib_code       #####################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
                
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
            
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_config_code_" & CStr(Calib_Code_X2_default) & "_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_meas_code_" & CStr(Calib_Code_X2_default) & "_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then

            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p74V_LDO_ret_radio_refgen_0p74_trim_x2_calib_code", chan_num_Default, 0, Calib_Code_X2(site), 1, unitNone, 0, unitNone, 0)

            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p74V_RADIO_0p74V_LDO_ret_radio_refgen_0p74_trim_x2_calib_code" & " = " & CStr(Calib_Code_X2(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
            
            If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "RADIO_0p74V_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestFail, parmLow, "RADIO_0p74V_LDO_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p74V_RADIO_0p74V_LDO_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 2

'#####################################      RADIO_0p74V_LDO_calib_value       #####################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015)
Else
        TheHdw.Wait (Defult_Delay_sec)
End If

If RunVerifyMeas Then
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.755) 'V
    End If
Else
    Result = Calib_Voltage
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Abs(Calib_Voltage(site) - Result(site)) < RefGen_0p74_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_0p74V_LDO_calib_value", chan_num_GPIO4(site), 0.745 - 0.005, Result(site), 0.745 + 0.045 + 0.005, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_0p74V_LDO_calib_value", chan_num_GPIO4(site), 0.745 - 0.005, Result(site), 0.745 + 0.045 + 0.005, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p74V_RADIO_0p74V_LDO_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Calib_Voltage(site) - Result(site)) > RefGen_0p74_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_refgen_0p74V_RADIO_0p74V_LDO_Verify_Variance = " & CStr(Abs(Calib_Voltage(site) - Result(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
             
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_calc_calib_result_RADIO_0p74V_LDO.Pat").Run("")
'TheHdw.Wait (0.005)

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Aux_Mode_VB
'# Parameters:
'# Description: inner calibration for the radio aux mode
'# High Level Flow: running patterns for calbration -> measuring voltaged to see results -> pass/fail according to specific range
'#####################################################################################################################################
Function Radio_Aux_Mode_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging

'Radio Aux Modee 0.8V measurement
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_ID_34_START.PAT").Run("") 'EdenTomer - should we use this pattern
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p8V_LDO.PAT").Run("")

'#############################################      verif_verify_0p8V_LDO       #############################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")
Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
       
If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.85) 'V
End If


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Result(site) >= (Radio_Aux_Mode_0p8V_LDO_Lowest) And Result(site) <= (Radio_Aux_Mode_0p8V_LDO_Highest) Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verif_verify_0p8V_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p8V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p8V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verif_verify_0p8V_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p8V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p8V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_aux_mode_verif_verify_0p8V_LDO = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      verif_verify_0p74V_LDO       #############################################

'Radio Aux Modee 0.74V measurement
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p74V_LDO.PAT").Run("")

Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
            
If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.77) 'V
End If


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Result(site) >= (Radio_Aux_Mode_0p74V_LDO_Lowest) And Result(site) <= (Radio_Aux_Mode_0p74V_LDO_Highest) Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verif_verify_0p74V_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p74V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p74V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verif_verify_0p74V_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p74V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p74V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_aux_mode_verif_verify_0p74V_LDO = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      radio_aux_mode_verif_verify_0p6V       #############################################

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

'Radio Aux Modee 0.6V measurement
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p6V_AUX_LDO.PAT").Run("")

'#############################################      verif_verify_0p6V_AUX_LDO       #############################################

Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
'Call HPT_PPMU_DisconnectAll_GPIO4
'Thehdw.Pins("GPIO4").PPMU.Disconnect
Call HPT_PPMU_DisconnectAll("GPIO4")

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.58) 'V
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Result(site) >= (Radio_Aux_Mode_0p6V_LDO_Lowest) And Result(site) <= (Radio_Aux_Mode_0p6V_LDO_Highest) Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verif_verify_0p6V_AUX_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p6V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p6V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verif_verify_0p6V_AUX_LDO", chan_num_GPIO4(site), Radio_Aux_Mode_0p6V_LDO_Lowest, Result(site), Radio_Aux_Mode_0p6V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_aux_mode_verif_verify_0p6V_AUX_LDO = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_LC_Aux_Mode_VB
'# Parameters:
'# Description: inner calibration for the radio lc aux mode
'# High Level Flow: running patterns for calbration -> measuring voltaged to see results -> pass/fail according to specific range
'#####################################################################################################################################

Function Radio_LC_Aux_Mode_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging


'Radio Aux Modee 0.8V measurement
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_ID_35_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_verify_0p8V_LDO.PAT").Run("")

'#############################################      verif_verify_0p8V_LDO       #############################################

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")
Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.85) 'V
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Result(site) >= (Radio_LC_Aux_Mode_0p8V_LDO_Lowest) And Result(site) <= (Radio_LC_Aux_Mode_0p8V_LDO_Highest) Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verif_verify_0p8V_LDO", chan_num_GPIO4(site), Radio_LC_Aux_Mode_0p8V_LDO_Lowest, Result(site), Radio_LC_Aux_Mode_0p8V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verif_verify_0p8V_LDO", chan_num_GPIO4(site), Radio_LC_Aux_Mode_0p8V_LDO_Lowest, Result(site), Radio_LC_Aux_Mode_0p8V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_mode_verif_verify_0p8V_LDO = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      verif_verify_0p74V_LDO       #############################################

'Radio Aux Modee 0.74V measurement
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_verify_0p74V_LDO.PAT").Run("")

Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
  
If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("GPIO4")
Else
    Result = OfflineFillArray(0.76) 'V
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If Result(site) >= (Radio_LC_Aux_Mode_0p74V_LDO_Lowest) And Result(site) <= (Radio_LC_Aux_Mode_0p74V_LDO_Highest) Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verif_verify_0p74V_LDO", chan_num_GPIO4(site), Radio_LC_Aux_Mode_0p74V_LDO_Lowest, Result(site), Radio_LC_Aux_Mode_0p74V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verif_verify_0p74V_LDO", chan_num_GPIO4(site), Radio_LC_Aux_Mode_0p74V_LDO_Lowest, Result(site), Radio_LC_Aux_Mode_0p74V_LDO_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_mode_verif_verify_0p74V_LDO = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Lc_Aux_Idac_Calib_VB
'# Parameters:
'# Description: calibration for the LC_AUX_Idac values
'# High Level Flow: running all codes from 0_1 to 1_7 -> calculating avg_delta per site -> choosing code using the avrage delta and target -> setting minimal code of 0_6 -> run verify meas
'#####################################################################################################################################
Function Radio_Lc_Aux_Idac_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Active_Sites(59) As Boolean

Dim Code_Number_major As Double
Dim Code_Number_minor As Double
Dim Chosen_Calib_Code_major As New SiteDouble
Dim Chosen_Calib_Code_minor As New SiteDouble
Dim Chosen_Calib_Code_value As New SiteDouble
Dim LCAUX_ZERO_CURRENT(59) As Double
Dim Calib_Current(20) As New SiteDouble
Dim Delta_Idac(20) As New SiteDouble
Dim Avg_Delta As New SiteDouble
Dim Abs_index As Double
Dim Internal_Code_String As String
Dim Code_Number_Var As Variant

Dim current_distance As Double
Dim tmp_value As Double
Dim tmp_distance As Double
Dim tmp_index As Double
Dim tmp_divider As Double

Dim SitesPerCode(0 To 19, 0 To 59) As Long
Dim ActiveCodes(19) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long
Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

'LC_AUX_IDAC_Lookup_Array = Array(6, 7, 14, 15, 16, 17)
LC_AUX_IDAC_Lookup_Array = Array(1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 16, 17)

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9)

If Debug_Delay Then
    TheHdw.Wait (0.2)
Else
    TheHdw.Wait (0.005)
End If


TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

If Debug_Delay Then
    TheHdw.Wait (0.2)
Else
    TheHdw.Wait (0.03)
End If


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_ID_46_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_verify_current_LCAUX_ZERO_CURRENT.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_SCAN_VALS.PAT").Run("")

'##################################     initialize and Zero current meas      ####################################

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)


    
If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetCurrents.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.00039) 'V
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            LCAUX_ZERO_CURRENT(site) = Result(site)
            Avg_Delta(site) = 0
            Internal_Code_String = "verify_current_LCAUX_ZERO_CURRENT"
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, Internal_Code_String, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestNoPF, parmPass, Internal_Code_String & "_delta_idac", chan_num_VddCap(site), 0, Avg_Delta(site), 0.001, unitAmp, 1.1, unitVolt, 0)

            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_" & Internal_Code_String & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With


'##################################        looping over all the codes      ##################################

For Each Code_Number_Var In LC_AUX_IDAC_Lookup_Array

    If Code_Number_Var > 9 Then
        Code_Number_major = 1
    Else
        Code_Number_major = 0
    End If
    
    Code_Number_minor = CDbl(CDbl(Code_Number_Var) Mod 10)
        
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_" & CStr(Code_Number_major) & "_" & CStr(Code_Number_minor) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_" & CStr(Code_Number_major) & "_" & CStr(Code_Number_minor) & ".Pat").Run("")
    test_num = test_num + 2
    
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
  '  TheHdw.Wait (0.02)
                
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result(site) = 0.00041 'V
    End If
    
     With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Result(site) = Result(site) - LCAUX_ZERO_CURRENT(site)
                
                tmp_index = Code_Number_major * 10 + Code_Number_minor
                tmp_divider = (Code_Number_major + 1) * Code_Number_minor
                Calib_Current(tmp_index)(site) = Result(site)
                Delta_Idac(tmp_index)(site) = Calib_Current(tmp_index)(site) / tmp_divider
                Avg_Delta(site) = Avg_Delta(site) + Delta_Idac(tmp_index)(site)
                Internal_Code_String = "code_" & CStr(Code_Number_major) & "_code_" & CStr(Code_Number_minor)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, Internal_Code_String, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestNoPF, parmPass, Internal_Code_String & "_delta_idac", chan_num_VddCap(site), 0, Delta_Idac(tmp_index)(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                 
                If Code_Number_major = 1 And Code_Number_minor = 7 Then
                    'this will be the end of the loop - lets calculate the avg delta
                    Avg_Delta(site) = Avg_Delta(site) / 14
                    Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestNoPF, parmPass, "Avg_Delta", chan_num_Default, 0, Avg_Delta(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                End If
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_" & Internal_Code_String & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_" & Internal_Code_String & "_delta_idac" & " = " & CStr(Delta_Idac(tmp_index)(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                    If Code_Number_major = 1 And Code_Number_minor = 7 Then
                        Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_" & "avg_delta" & " = " & CStr(Avg_Delta(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                    End If
                End If
                
            End If
            
        Next site
    End With
        
Next Code_Number_Var

test_num = test_num + 3

'###########################    find optimal calib_code      #####################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            'start by choosing code -1 as our default
            Chosen_Calib_Code_major(site) = -1
            Chosen_Calib_Code_minor(site) = -1
            current_distance = 1
                
            
            For Each Code_Number_Var In LC_AUX_IDAC_Lookup_Array
            
                If Code_Number_Var > 9 Then
                    Code_Number_major = 1
                Else
                    Code_Number_major = 0
                End If
                
                Code_Number_minor = CDbl(CDbl(Code_Number_Var) Mod 10)
     
                tmp_index = Code_Number_major * 10 + Code_Number_minor
                tmp_value = Calib_Current(tmp_index)(site) + Avg_Delta(site)
                tmp_distance = tmp_value - 0.000035

                If tmp_distance >= 0 And tmp_distance < current_distance Then
                    Chosen_Calib_Code_major(site) = Code_Number_major
                    Chosen_Calib_Code_minor(site) = Code_Number_minor
                    Chosen_Calib_Code_value(site) = tmp_value
                    current_distance = tmp_distance
                End If
                
            Next Code_Number_Var
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "selected_code_x2", chan_num_Default, 0, Chosen_Calib_Code_major(site), 1, unitNone, 0, unitNone, 0)
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestNoPF, parmPass, "selected_code", chan_num_Default, 1, Chosen_Calib_Code_minor(site), 7, unitNone, 1.1, unitNone, 0)
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestNoPF, parmPass, "selected_code_value", chan_num_Default, 0, Chosen_Calib_Code_value(site), 0.00008, unitAmp, 1.1, unitVolt, 0)

            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_selected_code_x2 = " & CStr(Chosen_Calib_Code_major(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_selected_code = " & CStr(Chosen_Calib_Code_minor(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_selected_code_value = " & CStr(Chosen_Calib_Code_value(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
            'we would like to set the minimum special index to be 6 meaning 1_2 or 1_3 will set also to be 0_6
            Abs_index = (Chosen_Calib_Code_major(site) + 1) * Chosen_Calib_Code_minor(site)
            If Abs_index <= 6 Then
                Chosen_Calib_Code_major(site) = 0
                Chosen_Calib_Code_minor(site) = 6
                tmp_index = Chosen_Calib_Code_major(site) * 8 + Chosen_Calib_Code_minor(site)
                Chosen_Calib_Code_value(site) = Calib_Current(tmp_index)(site) + Avg_Delta(site)
            End If
            
            test_num = test_num + 3
            
            If current_distance <> 1 Then ' we found a calibration >=25uA
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "ret_lcaux_idac_x2_ctl", chan_num_Default, 0, Chosen_Calib_Code_major(site), 1, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "ret_lcaux_idac_ctl", chan_num_Default, 1, Chosen_Calib_Code_minor(site), 7, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "ret_lcaux_idac_x2_ctl", chan_num_Default, 0, Chosen_Calib_Code_major(site), 1, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestFail, parmLow, "ret_lcaux_idac_ctl", chan_num_Default, 1, Chosen_Calib_Code_minor(site), 7, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Chosen_Calib_Code_value(site) < 0.00008 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestPass, parmPass, "calib_value", chan_num_VddCap(site), 0, Chosen_Calib_Code_value(site), 0.00008, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestFail, parmLow, "calib_value", chan_num_VddCap(site), 0, Chosen_Calib_Code_value(site), 0.00008, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_ret_lcaux_idac_x2_ctl = " & CStr(Chosen_Calib_Code_major(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_ret_lcaux_idac_ctl = " & CStr(Chosen_Calib_Code_minor(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_calib_value = " & CStr(Chosen_Calib_Code_value(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If

    Next site
End With

test_num = test_num + 3

'##########################################     'Running the correct code per Code to calibrate the voltage and measuring       ##########################################

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_calc_calib_result_LCAUX_IDAC_CALIB.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Chosen_Calib_Code_major(site) <> -1 And Chosen_Calib_Code_minor(site) <> -1 Then

            indexCode = Chosen_Calib_Code_major(site) * 10 + Chosen_Calib_Code_minor(site)  ' we wiil use codes 0,1,2,3,4,5,6,7 and 10,11,12,13,14,15,16,17 as our codes when the first digit is the major code
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
            
        End If
    Next site
End With


For Code_Number_major = 0 To 1
    For Code_Number_minor = 1 To 7
    
        indexCode = Code_Number_major * 10 + Code_Number_minor
        
        If ActiveCodes(indexCode) = True Then
        
            For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
            
            TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
            
            If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
            End If
            
            Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_" & CStr(Code_Number_major) & "_" & CStr(Code_Number_minor) & ".Pat").Run("")
        
            Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_" & CStr(Code_Number_major) & "_" & CStr(Code_Number_minor) & ".Pat").Run("")
            
            TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
    
        End If
    Next Code_Number_minor
Next Code_Number_major

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

test_num = test_num + 1

'#################################################          radio_lcaux_idac_calib_calib_value - verify         #################################################

If RunVerifyMeas Then

    TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
    TheHdw.PPMU.samples = PPMU_Averaging
    'thehdw.Pins("VDD_CAP").PPMU.Connect
    Call HPT_PPMU_ConnectAll("VDD_CAP")
    RetCurrents.ResultType = tlResultTypeParametricValue

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
        
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000103)
    End If
Else
    Result = Chosen_Calib_Code_value
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If RunVerifyMeas Then
                Result(site) = Result(site) - LCAUX_ZERO_CURRENT(site)
            End If
            
            If Abs(Result(site) + Avg_Delta(site) - Chosen_Calib_Code_value(site)) <= LC_AUX_IDAC_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestPass, parmPass, "verify_calib_value", chan_num_VddCap(site), 0, Result(site) + Avg_Delta(site), 0.00008, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestFail, parmLow, "verify_calib_value", chan_num_VddCap(site), 0, Result(site) + Avg_Delta(site), 0.00008, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                    
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_verify_calib_value = " & CStr(Result(site) + Avg_Delta(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) + Avg_Delta(site) - Chosen_Calib_Code_value(site)) > LC_AUX_IDAC_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lcaux_idac_calib_verify_variance = " & CStr(Abs(Result(site) + Avg_Delta(site) - Chosen_Calib_Code_value(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Terminate the calibration
Call HPT_PPMU_DisconnectAll("VDD_CAP")

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_CALIB_DONE.Pat").Run("")

Call SitesFailes_FinalSetAllFails

End Function


'#####################################################################################################################################
'# Function name: Radio_Clocks_Calib_VB
'# Parameters: argc As Long, argv() As String
'# Description: calibrating the system clocks - using the given pattern we run the inside calibration then we are reading from the DRAM all the values and compare to limits for pass/fail
'# High Level Flow: Running patterns -> for each test do for all sites -> read DRAM and compare to limits pass/fail -> fails will be declared at the end of test program
'#####################################################################################################################################
Function Radio_Clocks_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim TDI_Data(59) As String
Dim i As Long
Dim Freq_rx_sym_clk(59) As Double
Dim Freq_tx_sym_clk(59) As Double
Dim Freq_aux_clk(59) As Double
Dim Freq_DCDC(59) As Double
Dim loop_index As Long

Dim calib_success_val As Double
Dim calib_success_pass As Double

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Call SitesFailes_Initialize

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_ID_50_START.Pat").Run("")

'moved from the CTAT tempreture
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDI_Data(site) = Converted_Temp_Bin
            
            For i = 0 To 11 '7
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").ModifyChanVectorData("modv_radio_clocks_actual_temperature_index", i, chan_num_TDI(site), Mid(TDI_Data(site), i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").ModifyChanVectorData("modv_radio_clocks_actual_temperature_index", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), i + 1, 1))
                End If
            Next i
            
        End If
    Next site
End With

'Call ReadRegForcePrint2Datalog("40010128")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").Run("")

'Call ReadRegForcePrint2Datalog("40010128")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_CALIB_DONE.PAT").Run("")

'Call ReadRegForcePrint2Datalog("40010128")

'#################################################         mss_done        #################################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            If TDO_Data(site) = "1" Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "mss_done", chan_num_TDO(site), 1, CStr(TDO_Data(site)), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "mss_done", chan_num_TDO(site), 1, CStr(TDO_Data(site)), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
                
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_mss_done = " & CStr(TDO_Data(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
                
        
        End If
    Next site
End With

'################################################        calib_success & calib_success_pass      #################################################

test_num = test_num + 1
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 4)
            Else
                TDO_Data(site) = "1111"
            End If
            
            calib_success_val = CDbl(Bin2Dec(TDO_Data(site)))
            
            If TDO_Data(site) = "1111" Then
                calib_success_pass = 1
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "calib_success", chan_num_TDO(site), 15, calib_success_val, 15, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "calib_success_pass", chan_num_Default, 0, calib_success_pass, 2, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                calib_success_pass = 0
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "calib_success", chan_num_TDO(site), 15, calib_success_val, 15, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "calib_success_pass", chan_num_Default, 1, calib_success_pass, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_calib_success = " & CStr(Bin2Dec(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_calib_success_pass = " & CStr(calib_success_pass) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With

test_num = test_num + 2

'################################################        rx_sym_clk_freq        ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 5, 32)
            Else
                TDO_Data(site) = "00000000000011110100001001000000"
            End If
            
            Freq_rx_sym_clk(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Freq_rx_sym_clk(site) >= 990000# And Freq_rx_sym_clk(site) <= 1010000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "rx_sym_clk_freq", chan_num_TDO(site), 990000#, Freq_rx_sym_clk(site), 1010000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "rx_sym_clk_freq", chan_num_TDO(site), 990000#, Freq_rx_sym_clk(site), 1010000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
                
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_rx_sym_clk_freq = " & CStr(Freq_rx_sym_clk(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With

test_num = test_num + 1
   
'################################################        tx_sym_clk_freq        ################################################
   
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            'Watch :   : TDO_Data(site) : "00000000000011110011111100110010" : String : Module1.Radio_Clocks_Calib_VB
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 37, 32)
            Else
                TDO_Data(site) = "00000000000011110011111100110010"
            End If

            Freq_tx_sym_clk(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Freq_tx_sym_clk(site) >= 990000# And Freq_tx_sym_clk(site) <= 1010000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "tx_sym_clk_freq", chan_num_TDO(site), 990000#, Freq_tx_sym_clk(site), 1010000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "tx_sym_clk_freq", chan_num_TDO(site), 990000#, Freq_tx_sym_clk(site), 1010000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
                            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_tx_sym_clk_freq = " & CStr(Freq_tx_sym_clk(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With
        
test_num = test_num + 1

'################################################        aux_clk_freq       ################################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 69, 32)
            Else
                TDO_Data(site) = "00000000000100100001111010101100"
            End If
            
            Freq_aux_clk(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Freq_aux_clk(site) >= 650000# And Freq_aux_clk(site) <= 1500000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "aux_clk_freq", chan_num_TDO(site), 650000#, Freq_aux_clk(site), 1500000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "aux_clk_freq", chan_num_TDO(site), 650000#, Freq_aux_clk(site), 1500000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
                
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_aux_clk_freq = " & CStr(Freq_aux_clk(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'################################################        dcdc_clk_freq      ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 101, 32)
            Else
                TDO_Data(site) = "00000001011110101100110010101000"
            End If
            
            Freq_DCDC(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Freq_DCDC(site) >= 20000000# And Freq_DCDC(site) <= 30000000# Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "dcdc_clk_freq", chan_num_TDO(site), 20000000#, Freq_DCDC(site), 30000000#, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "dcdc_clk_freq", chan_num_TDO(site), 20000000#, Freq_DCDC(site), 30000000#, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_dcdc_clk_freq = " & CStr(Freq_DCDC(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'################################################        def_ret_sym_freq_word_c        ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 133, 4)
            Else
                TDO_Data(site) = "1110"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_sym_freq_word_c", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_sym_freq_word_c", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_def_ret_sym_freq_word_c = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'################################################        def_ret_sym_freq_word_f        ################################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 137, 5)
            Else
                TDO_Data(site) = "00101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 31 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_sym_freq_word_f", chan_num_TDO(site), 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_sym_freq_word_f", chan_num_TDO(site), 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
                            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_def_ret_sym_freq_word_f = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'################################################        def_ret_aux_div_ctl        ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 142, 2)
            Else
                TDO_Data(site) = "01"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_aux_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_aux_div_ctl", chan_num_TDO(site), 0, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_def_ret_aux_div_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'################################################        def_ret_aux_measure        ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 144, 20)
            Else
                TDO_Data(site) = "01111110011110010110"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 1048575 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_aux_measure", chan_num_TDO(site), 0, Calib_Code(site), 1048575, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_aux_measure", chan_num_TDO(site), 0, Calib_Code(site), 1048575, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_def_ret_aux_measure = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'################################################        def_ret_radio_dcdc_ctl         ################################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 164, 4)
            Else
                TDO_Data(site) = "1001"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_radio_dcdc_ctl", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_radio_dcdc_ctl", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_clocks_def_ret_radio_dcdc_ctl = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With


Call SitesFailes_FinalSetAllFails

End Function
'#####################################################################################################################################
'# Function name: Radio_LO_Init_Measure_VB
'# Parameters:
'# Description: measuring the Radio_LO_Init current
'# High Level Flow: Running patterns -> measure viltegs => write results
'#####################################################################################################################################

Function Radio_LO_Init_Measure_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (0.005)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue
TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_ID_36_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_verify_current_RADIO_SYM_MODE_CURRENT.Pat").Run("")

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_CALIB_DONE.PAT").Run("")

'################################################        verify_current_RADIO_SYM_MODE_CURRENT      ################################################

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetCurrents.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.0000077) 'A
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Idd_Radio_Lo(site) = Result(site)

            If Result(site) >= Radio_LO_Init_SYM_MODE_CURRENT_Lowest And Result(site) <= Radio_LO_Init_SYM_MODE_CURRENT_Highest Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verify_current_RADIO_SYM_MODE_CURRENT", chan_num_VddCap(site), Radio_LO_Init_SYM_MODE_CURRENT_Lowest, Idd_Radio_Lo(site), Radio_LO_Init_SYM_MODE_CURRENT_Highest, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verify_current_RADIO_SYM_MODE_CURRENT", chan_num_VddCap(site), Radio_LO_Init_SYM_MODE_CURRENT_Lowest, Idd_Radio_Lo(site), Radio_LO_Init_SYM_MODE_CURRENT_Highest, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_init_and_meas_verify_current_RADIO_SYM_MODE_CURRENT = " & CStr(Idd_Radio_Lo(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call HPT_PPMU_DisconnectAll("VDD_CAP")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Lo_VBP_TX_Calib_VB
'# Parameters:
'# Description: calibrating the VBP TX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################

Function Radio_Lo_VBP_TX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double

Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim calib_code_with_offset(59) As Double
Dim Internal_Code_Used_with_offset As Double
Dim Calib_Code_NVM As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.2)
Else
   TheHdw.Wait (0.005)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1 'ppmu200uA
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

If Debug_Delay Then
   TheHdw.Wait (0.2)
Else
   TheHdw.Wait (0.03)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_ID_38_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_SCAN_VALS.PAT").Run("")

'###########################################        radio_lo_vbp_tx 1st code     ###########################################


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site) but result is always more than 20uA higher so I set it to zero to disable wrong selection of this code
            Calib_Code(site) = -1
            
        End If
    Next site
End With

'###########################################        radio_lo_vbp_tx_code 1-end          ###########################################

For Code_Number = LO_VBP_TX_MinCode To LO_VBP_TX_MaxCode Step 2

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
        
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000237) 'V
    End If
    

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Result(site) = Result(site) - Idd_Radio_Lo(site)
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_tx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                If (Abs(LO_VBP_TX_Target_Curr - Result(site)) < Abs(LO_VBP_TX_Target_Curr - Calib_Current(site)) And Result(site) >= LO_VBP_TX_Target_Curr - LO_VBP_TX_Lower_Margin) Then 'Finding the correct code
                    Calib_Current(site) = Result(site)
                    Calib_Code(site) = Code_Number
                End If
                
                If (Result(site) - LO_VBP_TX_Target_Curr) >= 0 Then
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

'###########################################        RADIO_LO_VBP_TX_calib_code & RADIO_LO_VBP_TX_calib_code_With_Offset         ###########################################

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_calc_calib_result_RADIO_LO_VBP_TX.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

'Filling the Array wuth TMS channel per code
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
        
            If Calib_Code(site) + 4 < 31 Then
                calib_code_with_offset(site) = Calib_Code(site) + 4
            Else
                calib_code_with_offset(site) = 31
            End If
        
            indexCode = calib_code_with_offset(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With


'Running the config pattern per code for all common sites
For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


'printing the result for each site
With HPTInfo.Sites

    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
        
            If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_VBP_TX_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "RADIO_LO_VBP_TX_calib_code_With_Offset", chan_num_Default, 0, calib_code_with_offset(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_VBP_TX_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestFail, parmLow, "RADIO_LO_VBP_TX_calib_code_With_Offset", chan_num_Default, 0, calib_code_with_offset(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                 Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                 'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_tx_RADIO_LO_VBP_TX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_tx_RADIO_LO_VBP_TX_calib_code_WithOffset" & " = " & CStr(calib_code_with_offset(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
             End If
            
        End If
    Next site
End With

test_num = test_num + 2

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

TheHdw.Wait (0.005)

'###########################################        RADIO_LO_VBP_TX_calib_value           ###########################################

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetCurrents.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.000033) 'A
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Result(site) = Result(site) - Idd_Radio_Lo(site)
            
            'Here because of the offset we will agree to higer current up to 46.5uA
            If Result(site) >= (Calib_Current(site) - LO_VBP_TX_MeasVariance) And Result(site) <= (Calib_Current(site) + 0.000013 + 0.00001 + LO_VBP_TX_MeasVariance) Then 'offset can take us to higher values then margin wider
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_VBP_TX_calib_value", chan_num_VddCap(site), 0.000022 - 0.0000015, Result(site), 0.000022 + 0.000013 + 0.0000015 + 0.00001, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_VBP_TX_calib_value", chan_num_VddCap(site), 0.000022 - 0.0000015, Result(site), 0.000022 + 0.000013 + 0.0000015 + 0.00001, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_tx_RADIO_LO_VBP_TX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs((Result(site) - Calib_Current(site))) > (LO_VBP_RX_MeasVariance + 0.000013 + 0.00001) Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_tx_RADIO_LO_VBP_TX_Verify_Variance = " & CStr(Abs((Result(site) - Calib_Current(site)))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


test_num = test_num + 1

Call HPT_PPMU_DisconnectAll("VDD_CAP")

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Lo_VBP_RX_Calib_VB
'# Parameters:
'# Description: calibrating the VBP RX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_Lo_VBP_RX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long
Dim Code_Number As Double
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Code_NVM As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim index As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

Call SitesFailes_Initialize

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.85) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

'Measureing code 0
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_ID_40_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_SCAN_VALS.PAT").Run("")


'###################################        initialize       ###################################


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site) but result is always more than 20uA higher so I set it to zero to disable wrong selection of this code
            Calib_Code(site) = -1
            
        End If
    Next site
End With

'###################################        Code 1 to all found        ###################################

For Code_Number = LO_VBP_RX_MinCode To LO_VBP_RX_MaxCode Step 2

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000018) 'A
    End If
    
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Result(site) = Result(site) - Idd_Radio_Lo(site)
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                 
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_rx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                'Vtarger: closest to 11 uA and could be in 0.000007 <= Vtarget <= 0.000012 ' Eden: Ask Sagi exactly what we want here
                If (Abs(LO_VBP_RX_Target_Curr - Result(site)) < Abs(LO_VBP_RX_Target_Curr - Calib_Current(site)) And Result(site) <= LO_VBP_RX_Target_Curr + LO_VBP_RX_Upeer_Margin) And Result(site) >= (LO_VBP_RX_Target_Curr - LO_VBP_RX_Lower_Margin) Then
                    Calib_Current(site) = Result(site)
                    Calib_Code(site) = Code_Number
                End If
                
                If (Result(site) - LO_VBP_RX_Target_Curr) >= 0 And Calib_Code(site) <> -1 Then
                    CalibratedSitesCounter(site) = 1
                End If
                
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_calc_calib_result_RADIO_LO_VBP_RX.Pat").Run("")

'####################################           ADIO_LO_VBP_RX_calib_code           ####################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

'Filling the Array of channels per code
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            index = Calib_Code(site)
            ActiveCodes(index) = True
            SitesPerCode(index, site) = chan_num_TMS(site)
        End If
    Next site
End With

'Running the chosen code for all sites sharing same code
For index = 0 To 31
    If ActiveCodes(index) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(index, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_config_code_" & CStr(index) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_meas_code_" & CStr(index) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)

    End If
Next index


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            CodeString = "code_" & CStr(Calib_Code(site))
            
            If Calib_Code(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_VBP_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_VBP_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                 Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                 'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_rx_RADIO_LO_VBP_RX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With


TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

'###########################################        RADIO_LO_VBP_RX_calib_value - verify         ###########################################

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
    
If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetCurrents.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.000018) 'A
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Result(site) = Result(site) - Idd_Radio_Lo(site)

            If Abs(Result(site) - Calib_Current(site)) < LO_VBP_RX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_VBP_RX_calib_value", chan_num_VddCap(site), LO_VBP_RX_Target_Curr - LO_VBP_RX_Lower_Margin - LO_VBP_RX_MeasVariance, Result(site), LO_VBP_RX_Target_Curr + LO_VBP_RX_Upeer_Margin + LO_VBP_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_VBP_RX_calib_value", chan_num_VddCap(site), LO_VBP_RX_Target_Curr - LO_VBP_RX_Lower_Margin - LO_VBP_RX_MeasVariance, Result(site), LO_VBP_RX_Target_Curr + LO_VBP_RX_Upeer_Margin + LO_VBP_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_rx_RADIO_LO_VBP_RX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Current(site)) > LO_VBP_RX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vbp_rx_RADIO_LO_VBP_RX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Current(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_calc_calib_result_RADIO_LO_VBP_RX.Pat").Run("")

Call HPT_PPMU_DisconnectAll("VDD_CAP")

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: BUI_Vstart_Calib_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function BUI_Vstart_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim Result As New SiteDouble

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)  '0.96
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_ID_62_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_end_of_idle.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_set_high_vdd_cap.Pat").Run("")
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.8)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.03)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_CALIB_DONE.Pat").Run("")

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (0.03)
End If

'############################### vstart_ref_success ###############################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 4, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If TDO_Data(site) = "011" Then  'Need to be 'LHH'
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "vstart_ref_success", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "vstart_ref_success", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
              
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " bui_vstart_ref_vstart_ref_success = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
                    
        End If
    Next site
End With

test_num = test_num + 1

'############################### read_vstart_ref_bui ###############################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 4)
            Else
                TDO_Data(site) = "0101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 2 And Calib_Code(site) <= 12 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "read_vstart_ref_bui", chan_num_TDO(site), 2, Calib_Code(site), 12, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "read_vstart_ref_bui", chan_num_TDO(site), 2, Calib_Code(site), 12, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " bui_vstart_ref_read_vstart_ref_bui" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'############################### read_vstart_ref_bui ###############################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 7, 4)
            Else
                TDO_Data(site) = "0101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 2 And Calib_Code(site) <= 12 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_vstart_ref", chan_num_TDO(site), 2, Calib_Code(site), 12, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_vstart_ref", chan_num_TDO(site), 2, Calib_Code(site), 12, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " bui_vstart_ref_def_ret_vstart_ref" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function


'#####################################################################################################################################
'# Function name: Wkup_Level_Ret_Calib_VB
'# Parameters:
'# Description: inner calibration for the Wkup_Level_Ret
'# High Level Flow: running patterns -> reading results -> verifing all in range -> writing results
'#####################################################################################################################################
Function Wkup_Level_Ret_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim Done_Val(59) As Double
Dim Calib_Voltage As New SiteDouble
Dim TDO_Data(59) As String
Dim loop_index As Long

For loop_index = 0 To Number_of_Sites - 1
    WkUp_Fails(loop_index) = False
Next loop_index

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_ID_61_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_wkup_end_of_idle.Pat").Run("")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_run_internal_calib.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_read_calib_success.Pat").Run("")

'TheHdw.Wait (0.3)   '01/03/2022 Tomer: "400ms delay inserted to patterns"

'##############################################      calib_success       ##############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 2)
            Else
                TDO_Data(site) = "11"
            End If
            
            Done_Val(site) = CDbl(Bin2Dec(TDO_Data(site)))
                
            If Done_Val(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "calib_success", chan_num_TDO(site), 3, Done_Val(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "calib_success", chan_num_TDO(site), 3, Done_Val(site), 3, unitNone, 0, unitNone, 0)
                WkUp_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_level_ret_calib_calib_success = " & Done_Val(site) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1
'##############################################      nrgdet_adv_comp_offset_neg       ##############################################
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_CALIB_DONE.Pat").Run("")


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 4)
            Else
                TDO_Data(site) = "0110"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "nrgdet_adv_comp_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "nrgdet_adv_comp_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                WkUp_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Calib_nrgdet_adv_comp_offset_neg" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'##############################################      nrgdet_adv_comp_offset_pos       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 4, 4)
            Else
                TDO_Data(site) = "0111"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "nrgdet_adv_comp_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "nrgdet_adv_comp_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                WkUp_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Calib_nrgdet_adv_comp_offset_pos" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##############################################      def_ret_nrgdet_comp_ref       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 8, 3)
            Else
                TDO_Data(site) = "110"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 6 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ret_nrgdet_comp_ref", chan_num_TDO(site), 0, Calib_Code(site), 6, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ret_nrgdet_comp_ref", chan_num_TDO(site), 0, Calib_Code(site), 6, unitNone, 0, unitNone, 0)
                WkUp_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Calib_def_ret_nrgdet_comp_ref" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##############################################      def_active_ret_nrgdet_comp_ref       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 11, 3)
            Else
                TDO_Data(site) = "110"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 6 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_active_ret_nrgdet_comp_ref", chan_num_TDO(site), 0, Calib_Code(site), 6, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_active_ret_nrgdet_comp_ref", chan_num_TDO(site), 0, Calib_Code(site), 6, unitNone, 0, unitNone, 0)
                WkUp_Fails(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Calib_def_active_ret_nrgdet_comp_ref" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


test_num = test_num + 1

End Function

'#####################################################################################################################################
'# Function name: Wkup_Level_Ret_Active_Calib_VB
'# Parameters:
'# Description: inner calibration for the Wkup_Level_Ret_Active
'# High Level Flow: running patterns -> reading results -> verifing all in range -> writing results
'#####################################################################################################################################
Function Wkup_Level_Ret_Active_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim calib_success As New SiteDouble

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.Wait (0.05) 'D3 WS ToDo check if needed Ameet 5-11-21 - Eden: maybe we can cut this out on D4?
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_ID_62_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_wkup_end_of_idle.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_CALIB_DONE.Pat").Run("")

'##############################################      Wkup_Level_Active_Ret_offset_neg       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 2, 4)
            Else
                TDO_Data(site) = "0101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Wkup_Level_Active_Ret_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Wkup_Level_Active_Ret_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Active_Calib_Wkup_Level_Active_Ret_offset_neg" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'##############################################      Wkup_Level_Active_Ret_offset_pos       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 6, 4)
            Else
                TDO_Data(site) = "0101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Wkup_Level_Active_Ret_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Wkup_Level_Active_Ret_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Ret_Active_Calib_Wkup_Level_Active_Ret_offset_pos" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##############################################      calib_success       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 2)
            Else
                TDO_Data(site) = "11"
            End If
            
            calib_success(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If calib_success(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "calib_success", chan_num_TDO(site), 3, calib_success(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "calib_success", chan_num_TDO(site), 3, calib_success(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Wkup_Level_Ret_Active_Calib_calib_success = " & CStr(calib_success(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Wkup_Level_Active_Calib_VB
'# Parameters:
'# Description: inner calibration for the Wkup_Level_Active
'# High Level Flow: running patterns -> reading results -> verifing all in range -> writing results
'#####################################################################################################################################
Function Wkup_Level_Active_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim calib_success As New SiteDouble

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
   TheHdw.Wait (0.05)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_ID_63_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_wkup_end_of_idle.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_CALIB_DONE.Pat").Run("")

'##############################################      Wkup_Level_Active_offset_neg       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 2, 4)
            Else:
                TDO_Data(site) = "0100"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Wkup_Level_Active_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Wkup_Level_Active_offset_neg", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Active_Calib_Wkup_Level_Active_offset_neg" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'##############################################      Wkup_Level_Active_offset_pos       ##############################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 6, 4)
            Else:
                TDO_Data(site) = "0101"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 15 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Wkup_Level_Active_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Wkup_Level_Active_offset_pos", chan_num_TDO(site), 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_Level_Active_Calib_Wkup_Level_Active_offset_pos" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##############################################      calib_success       ##############################################
 
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 2)
            Else
                TDO_Data(site) = "01"
            End If
            
            calib_success(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If calib_success(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "calib_success", chan_num_TDO(site), 3, calib_success(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "calib_success", chan_num_TDO(site), 3, calib_success(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If

            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " wkup_level_active_calib_calib_success = " & CStr(calib_success(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Read_SRID_VB
'# Parameters:
'# Description: Reading the SRID and Flow version from the chip
'# High Level Flow: Running patterns -> Reading Results -> comparing to expected readouts
'#####################################################################################################################################
Function Read_SRID_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_soc_clk_in.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_analog_cfg.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_dram_net_go.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_load_mcu_clocks_calib.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_ret_from_gnvm.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_CALIB_DONE.Pat").Run("")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\WR_RD_REG_TEST_ID_100_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\WR_RD_REG_TEST_CALIB_DONE.Pat").Run("")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\SRID\READ_SRID_ID_105_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\SRID\READ_SRID_CALIB_DONE.Pat").Run("")

test_num = test_num + 1
            
'###########################################    read_srid   ###########################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 12)
            Else
                TDO_Data(site) = "010000000011" ' 1027
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) = SRID Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "read_srid", chan_num_TDO(site), SRID, Calib_Code(site), SRID, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "read_srid", chan_num_TDO(site), SRID, Calib_Code(site), SRID, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Read_SRID_read_srid = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1
            
'###########################################    flow_version   ###########################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 12, 16)
            Else
                TDO_Data(site) = "0000010000100000" '1056
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) = Flow_Version Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "flow_version", chan_num_TDO(site), Flow_Version, Calib_Code(site), Flow_Version, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "flow_version", chan_num_TDO(site), Flow_Version, Calib_Code(site), Flow_Version, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Read_SRID_flow_version = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1
            
'############## The printout of packet version to cloud was disabled due to Tomer A. request - Ameet Nov 2021 start of D3 #################
            
'With HPTInfo.sites
'    For site = 0 To .ExistingCount - 1
'        If .site(site).Active = True Then
'                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Packet_version", Chan_num, 2.1, 2.1, 2.1, unitNone, 0, unitNone, 0)
'                HPTInfo.sites.site(site).TestResult = sitePass
'                Call HPT_ReportResult(site, logTestPass)
'        End If
'    Next site
'End With
                
test_num = test_num + 1
            
'###########################################    Group_ID   ###########################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            KEYS_ARRAY(site).groupId = KEYS_ARRAY(site).groupId
            Calib_Code(site) = CDbl(Bin2Dec(ConvertHexBin(KEYS_ARRAY(site).groupId)))
           
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Group_ID", chan_num_Default, 0, Calib_Code(site), 100, unitNone, 0, unitNone, 0)
            HPTInfo.Sites.site(site).TestResult = sitePass
            Call HPT_ReportResult(site, logTestPass)
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Read_SRID_Group_ID = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With
        
Call SitesFailes_FinalSetAllFails
        
End Function


'#####################################################################################################################################
'# Function name: Radio_FLL_Meas_LO_VB
'# Parameters:
'# Description: Measurment of the LO frequency by the FLL
'# High Level Flow: Running patterns -> Reading Results -> comparing to lower and upper allowed limits
'#####################################################################################################################################
Function Radio_FLL_Meas_LO_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim LO_Freq(59) As Double
Dim TDO_Data(59) As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_ID_45_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_CALIB_DONE.Pat").Run("")

TheHdw.Wait (0.1)

test_num = test_num + 1
            
'###########################################    LO_Frequency_Measurment   ###########################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 16)
            Else
                TDO_Data(site) = "010000000011" ' 1027
            End If
            
            LO_Freq(site) = CDbl(Bin2Dec(TDO_Data(site)))
            LO_Freq(site) = LO_Freq(site) / 16
           
            If LO_Freq(site) >= LO_Freq_Lowest And LO_Freq(site) <= LO_Freq_Highest Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "aux_meas_lo_6", chan_num_TDO(site), LO_Freq_Lowest, LO_Freq(site), LO_Freq_Highest, unitHz, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "aux_meas_lo_6", chan_num_TDO(site), LO_Freq_Lowest, LO_Freq(site), LO_Freq_Highest, unitHz, 0, unitNone, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_fll_meas_lo_aux_meas_lo_6 = " & CStr(LO_Freq(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
Call SitesFailes_FinalSetAllFails
        
End Function


'#####################################################################################################################################
'# Function name: Temp_Sense_CTAT_Calib_VB
'# Parameters:
'# Description: inner calibration for the temp sense CTAT
'# High Level Flow: running patterns -> reading results -> verifing all in range -> writing results
'#####################################################################################################################################
Function Temp_Sense_CTAT_Calib_VB_WA(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim CTAT_Fail(59) As Boolean

'Call ReadRegForcePrint2Datalog("40010128") 'The register that the Temp written to

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower
'was 24
TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If
    

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_ID_6_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_CALIB_DONE.Pat").Run("")


'########################################       def_ctat_trim         ########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 3, 7)
            Else
                TDO_Data(site) = "0011111"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 127 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ctat_trim", chan_num_TDO(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ctat_trim", chan_num_TDO(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_CTAT_def_ctat_trim" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        

test_num = test_num + 1

'########################################       Temp_Sense_CTAT_Success         ########################################

Dim RegData(59) As String

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 3)
            Else
                TDO_Data(site) = "011"
            End If
            

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
           
            If Calib_Code(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                'Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                CTAT_Fail(site) = True
                'HPTInfo.sites.site(site).TestResult = siteFail
                'Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_CTAT_Temp_Sense_CTAT_Success" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With



With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If CTAT_Fail(site) Then
                
                RegData(site) = ReadSiteRegForcePrint2Datalog("4001010C", site)
                
                If RegData(site) = "00000003" Then
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, 3, 3, unitNone, 0, unitNone, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                Else
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                    HPTInfo.Sites.site(site).TestResult = siteFail
                    Call HPT_ReportResult(site, logTestFail)
                
                End If
          
          
            End If
        End If
    Next site
End With


End Function

'#####################################################################################################################################
'# Function name: Temp_Sense_CTAT_Calib_VB
'# Parameters:
'# Description: inner calibration for the temp sense CTAT
'# High Level Flow: running patterns -> reading results -> verifing all in range -> writing results
'#####################################################################################################################################
Function Temp_Sense_CTAT_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String

'Call ReadRegForcePrint2Datalog("40010128") 'The register that the Temp written to

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower
'was 24
TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If
    

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_ID_6_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_SCAN_VALS.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_run_internal_calib.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\TEMP_SENSE_CTAT_read_calib_success.Pat").Run("")

'########################################       Temp_Sense_CTAT_Success         ########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 3)
            Else
                TDO_Data(site) = "011"
            End If

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Temp_Sense_CTAT_Success", chan_num_TDO(site), 3, Calib_Code(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_CTAT_Temp_Sense_CTAT_Success" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'########################################       def_ctat_trim         ########################################
Call TheHdw.Digital.Patterns.Pat(".\Patterns\TEMP_SENSE_CTAT_CALIB_DONE.Pat").Run("")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 7)
            Else
                TDO_Data(site) = "0011111"
            End If
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
           
            If Calib_Code(site) >= 0 And Calib_Code(site) <= 127 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "def_ctat_trim", chan_num_TDO(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "def_ctat_trim", chan_num_TDO(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_CTAT_def_ctat_trim" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Write_Device_Params_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function Write_Device_Params_VB(argc As Long, argv() As String) As Long

Dim site As Long
Dim TDI_Data(59) As String
Dim i As Long
Dim UID_MSB_lower_Inv(59) As String
Dim UID_LSB_upper_Inv(59) As String
Dim Physical_info_Inv(59) As String

Dim X_Loc(59) As String
Dim Y_Loc(59) As String
Dim Wafer_STR As String
Dim Lot_STR As String

Dim LOT_Digit1 As String
Dim LOT_Digit2 As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

'test_num = TheExec.Sites.site(0).TestNumber

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").Load  'Eden: Already Loaded why we doing again?

'########################################       modv_write_device_specific_params_uid_lower         ########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
    
        If .site(site).Active = True Then
        
            UID_MSB_lower(site) = Mid(Site_Die_ID(site).tagId, 1, 4)
            UID_MSB_lower(site) = ConvertHexBin(Mid(Site_Die_ID(site).tagId, 1, 4))
            
            'In CJTAG we need to input the NOT Data to the TDI (TMS)
            If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then
                UID_MSB_lower_Inv(site) = Cjtag_Inverse(UID_MSB_lower(site), 16)
                TDI_Data(site) = UID_MSB_lower_Inv(site)
            End If
            
            If Debug_Printout Then
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " from file MSB = " & CStr(UID_MSB_lower(site)))
            End If
            
            For i = 0 To 15
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_uid_lower", i, chan_num_TDI(site), Mid(TDI_Data(site), 15 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_uid_lower", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 15 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                End If
            Next i
            
        End If
    Next site
End With

'########################################       modv_write_device_specific_params_uid_upper         ########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            UID_LSB_upper(site) = ConvertHexBin(Mid(Site_Die_ID(site).tagId, 5, 8))
            
            'In CJTAG we need to input the NOT Data to the TDI (TMS)
            If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then
                UID_LSB_upper_Inv(site) = Cjtag_Inverse(UID_LSB_upper(site), 32)
                TDI_Data(site) = UID_LSB_upper_Inv(site)
            End If
            
            If Debug_Printout Then
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " from file LSB = " & CStr(UID_LSB_upper(site)))
            End If

            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_uid_upper", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_uid_upper", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i

        End If
    Next site
End With

'########################################       modv_write_device_specific_params_wafer_info_0         ########################################

With HPTInfo.Sites

    LOT_Digit1 = Mid(Lot_Name, 5, 1)
    
    If (LOT_Digit1 >= "0") And (LOT_Digit1 <= "9") Then
        LOT_Digit1 = Mid(Lot_Name, 5, 1)
    Else
        LOT_Digit1 = "0"
    End If
            
    LOT_Digit2 = Mid(Lot_Name, 6, 1)
    
    If (LOT_Digit2 >= "0") And (LOT_Digit2 <= "9") Then
        LOT_Digit2 = Mid(Lot_Name, 6, 1)
    Else
        LOT_Digit2 = "0"
    End If
  
    Lot_STR = ConvertHexBin(LOT_Digit1 & LOT_Digit2)
    Wafer_STR = Dec2Bin(wafer_id, 8)
    Wafer_STR = Right(Wafer_STR, 7) ' Ameet change to 7 bits since the X coordinate is now 9 bits
    
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            X_Loc(site) = Dec2Bin(X_coor(site), 9)
            Y_Loc(site) = Dec2Bin(Y_coor(site), 8)

            Physical_info(site) = Lot_STR & Wafer_STR & X_Loc(site) & Y_Loc(site)

            If Debug_Printout = True Then
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " Lot " & CStr(Lot_STR))
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " Wafer " & CStr(Wafer_STR))
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " X Location " & CStr(X_Loc(site)))
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " Y Location " & CStr(Y_Loc(site)))
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " Info to be Programmed " & CStr(Physical_info(site)))
                TheExec.Datalog.WriteComment ("Write_Device_Params: Site " & CStr(site) & " Info to be Programmed hex " & CStr(Bin2Hex32bits(Physical_info(site))))
            End If
            
            If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then
                Physical_info_Inv(site) = Cjtag_Inverse(Physical_info(site), 32)
                TDI_Data(site) = Physical_info_Inv(site)
            End If
            
            For i = 0 To 31
            
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_wafer_info_0", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_wafer_info_0", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                End If
                
            Next i
        End If
    Next site
End With

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_ID_98_START.Pat").Run("") '
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").Run("") '

End Function

'#####################################################################################################################################
'# Function name: Ext_Temp_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function Ext_Temp_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim Decimal_Point As Double
Dim TDO_Data_MSB As String
Dim TDO_Data_LSB As String
Dim i As Long
Dim Tmp_Data As String
Dim RetPinArray() As String
Dim RetPinCnt As Long
Dim chan_num_SDA(59) As Long
Dim Temp_Source As Double

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

'Creating local array for the channel num for the SDA pin - only needed in this function
Call TheExec.DataManager.DecomposePinList("Temp_Sens_SDA", RetPinArray, RetPinCnt)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        chan_num_SDA(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray(site), site, True))
    Next site
End With

'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Ext_Temp\Read_Manufacturer_ID.Pat").Load
'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Ext_Temp\Read_Temperature_rev2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Ext_Temp\MCP9808_read_temperature.Pat").Load


Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower


TheHdw.Wait (0.055)  ' Nesacery dely passed 10 times at 75msec, not stable at 70msec fail always at 65msec - took 50msec for margin A.L. 25-7-21 - ToDo verify on PC done on 10-8-21 in PC also 5msec is OK

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Ext_Temp\MCP9808_read_temperature.Pat").Run("")

'###########################################        Measured_temp_MSB       ###########################################

test_num = test_num + 1

'Read the Temp for the first active site

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data_MSB = ""
            If TheExec.TesterMode = testModeOnline Then
                For i = 4 To 7 'Step -1
                    Tmp_Data = TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(i)
                    If Tmp_Data = "H" Then
                        TDO_Data_MSB = TDO_Data_MSB & "1"
                    Else
                        TDO_Data_MSB = TDO_Data_MSB & "0"
                    End If
                Next i
             Else:
               TDO_Data_MSB = "1101"
            End If
            
            'We would like to read the temperture once for the first active site and use the data for all sites
            If CDbl(Bin2Dec(TDO_Data_MSB)) >= 0 And CDbl(Bin2Dec(TDO_Data_MSB)) <= 256 Then Exit For
            

        End If
    Next site
End With

'Write the temperture for all active sites

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data_MSB))

            If Calib_Code(site) >= 0 And Calib_Code(site) <= 256 Then   'Should be 15? only 4 bit
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Measured_temp_MSB", chan_num_SDA(site), 0, Calib_Code(site), 256, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Measured_temp_MSB", chan_num_SDA(site), 0, Calib_Code(site), 256, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

test_num = test_num + 1

'###########################################        Measured_temp_LSB       ###########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

        TDO_Data_LSB = ""
        
        If TheExec.TesterMode = testModeOnline Then
            For i = 8 To 11 'Step -1
                Tmp_Data = TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(i)
                If Tmp_Data = "H" Then
                    TDO_Data_LSB = TDO_Data_LSB & "1"
                Else
                    TDO_Data_LSB = TDO_Data_LSB & "0"
                End If
            Next i
        Else
            TDO_Data_LSB = "0100"
        End If

        'We would like to read the temperture once for the first active site and use the data for all sites
        If CDbl(Bin2Dec(TDO_Data_LSB)) >= 0 And CDbl(Bin2Dec(TDO_Data_LSB)) <= 127 Then Exit For

        End If
    Next site
End With

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            Calib_Code(site) = CDbl(Bin2Dec(TDO_Data_LSB))

            If Calib_Code(site) >= 0 And Calib_Code(site) <= 127 Then   'Should be 15? only 4 bit
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Measured_temp_LSB", chan_num_SDA(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Measured_temp_LSB", chan_num_SDA(site), 0, Calib_Code(site), 127, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

test_num = test_num + 1

'###########################################        Measured_temp_Point       ###########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

        Decimal_Point = 0

        If TheExec.TesterMode = testModeOnline Then

            If TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(12) = "H" Then
                Decimal_Point = 2 ^ -1
            End If

            If TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(13) = "H" Then
                Decimal_Point = Decimal_Point + 2 ^ -2
            End If

            If TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(14) = "H" Then
                Decimal_Point = Decimal_Point + 2 ^ -3
            End If

            If TheHdw.Digital.HRAM.Chans(chan_num_SDA(site)).PinData(15) = "H" Then
                Decimal_Point = Decimal_Point + 2 ^ -4
            End If

        Else
            Decimal_Point = Decimal_Point + 2 ^ -3
        End If

        'We would like to read the temperture once for the first active site and use the data for all sites
        If Decimal_Point >= 0 And Decimal_Point <= 1 Then Exit For
    
        End If
    Next site
End With


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Decimal_Point >= 0 And Decimal_Point <= 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Measured_temp_Point", chan_num_SDA(site), 0, Decimal_Point, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Measured_temp_Point", chan_num_SDA(site), 0, Decimal_Point, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

test_num = test_num + 1

'###########################################        Measured_temp       ###########################################

Fix_Temp = False

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            Tmp_Data = TDO_Data_MSB & TDO_Data_LSB

            Actual_Temp(site) = CDbl(Bin2Dec(TDO_Data_MSB & TDO_Data_LSB)) + Decimal_Point '25.5 setting it to fixed temp

            If (Actual_Temp(site) < 32) And (Actual_Temp(site) > 20) Then
                Actual_Temp(site) = Actual_Temp(site) ' We have valid Probe card temperature
            Else
                Fix_Temp = True ' We have a non valid Probe card temperature so we use constant temperature
                Actual_Temp(site) = FixedTempreture
            End If

            If Actual_Temp(site) >= 0 And Actual_Temp(site) <= 85 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Measured_temp", chan_num_Default, 0, Actual_Temp(site), 85, unitCustom, 0, unitNone, 0, , "C")
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else ' we can't get here
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Measured_temp", chan_num_Default, 0, Actual_Temp(site), 85, unitCustom, 0, unitNone, 0, , "C")
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

test_num = test_num + 1 '  add 1 for next test without VB 7-12-21 A.L


'Eden: this part till the end was part of the CTAT function and moved to here

'Moving to prober temperature - disabled now - we use PC temp sensor only -Ameet 9-8-2021
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            Call TheExec.Datalog.WriteComment("#DB999 prober temp is " & CStr(Actual_Prober_Temp))
            Call TheExec.Datalog.WriteComment("#D888 PC-Sensor temp is " & CStr(Actual_Temp(site)))

            'Comment : Actual_Temp(site) contains the temperature from the function of the probe-card sensor it will be either the probe-card temp or a fixed temp
            If (Actual_Prober_Temp > (Actual_Temp(site) - 4)) And (Actual_Prober_Temp < (Actual_Temp(site) + 6)) Then  '
                Valid_Prober_temp = True
                Actual_Temp(site) = Actual_Prober_Temp
                Former_good_Prober_Temp = Actual_Prober_Temp
             ElseIf Valid_Prober_temp = True Then
                 Actual_Temp(site) = Former_good_Prober_Temp
             Else
                Actual_Temp(site) = Actual_Temp(site) ' + PC_TempSense_Offset

            End If
         End If
    Next site
End With

'#########################################      Temp_Source     #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If Valid_Prober_temp = True Then 'And Fix_Temp = False Then
                 Temp_Source = 1
                'Call TheExec.Datalog.WriteComment("-----> Using the Prober Temperature!!!!")
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Source", chan_num_Default, 3, Temp_Source, 3, unitNone, 0, unitNone, 0)
            Else
                If Fix_Temp = True Then
                    Temp_Source = 2
                    'Call TheExec.Datalog.WriteComment("-----> Using the Fixed Temperature!!!!")
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Source", chan_num_Default, 3, Temp_Source, 3, unitNone, 0, unitNone, 0)
                Else
                    Temp_Source = 3
                    'Call TheExec.Datalog.WriteComment("-----> Using the Probe-Card Temperature!!!!")
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Temp_Source", chan_num_Default, 3, Temp_Source, 3, unitNone, 0, unitNone, 0)
                End If
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

'#########################################      Measured_Temperature  &  Converted_Temp_Code     #########################################


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Converted_Temp = Int((Actual_Temp(site) + 40) * 4 + 0.5) ' should happen once since its global and all the tempertures are per wafer
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Measured_Temperature", chan_num_Default, -40, Actual_Temp(site), 85, unitNone, 0, unitNone, 0)
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " TEMP_SENSE_CTAT_Measured_Temperature" & " = " & CStr(Actual_Temp(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "Converted_Temp_Code", chan_num_Default, 0, Converted_Temp, 500, unitNone, 0, unitNone, 0)
            HPTInfo.Sites.site(site).TestResult = sitePass
            Call HPT_ReportResult(site, logTestPass)
                
        End If
    Next site
End With

test_num = test_num + 2

'#########################################      Measured_Temperature    &     Converted_Temp_Code     #########################################

'Modifying the pattern with the updated temperature code

Converted_Temp_Bin = DecimalToBinary(CLng(Converted_Temp)) 'The conversion returns the binary with LSB first, MSB last
For i = Len(Converted_Temp_Bin) + 1 To 12 'to make sure we get 12 bits binary number
    Converted_Temp_Bin = Converted_Temp_Bin & "0"
Next i

'TheExec.Datalog.WriteComment ("Converted Temp Bin = " & CStr(Converted_Temp_Bin))

If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then 'in CJTAG we need to input the NOT Data to the TDI (TMS)
    For i = 1 To 12
        If Mid(Converted_Temp_Bin, i, 1) = "1" Then
            Mid(Converted_Temp_Bin, i, 1) = "0"
        Else
            Mid(Converted_Temp_Bin, i, 1) = "1"
        End If
    Next i
'TheExec.Datalog.WriteComment ("CJTAG Converted Temp Bin = " & CStr(Converted_Temp_Bin))
End If

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Verify_Device_Params_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function Verify_Device_Params_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim Result(59) As Double
Dim TDO_Data(59) As String
Dim Printout_Result As String


'######## Failing dictionary ########
'Result = 0 | Not failed yet
'Result = 1 | Pass All
'Result = 2 | Failed on uid_upper
'Result = 3 | Failed on uid_lower
'Result = 4 | Failed on PGXDID
'Result = 5 | Failed on ID_Key_0
'Result = 6 | Failed on ID_Key_1
'Result = 7 | Failed on ID_Key_2
'Result = 8 | Failed on ID_Key_3
'Result = 9 | Failed on Data_Key_0
'Result = 10 | Failed on Data_Key_1
'Result = 11 | Failed on Data_Key_2
'Result = 12 | Failed on Data_Key_3
'Result = 13 | Failed on Wafer_Info
'######## Failing dictionary ########

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_ID_98_START.Pat").Run("")
'Call Thehdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_wafer_info.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_wiliot_uid.Pat").Run("")

'#########################################      uid_upper      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "0001101011110111111000101111011"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), UID_LSB_upper(site)) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 2
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_uid_upper = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_uid_upper_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With
        
'test_num = test_num + 1

'#########################################      uid_lower      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 32, 16)
            Else
                TDO_Data(site) = "1111101011110111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), UID_MSB_lower(site)) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 3
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_uid_lower = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_uid_lower_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      PGXDID      #########################################

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_PGXDID.Pat").Run("")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "111111011000101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(Bin2Hex32bits(TDO_Data(site)), Group_ID_ForCompare) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 4
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_PGXDID = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_PGXDID_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With
        
        
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_ID_KEY.Pat").Run("")
'test_num = test_num + 1

'#########################################      ID_Key_0      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "111111011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), ID_KEY_0) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 5
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_Key_0 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_Key_0_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      ID_Key_1      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 32, 32)
            Else
                TDO_Data(site) = "111111011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), ID_KEY_1) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 6
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_Key_1 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_Key_1_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      ID_Key_2      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 64, 32)
            Else
                TDO_Data(site) = "11000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), ID_KEY_2) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 7
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_KEY_2 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_KEY_2_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      ID_Key_3      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 96, 32)
            Else
                TDO_Data(site) = "10000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), ID_KEY_3) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 8
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_KEY_3 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_ID_KEY_3_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_DATA_KEY.Pat").Run("")
'test_num = test_num + 1

'#########################################      Data_Key_0      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "11000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), Data_KEY_0) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 9
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_0 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_0_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      Data_Key_1      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 32, 32)
            Else
                TDO_Data(site) = "11000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), Data_KEY_1) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 10
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_1 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_1_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      Data_Key_2      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 64, 32)
            Else
                TDO_Data(site) = "11000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), Data_KEY_2) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 11
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_2 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_2_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'test_num = test_num + 1

'#########################################      Data_Key_3      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 96, 32)
            Else
                TDO_Data(site) = "11000011111101110111110101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), Data_KEY_3) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 12
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_3 = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Data_Key_3_value = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If

        End If
    Next site
End With


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_wafer_info.Pat").Run("")
'test_num = test_num + 1

'#########################################      Wafer_Info      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "11000011111101110110000101000111"
            End If
            
            Printout_Result = ""
            
            If StrComp(TDO_Data(site), Physical_info(site)) = 0 Then
                Printout_Result = "1"
            Else
                Result(site) = 13
                Printout_Result = "-1"
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Wafer_Info = " & CStr(Printout_Result) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Wafer_Info = " & CStr(TDO_Data(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_Wafer_Info_hex = " & CStr(Bin2Hex32bits(TDO_Data(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_CALIB_DONE.Pat").Run("")

'#########################################      Print result      #########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If Result(site) = 0 Then
                Result(site) = 1
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "All", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "All", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Verify_Device_Params_All = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: ROM_CRC_VB
'# Parameters:
'# Description: validating the data burned in the NVM is the correct data we wanted to
'# High Level Flow: running patterns for calculatinf CRC on the ROM inside -> readinf results -> comparing to expected results
'#####################################################################################################################################
Function ROM_CRC_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim TDO_Data(59) As String

Dim PrintOut_long As Long
Dim PrintOut_String() As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9)
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.8) '1.4

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

'Call Read_SRID_VB(PrintOut_long, PrintOut_String())

Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\reset_post_nvm_burn_ID_92_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\RESET_AND_POWER_ON_POST_NVM_BURN.Pat").Run("")

Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\reset_post_nvm_burn_CALIB_DONE.Pat").Run("")


'Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity
'Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 0.96)
'TheHdw.Wait (0.05)

'If Debug_Delay Then    'Ameet commented out 22.12.2021
'    TheHdw.Wait (0.05)
'Else
'    TheHdw.Wait (Defult_Delay_sec)
'End If


Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\rom_crc_check_ID_90_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\rom_crc_check_CALIB_DONE.Pat").Run("")

'#####################################      check_rom_crc_done      #####################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
        
            If Result(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "check_rom_crc_done", chan_num_TDO(site), 0, Result(site), 2, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "check_rom_crc_done", chan_num_TDO(site), 0, Result(site), 2, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " ROM_CRC_check_rom_crc_done = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#####################################      check_rom_crc_success      #####################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 2)
            Else
                TDO_Data(site) = "01"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "check_rom_crc_success", chan_num_TDO(site), 0, Result(site), 2, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "check_rom_crc_success", chan_num_TDO(site), 0, Result(site), 2, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " ROM_CRC_check_rom_crc_success = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


'Call ReadRegForcePrint2Datalog("00200AD4")
'Call ReadRegForcePrint2Datalog("00200BB4")
'Call ReadRegForcePrint2Datalog("00200BB8")
'Call ReadRegForcePrint2Datalog("00200BBC")
'Call ReadRegForcePrint2Datalog("00200BC0")
'Call ReadRegForcePrint2Datalog("00200BC4")
'Call ReadRegForcePrint2Datalog("00200BC8")
'Call ReadRegForcePrint2Datalog("00200BCC")
'Call ReadRegForcePrint2Datalog("00200BD0")

Call SitesFailes_FinalSetAllFails

End Function


Function Temp_Sens_Internal_Voltage_Calib_VB_PPMU(argc As Long, argv() As String) As Long

'This function does not updated with the TMS connecting per code

Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim ZeroVoltage(59) As Double
Dim AvgVoltagePerSite(59) As Double
Dim AvgTmpPerSite(59) As Double
Dim AvgTmpCrossAll As Double
Dim SitesCounter As Long
Dim AvgTmpPerCode(7) As Double

Dim Samples_PPMU As Long


Dim RetPinArray() As String
Dim RetPinCnt As Long
Dim chan_num_TMP_SNS(59) As Long


test_num = TheExec.Sites.site(0).TestNumber

'Creating local array for the channel num for the TMP_SNS pin - only needed in this function
Call TheExec.DataManager.DecomposePinList("TMP_SNS", RetPinArray, RetPinCnt)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        chan_num_TMP_SNS(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray(site), site, True))
    Next site
End With


Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

Samples_PPMU = 64

RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = Samples_PPMU


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_ID_7_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_SCAN_VALS.PAT").Run("")

For Code_Number = 0 To 7
    
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    

    If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("TMP_SNS").ForceCurrent(ppmu20uA) = 0
    TheHdw.Pins("TMP_SNS").PPMU.Connect
    Call TheHdw.Pins("TMP_SNS").PPMU.MeasureVoltages(RetVoltages)
    TheHdw.Pins("TMP_SNS").PPMU.Disconnect
    
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("TMP_SNS")
    
    test_num = test_num + 1
    
    SitesCounter = 0

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
            
                SitesCounter = SitesCounter + 1
            
                AvgVoltagePerSite(site) = Result(site)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "AvgVoltagePerSite_Code_" & CStr(Code_Number), chan_num_TMP_SNS(site), 0, AvgVoltagePerSite(site), 5, unitVolt, 0, unitAmp, 0)

                Result(site) = 71.597 * Result(site) - 273.15
                
                AvgTmpPerSite(site) = Result(site)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestNoPF, parmPass, "AvgTmpPerSite_Code_" & CStr(Code_Number), chan_num_TMP_SNS(site), 0, AvgTmpPerSite(site), 80, unitCustom, 0, unitNone, 0, "", "C")

                AvgTmpPerCode(Code_Number) = AvgTmpPerCode(Code_Number) + AvgTmpPerSite(site)

            End If
        Next site
    End With
    
    test_num = test_num + 1
    
    AvgTmpPerCode(Code_Number) = AvgTmpPerCode(Code_Number) / SitesCounter
    
    Call TheExec.Datalog.WriteParametricResult(0, test_num + 2, logTestNoPF, parmPass, "AvgTmpPerCode_Code" & CStr(Code_Number), chan_num_Default, 0, AvgTmpPerCode(Code_Number), 80, unitCustom, 0, unitNone, 0, "", "C")
        
    AvgTmpCrossAll = AvgTmpCrossAll + AvgTmpPerCode(Code_Number)
    
Next Code_Number

test_num = test_num + 1

AvgTmpCrossAll = AvgTmpCrossAll / 8

Call TheExec.Datalog.WriteParametricResult(0, test_num + 3, logTestNoPF, parmPass, "AvgTmpCrossAll", chan_num_Default, 0, AvgTmpCrossAll, 80, unitCustom, 0, unitNone, 0, "", "C")



test_num = test_num + 1

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_calc_calib_result_TMP_SNS_VOLTAGE_TEMPERATURE.Pat").Run("")


'Terminate the calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_CALIB_DONE.PAT").Run("")

End Function
'Eden is here 30.12.2021 20:44 - see you next year

Function Temp_Sens_Internal_Voltage_Calib_VB_BPMU(argc As Long, argv() As String) As Long

'This function does not updated with the TMS connecting per code

Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String

Dim site As Long
Dim Code_Number As Double
Dim RetVoltages() As Double 'New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Voltage As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim RetPinArray() As String
Dim RetPinCnt As Long
Dim RetPinArray_BPMU_Spot_Cal1() As String
Dim RetPinCnt_BPMU_Spot_Cal1 As Long
Dim chan_num_BPMU_Spot_Cal1(59) As Long
Dim chan_num_TMP_SNS(59) As Long
Dim i As Long

Dim ZeroVoltage(59) As Double
Dim BPMU_Sampels As Long

Dim AvgVoltagePerSite(59) As Double
Dim AvgTmpPerSite(59) As Double
Dim AvgTmpCrossAll As Double
Dim SitesCounter As Long
Dim AvgTmpPerCode(7) As Double

test_num = TheExec.Sites.site(0).TestNumber

'Creating local array for the channel num for the TMP_SNS pin - only needed in this function
Call TheExec.DataManager.DecomposePinList("TMP_SNS", RetPinArray, RetPinCnt)
Call TheExec.DataManager.DecomposePinList("BPMU_Spot_Cal1", RetPinArray_BPMU_Spot_Cal1, RetPinCnt_BPMU_Spot_Cal1)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        chan_num_TMP_SNS(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray(site), site, True))
    Next site
End With

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        chan_num_BPMU_Spot_Cal1(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_BPMU_Spot_Cal1(site), site, True))
    Next site
End With



Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

'RetVoltages.ResultType = tlResultTypeParametricValue
TheHdw.PPMU.samples = PPMU_Averaging

'measure zero for BPMU 1
BPMU_Sampels = 64

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Call TheHdw.BPMU.Chans(chan_num_BPMU_Spot_Cal1(CInt(site))).ModeFIMV(bpmu200uA, bpmu2v)
            TheHdw.BPMU.Chans(chan_num_BPMU_Spot_Cal1(CInt(site))).ForceCurrent(bpmu200uA) = 0
            TheHdw.BPMU.Chans(chan_num_BPMU_Spot_Cal1(CInt(site))).ClampVoltage(bpmu2v) = 2
            Call TheHdw.BPMU.Chans(chan_num_BPMU_Spot_Cal1(CInt(site))).Connect
            TheHdw.Wait (0.01)
            Call TheHdw.BPMU.Chans(chan_num_BPMU_Spot_Cal1(CInt(site))).Measure(BPMU_Sampels, RetVoltages)


            ZeroVoltage(site) = 0
            For i = 0 To BPMU_Sampels - 1
                ZeroVoltage(site) = ZeroVoltage(site) + RetVoltages(i)
            Next i
            ZeroVoltage(site) = ZeroVoltage(site) / BPMU_Sampels
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "ZeroVoltage", chan_num_BPMU_Spot_Cal1(site), 0, ZeroVoltage(site), 10, unitVolt, 0, unitAmp, 0)


        End If
    Next site
End With


test_num = test_num + 1


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_ID_7_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_SCAN_VALS.PAT").Run("")

For Code_Number = 0 To 7
    
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1
    
    SitesCounter = 0

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
            
            SitesCounter = SitesCounter + 1
            
            Call TheHdw.BPMU.Chans(chan_num_TMP_SNS(CInt(site))).ModeFIMV(bpmu200uA, bpmu10V)
            TheHdw.BPMU.Chans(chan_num_TMP_SNS(CInt(site))).ForceCurrent(bpmu200uA) = 0
            TheHdw.BPMU.Chans(chan_num_TMP_SNS(CInt(site))).ClampVoltage(bpmu10V) = 10
            Call TheHdw.BPMU.Chans(chan_num_TMP_SNS(CInt(site))).Connect
            TheHdw.Wait (0.01)
            Call TheHdw.BPMU.Chans(chan_num_TMP_SNS(CInt(site))).Measure(BPMU_Sampels, RetVoltages)
    
    
            Result(site) = 0
            For i = 0 To BPMU_Sampels - 1
              Result(site) = Result(site) + RetVoltages(i)
            Next i
            Result(site) = Result(site) / BPMU_Sampels
        
            
    
            AvgVoltagePerSite(site) = Result(site)
                
            Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "AvgVoltagePerSite_Code_" & CStr(Code_Number), chan_num_TMP_SNS(site), 0, AvgVoltagePerSite(site), 5, unitVolt, 0, unitAmp, 0)

            Result(site) = 71.597 * Result(site) - 273.15
            
            AvgTmpPerSite(site) = Result(site)
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestNoPF, parmPass, "AvgTmpPerSite_Code_" & CStr(Code_Number), chan_num_TMP_SNS(site), 0, AvgTmpPerSite(site), 80, unitCustom, 0, unitNone, 0, "", "C")

            AvgTmpPerCode(Code_Number) = AvgTmpPerCode(Code_Number) + AvgTmpPerSite(site)

    
            End If
        Next site
    End With
    
    test_num = test_num + 1
    
    AvgTmpPerCode(Code_Number) = AvgTmpPerCode(Code_Number) / SitesCounter
    
    Call TheExec.Datalog.WriteParametricResult(0, test_num + 2, logTestNoPF, parmPass, "AvgTmpPerCode_Code" & CStr(Code_Number), chan_num_Default, 0, AvgTmpPerCode(Code_Number), 80, unitCustom, 0, unitNone, 0, "", "C")
        
    AvgTmpCrossAll = AvgTmpCrossAll + AvgTmpPerCode(Code_Number)
    
    
Next Code_Number

test_num = test_num + 1

AvgTmpCrossAll = AvgTmpCrossAll / 8

Call TheExec.Datalog.WriteParametricResult(0, test_num + 3, logTestNoPF, parmPass, "AvgTmpCrossAll", chan_num_Default, 0, AvgTmpCrossAll, 80, unitCustom, 0, unitNone, 0, "", "C")


'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_calc_calib_result_TMP_SNS_VOLTAGE_TEMPERATURE.Pat").Run("")

'Terminate the calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_CALIB_DONE.PAT").Run("")

End Function


Public Function Dump_NVM_VB_Ameet(argc As Long, argv() As String) As Long

Dim i As Long
Dim Addr As Long
Dim Init_Addr As Long

'If Debug_Dump_Final And ID_Key_Index > 4 Then   'To perform selective dump during wafer sort at the end of the TPS but not on the first


If True Then ' Force dump
    Debug_Dump_Final = False ' righr now logic supports only the Final
    
    TheHdw.PinLevels.ConnectAllPins
    TheHdw.PinLevels.ApplyPower
    TheHdw.Wait (0.005)
    
    
    'Call Reset_Halt
    ''' ***************   Read static part of D1 ********************
        'MSS NVM
    Init_Addr = 2097152 ' 0x200000
    ' Call ReadRegWithoutPrint2Datalog(Hex(Init_Addr))
    
    For i = 0 To 679 ' + 1 ' might change for D2 Todo change
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    
    'SPU NVM
    Init_Addr = 2100224 ' 200C00
       ' Call ReadRegWithoutPrint2Datalog(Hex(Init_Addr))
    For i = 0 To 127 ' + 1
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    
    'HWS NVM
    Init_Addr = 2100736 ' 200E00
    
      '  Call ReadRegWithoutPrint2Datalog(Hex(Init_Addr))
    For i = 0 To 84 ' + 1
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i

        ''' ***************   Read Dynamic part of D1 ********************

    'NVM - GNVM - our calibrations and more
    Init_Addr = 2099872 ' 2000AA0
      '  Call ReadRegWithoutPrint2Datalog(Hex(Init_Addr))
    For i = 0 To 87 ' + 1
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    

End If

End Function

'#####################################################################################################################################
'# Function name: Dump_NVM_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function Dump_NVM_VB(argc As Long, argv() As String) As Long

Dim i As Long
Dim Addr As Long
Dim Init_Addr As Long

'If Debug_Dump_Final And ID_Key_Index > 4 Then   'To perform selective dump during wafer sort at the end of the TPS but not on the first


If True Then ' Force dump
    Debug_Dump_Final = False ' righr now logic supports only the Final
    
    TheHdw.PinLevels.ConnectAllPins
    TheHdw.PinLevels.ApplyPower
    TheHdw.Wait (0.005)
    
    
    'Call Reset_Halt
    ''' ***************   Read static part of D1 ********************
        'MSS NVM
    Init_Addr = 2097152 ' 0x200000
    For i = 0 To 639 ' might change for D2 Todo change
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    
    'SPU NVM
    Init_Addr = 2100224
    For i = 0 To 127
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    
    'HWS NVM
    Init_Addr = 2100736
    For i = 0 To 127
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i

        ''' ***************   Read Dynamic part of D1 ********************

    'NVM - GNVM - our calibrations and more
    Init_Addr = 2099712
    For i = 0 To 127
        Addr = Init_Addr + i * 4
        'Call TheExec.Datalog.WriteComment("Address = " & (Hex(Addr)))
        Call ReadRegForcePrint2Datalog(Hex(Addr))
    Next i
    

End If

End Function

'#####################################################################################################################################
'# Function name: NVM_Program_Ext_Power_VB
'# Parameters:
'# Description: burning the NVM with all the data we collected so far also burning the DATA_KEYS ID_KEYS and GROUP_ID
'# High Level Flow: running patterns for erase -> running patterns for burning the NVM -> running patterns to read results -> verification with 0V -> verification with -0.7 V
'#####################################################################################################################################
Function NVM_Program_Ext_Power_VB(argc As Long, argv() As String) As Long 'from VerifyNVM in D1 disconnect neg pin with Eli

Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim TDO_Data(59) As String
Dim i As Long
Dim Tmp_Data As String
Dim Chan_num_M4p5 As Long
Dim RetPinCnt_M4p5 As Long
Dim RetPinArray_M4p5() As String
Dim Active_Sites(59) As Boolean
Dim PGM_Voltage As Double
Dim DebugString As String
Dim Nvm_Burn_Enable As Boolean

Dim NVM_PS_Source As Double 'check why we still fail at Wiliot

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)

PGM_Voltage = 4.3

Nvm_Burn_Enable = True

If Nvm_Burn_Enable Then

    TheHdw.PPMU.Pins("Power_4P5V").ForceVoltage(ppmu2mA) = PGM_Voltage
    TheHdw.Pins("Power_4P5V").PPMU.Connect
    
    If Debug_Delay Then
        TheHdw.Wait (0.05)
    Else
        TheHdw.Wait (Defult_Delay_sec)
    End If
    
    If Debug_Printout Then
        Call TheExec.Datalog.WriteComment("#DB19| NVM_PGM Voltage= " & CStr(PGM_Voltage))
    End If
    Call TheExec.DataManager.DecomposePinList("Power_M4P5V", RetPinArray_M4p5, RetPinCnt_M4p5)
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            Chan_num_M4p5 = HPTInfo.Pins.ChannelFromPinSite(RetPinArray_M4p5(site), site, True)
            If (Chan_num_M4p5) >= 0 Then
                TheHdw.Digital.Relays.chan(Chan_num_M4p5).Connect (rlyDisconnect)
            End If
        Next site
    End With
    
    Call TheHdw.PinLevels.Pins("Power_1P15V").ModifyLevel(chVDriveHi, 1.15)
 
    If Debug_Delay Then
        TheHdw.Wait (0.05)
    Else
        TheHdw.Wait (Defult_Delay_sec)
    End If
    
    
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_ID_80_START.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_soc_clk_in.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_init_cfg_init.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_pre_erase_cfg_init.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_0.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_1.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_2.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_3.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_4.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_5.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_6.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_7.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_post_nvm_prog_erase.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_CALIB_DONE.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_ID_81_START.Pat").Run("")
    
    If OutOf_Wafer_touchDown = True Then ' if = True we stay with external negative voltage disconnected as we had so far

        'Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "External_NVM_PS", chan_num_Default, 1, 0, 1, unitNone, 0, unitNone, 0) 'Duplicate printing
        
        
        If Debug_Printout Then
            Call TheExec.Datalog.WriteComment("#DB667| NVM_PGM Internal voltage  ")
        End If
        TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartLo

        If Debug_Delay Then
            TheHdw.Wait (0.05)
        Else
            TheHdw.Wait (Defult_Delay_sec)
        End If
            
        TheHdw.DPS.Pins("LT1617_Power").ForceValue(dpsPrimaryVoltage) = 0 '5 ' 5
        TheHdw.Wait (0.05)
            
    Else 'External negative
    
        TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartHi

        If Debug_Delay Then
            TheHdw.Wait (0.05)
        Else
            TheHdw.Wait (Defult_Delay_sec)
        End If
        
        TheHdw.DPS.Pins("LT1617_Power").ForceValue(dpsPrimaryVoltage) = 5 ' 5
        TheHdw.Wait (0.05)
    
    End If
    
    
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_soc_clk_in.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_pre_nvm_prog_cfg.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_gnvm.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_0_1.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_2_3.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_4_5.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_6_7.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_post_nvm_prog_erase.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_CALIB_DONE.Pat").Run("")
    

    If OutOf_Wafer_touchDown = False Then ' if = True we stay with external negative voltage disconnected as we had so far

        TheHdw.DPS.Pins("LT1617_Power").ForceValue(dpsPrimaryVoltage) = 0
       
        If Debug_Delay Then
            TheHdw.Wait (0.05)
        Else
            TheHdw.Wait (Defult_Delay_sec)
        End If
            
        TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartLo
        
        If Debug_Delay Then
            TheHdw.Wait (0.05)
        Else
            TheHdw.Wait (Defult_Delay_sec)
        End If
    
    End If
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If Active_Sites(site) = True Then
            
                Chan_num_M4p5 = HPTInfo.Pins.ChannelFromPinSite(RetPinArray_M4p5(site), site, True)
                
                If Chan_num_M4p5 >= 0 Then
                    TheHdw.Digital.Relays.chan(Chan_num_M4p5).Connect (rlyPE)
                End If
    
            End If
        Next site
    End With
    
    TheHdw.Wait (0.05)
    
End If 'If burn enabled
    

Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_internal_vdd_low_ID_85_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_internal_vdd_low_CALIB_DONE.Pat").Run("")

'#############################################      External_NVM_PS     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If OutOf_Wafer_touchDown = True Then ' if = True we stay with external negative voltage disconnected as we had so far
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "External_NVM_PS", chan_num_Default, 1, 0, 1, unitNone, 0, unitNone, 0)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "External_NVM_PS", chan_num_Default, 1, 1, 1, unitNone, 0, unitNone, 0)
            End If
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestPass, parmPass, "VEE_Int_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num + 1, logTestFail, parmLow, "VEE_Int_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                
        End If
    Next site
End With

test_num = test_num + 2

'#############################################      VEE_Int_VDD_Low_nvm_static_crc_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 24)
            Else
                TDO_Data(site) = "011010001111111001010111"
            End If
                        
            TDO_Data(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("nvm_static_crc_success = " & CStr(TDO_Data(site)))
            
            If TDO_Data(site) = NVM_CRC Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Int_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                'Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VEE_Int_VDD_Low_nvm_static_crc_success = 1" )
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Int_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                'Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VEE_Int_VDD_Low_nvm_static_crc_success = 0" )
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)

                If Problem_Printout Then
                    Call TheExec.Datalog.WriteComment("#DB88| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " NVM_static_CRC_Failed-CRC" & " = " & TDO_Data(site))
                End If
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      VEE_Int_VDD_Low_GNVM_verify_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 25, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Int_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Int_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            
            If Debug_Printout Then
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " VEE_Int_VDD_Low_GNVM_verify_success = " & CStr(Result(site)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      verify_nvm_programming     #############################################

TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartLo  'chStartHi

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.PPMU.Pins("Power_M4P5V").ForceVoltage(ppmu2mA) = 0
TheHdw.Pins("Power_M4P5V").PPMU.Connect

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_ID_86_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_init_configs.Pat").Run("")

If (TheExec.CurrentChanMap <> "HPT_New_LB") Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_set_external_vee.Pat").Run("")
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_CALIB_DONE.Pat").Run("")


'#############################################      VEE_Ext_VDD_Hi_nvm_static_crc_done     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Hi_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Hi_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
 
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      VEE_Ext_VDD_Hi_nvm_static_crc_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 24)
            Else
                TDO_Data(site) = "011010001111111001010111" '"0068FE57"
            End If
                
            TDO_Data(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("nvm_static_crc_success = " & CStr(TDO_Data(site)))

            If TDO_Data(site) = NVM_CRC Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Hi_nvm_static_crc_success ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = "Passed"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Hi_nvm_static_crc_success ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
                DebugString = "Failed"
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Problem_Printout And TDO_Data(site) <> NVM_CRC Then
                Call TheExec.Datalog.WriteComment("#DB88| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " NVM_static_CRC_" & DebugString & "CRC = " & TDO_Data(site))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      VEE_Ext_VDD_Hi_GNVM_verify_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 25, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Hi_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Hi_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " VEE_Ext_VDD_Hi_GNVM_verify_success = " & CStr(Result(site)))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      verify_nvm_programming_vee_external     #############################################


Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_ID_87_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_init_configs.Pat").Run("")

If (TheExec.CurrentChanMap <> "HPT_New_LB") Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_set_external_vee.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_CALIB_DONE.Pat").Run("")

'#############################################      VEE_Ext_VDD_Low_nvm_static_crc_done     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " VEE_Ext_VDD_Low_nvm_static_crc_done = " & CStr(Result(site)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      VEE_Ext_VDD_Low_nvm_static_crc_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 24)
            Else
                TDO_Data(site) = "011010001111111001010111"  '"0068FE57"
            End If
            
            TDO_Data(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("nvm_static_crc_success = " & CStr(TDO_Data(site)))

            If TDO_Data(site) = NVM_CRC Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = "Passed"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
                DebugString = "Failed"
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Problem_Printout And TDO_Data(site) <> NVM_CRC Then
                Call TheExec.Datalog.WriteComment("#DB88| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " NVM_static_CRC_" & DebugString & "CRC = " & TDO_Data(site))
            End If
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      VEE_Ext_VDD_Low_GNVM_verify_success     #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 25, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_Ext_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_Ext_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      verify_nvm_programming_vee_external     #############################################

TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartLo  'chStartHi

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.PPMU.Pins("Power_M4P5V").ForceVoltage(ppmu2mA) = -0.7
TheHdw.Pins("Power_M4P5V").PPMU.Connect

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If


Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_ID_88_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_init_configs.Pat").Run("")

If (TheExec.CurrentChanMap <> "HPT_New_LB") Then
    'Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_set_external_vee.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_CALIB_DONE.Pat").Run("")


'#############################################      VEE_-0.7V_VDD_Hi_nvm_static_crc_done    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            If TDO_Data(site) = "1" Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Hi_nvm_static_crc_done ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Hi_nvm_static_crc_done ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            
                
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      VEE_-0.7V_VDD_Hi_nvm_static_crc_success    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 24)
            Else
                TDO_Data(site) = "011010001111111001010111"  '"0068FE57"
            End If
            
            TDO_Data(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("nvm_static_crc_success = " & CStr(TDO_Data(site)))

            If TDO_Data(site) = NVM_CRC Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Hi_nvm_static_crc_success ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = "Passed"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Hi_nvm_static_crc_success ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
                DebugString = "Failed"
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Problem_Printout And TDO_Data(site) <> NVM_CRC Then
                Call TheExec.Datalog.WriteComment("#DB88| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " NVM_static_CRC_" & DebugString & "CRC = " & TDO_Data(site))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      VEE_-0.7V_VDD_Hi_GNVM_verify_success    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 25, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Hi_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Hi_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      verify_nvm_programming_vee_external_m0p7    #############################################

       
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_ID_89_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_init_configs.Pat").Run("")

If (TheExec.CurrentChanMap <> "HPT_New_LB") Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_set_external_vee.Pat").Run("")
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_CALIB_DONE.Pat").Run("")

'#############################################      VEE_-0.7V_VDD_Low_nvm_static_crc_done    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If TDO_Data(site) = 1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Low_nvm_static_crc_done ", chan_num_TDO(site), 1, Result(site), 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                
        End If
    Next site
End With

test_num = test_num + 1

'#############################################      VEE_-0.7V_VDD_Low_nvm_static_crc_success    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 1, 24)
            Else
                TDO_Data(site) = "011010001111111001010111"   '"0068FE57"
            End If

            TDO_Data(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("nvm_static_crc_success = " & CStr(TDO_Data(site)))

            If TDO_Data(site) = NVM_CRC Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = "Passed"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Low_nvm_static_crc_success ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
                DebugString = "Failed"
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Problem_Printout And TDO_Data(site) <> NVM_CRC Then
                Call TheExec.Datalog.WriteComment("#DB88| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " NVM_static_CRC_" & DebugString & "CRC = " & TDO_Data(site))
            End If
            
        End If
    Next site
End With
        
test_num = test_num + 1

'#############################################      VEE_-0.7V_VDD_Low_GNVM_verify_success    #############################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 25, 3)
            Else
                TDO_Data(site) = "011"
            End If
            
            Result(site) = CDbl(Bin2Dec(TDO_Data(site)))
            
            If Result(site) = 3 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "VEE_-0.7V_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "VEE_-0.7V_VDD_Low_GNVM_verify_success ", chan_num_TDO(site), 3, Result(site), 3, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: NVM_Verification_VB
'# Parameters:
'# Description: running inner verification on the NVM
'# High Level Flow: run patterns for verification -> read results from this inner test and write them
'#####################################################################################################################################
Function NVM_Verification_VB(argc As Long, argv() As String) As Long

Dim site As Long
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim j As Double

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_ID_82_START.Pat").Load

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity
Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.4)
TheHdw.Wait (0.005)

TheHdw.PPMU.Pins("Power_4P5V").ForceVoltage(ppmu2mA) = 4.4
TheHdw.Pins("Power_4P5V").PPMU.Connect

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.PinLevels.Pins("Power_1P15V").ModifyLevel(chVDriveHi, 1.15)

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.Pins("RLY_Control_m4P5V").StartState = chStartLo  'chStartHi

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.PPMU.Pins("Power_M4P5V").ForceVoltage(ppmu2mA) = 0
TheHdw.Pins("Power_M4P5V").PPMU.Connect

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

For j = 0 To 7 Step 7
    'TheHdw.DPS.Pins("LT1617_Power").ForceValue(dpsPrimaryVoltage) = j / 10
    TheHdw.PPMU.Pins("Power_M4P5V").ForceVoltage(ppmu2mA) = -j / 10

    If Debug_Delay Then
        TheHdw.Wait (0.05)
    Else
        TheHdw.Wait (Defult_Delay_sec)
    End If
    
    '########################################       post_ws_nvm_verification        ########################################

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_init_config.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_disable_ecc.Pat").Run("")
    'Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_high_vdd_trim.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_low_vdd_trim.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_external_vee.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_reset_counters.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_0_1.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_2_3.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_4_5_no_gnvm.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_6_7.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_read_error_rates.Pat").Run("")

'    If Debug_Delay Then    'Ameet commented out 22.12.2021
'        TheHdw.Wait (0.05)
'    Else
'        TheHdw.Wait (Defult_Delay_sec)
'    End If
    
    test_num = test_num + 1
    
    '########################################       LER, VDD Low, VEE        ########################################
                
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
            
                TDO_Data(site) = ""
                
                If TheExec.TesterMode = testModeOnline Then
                    TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 16)
                Else
                    TDO_Data(site) = "00000000000000000"
                End If
                    
                Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
               
                If Calib_Code(site) = 0 Then
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "LER, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 0, Calib_Code(site), 0, unitNone, 0, unitNone, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                Else
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "LER, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 0, Calib_Code(site), 0, unitNone, 0, unitNone, 0)
                    Sites_Failes(site) = True
'                    HPTInfo.Sites.site(site).TestResult = siteFail
'                    Call HPT_ReportResult(site, logTestFail)
                End If
            End If
        Next site
    End With
                
    test_num = test_num + 1
    
    '########################################       LER, VDD Low, VEE        ########################################
                
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                TDO_Data(site) = ""
                
                If TheExec.TesterMode = testModeOnline Then
                    TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 16, 16)
                Else
                    TDO_Data(site) = "0000000000000000"
                End If
                
                Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
               
                If Calib_Code(site) = 0 Then
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "BER, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 0, Calib_Code(site), 0, unitNone, 0, unitNone, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                Else
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "BER, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 0, Calib_Code(site), 0, unitNone, 0, unitNone, 0)
                    Sites_Failes(site) = True
'                    HPTInfo.Sites.site(site).TestResult = siteFail
'                    Call HPT_ReportResult(site, logTestFail)
                End If
            End If
        
        Next site
    End With
    
    test_num = test_num + 1
    
    '########################################       LER, VDD Low, VEE        ########################################
                
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                TDO_Data(site) = ""
                
                If TheExec.TesterMode = testModeOnline Then
                    TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 32, 16)
                Else
                    TDO_Data(site) = "0000001110101000"
                End If
                
                Calib_Code(site) = CDbl(Bin2Dec(TDO_Data(site)))
    
                If Calib_Code(site) = 936 Then
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "LINES_COUNT, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 936, Calib_Code(site), 936, unitNone, 0, unitNone, 0)
                    HPTInfo.Sites.site(site).TestResult = sitePass
                    Call HPT_ReportResult(site, logTestPass)
                Else
                    Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "LINES_COUNT, VDD Low, VEE=" & CStr(-j / 10) & " ", chan_num_TDO(site), 936, Calib_Code(site), 936, unitNone, 0, unitNone, 0)
                    Sites_Failes(site) = True
'                    HPTInfo.Sites.site(site).TestResult = siteFail
'                    Call HPT_ReportResult(site, logTestFail)
                End If
            End If
    
        Next site
    End With
    
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_CALIB_DONE.Pat").Run("")
Next j
        
Call SitesFailes_FinalSetAllFails
        
End Function

'#####################################################################################################################################
'# Function name: Radio_Lo_Vref_TX_Calib_VB
'# Parameters:
'# Description: calibrating the Vref TX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_Lo_Vref_TX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double

Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code_TX As New SiteDouble
Dim Calib_Voltage_TX As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim SitesPerCode(0 To 15, 0 To 59) As Long
Dim ActiveCodes(15) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.85) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetVoltages.ResultType = tlResultTypeParametricValue

'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_ID_37_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_SCAN_VALS.PAT").Run("")


'######################################     initialize     ######################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            Calib_Voltage_TX(site) = Result(site)
            Calib_Code_TX(site) = -1
            ActiveSitesCounter = ActiveSitesCounter + 1
            
        End If
    Next site
End With

'######################################     radio_lo_vref_tx_code_1-15     ######################################

For Code_Number = LO_Vref_TX_MinCode To LO_Vref_TX_MaxCode

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    'TheHdw.Wait (0.001)    'Ameet commented out 22.12.2021

    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.238)
    End If
    
    test_num = test_num + 1

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "code_" & CStr(Code_Number), chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_tx_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                If (Abs(LO_Vref_TX_Target_Volt - Result(site)) < Abs(LO_Vref_TX_Target_Volt - Calib_Voltage_TX(site)) And (Result(site) > LO_Vref_TX_Target_Volt) And Result(site) <= (LO_Vref_TX_Target_Volt + LO_Vref_TX_Upper_Margin)) Then
                    Calib_Voltage_TX(site) = Result(site)
                    Calib_Code_TX(site) = Code_Number
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number


test_num = test_num + 1

'######################################     radio_lo_vref_tx_chosen_code_again     ######################################

'Running the TX correct code per site to calibrate the voltage and measuring

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_calc_calib_result_RADIO_LO_LDO_0p2V_TX.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code_TX(site) <> -1 Then
            indexCode = Calib_Code_TX(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With


For indexCode = 0 To 15
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            If Calib_Code_TX(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_LDO_0p2V_TX_calib_code", chan_num_Default, 0, Calib_Code_TX(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_LDO_0p2V_TX_calib_code", chan_num_Default, 0, Calib_Code_TX(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
                        
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_tx_RADIO_LO_LDO_0p2V_TX_calib_code" & " = " & CStr(Calib_Code_TX(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

'######################################     RADIO_LO_LDO_0p2V_TX_calib_value     ######################################

If RunVerifyMeas Then
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.238)
    End If
Else
    Result = Calib_Voltage_TX
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If Abs(Result(site) - Calib_Voltage_TX(site)) < LO_Vref_TX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_LDO_0p2V_TX_calib_value", chan_num_GPIO4(site), LO_Vref_TX_Target_Volt - LO_Vref_TX_MeasVariance, Result(site), LO_Vref_TX_Target_Volt + LO_Vref_TX_Upper_Margin + LO_Vref_TX_MeasVariance, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_LDO_0p2V_TX_calib_value", chan_num_GPIO4(site), LO_Vref_TX_Target_Volt - LO_Vref_TX_MeasVariance, Result(site), LO_Vref_TX_Target_Volt + LO_Vref_TX_Upper_Margin + LO_Vref_TX_MeasVariance, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_tx_RADIO_LO_LDO_0p2V_TX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Voltage_TX(site)) > LO_Vref_TX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_tx_RADIO_LO_LDO_0p2V_TX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Voltage_TX(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

TheHdw.Wait (0.005)

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref\radio_lo_vref_tx_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_HPM_LO_Init_Measure_VB
'# Parameters:
'# Description: measuring the HPM_Radio_LO_Init current
'# High Level Flow: Running patterns -> measure viltegs => write results
'#####################################################################################################################################

Function Radio_HPM_LO_Init_Measure_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.005)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue
TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_ID_41_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_verify_current_RADIO_HPM_SYM_MODE_CURRENT.Pat").Run("")

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)          '.MeasureVoltages(RetCurrents)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_CALIB_DONE.PAT").Run("")

If Debug_Delay Then
    TheHdw.Wait (0.01)
Else
    TheHdw.Wait (0.03)
End If
    
'##########################################         verify_current_RADIO_HPM_SYM_MODE_CURRENT       ##########################################

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetCurrents.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.000018)
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Idd_Radio_HPM_Lo(site) = Result(site)

            If Result(site) >= Radio_HPM_LO_Init_SYM_MODE_CURRENT_Lowest And Result(site) <= Radio_HPM_LO_Init_SYM_MODE_CURRENT_Highest Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "verify_current_RADIO_HPM_SYM_MODE_CURRENT", chan_num_VddCap(site), Radio_HPM_LO_Init_SYM_MODE_CURRENT_Lowest, Idd_Radio_HPM_Lo(site), Radio_HPM_LO_Init_SYM_MODE_CURRENT_Highest, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "verify_current_RADIO_HPM_SYM_MODE_CURRENT", chan_num_VddCap(site), Radio_HPM_LO_Init_SYM_MODE_CURRENT_Lowest, Idd_Radio_HPM_Lo(site), Radio_HPM_LO_Init_SYM_MODE_CURRENT_Highest, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_init_and_meas_verify_current_RADIO_HPM_SYM_MODE_CURRENT = " & CStr(Idd_Radio_HPM_Lo(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

Call HPT_PPMU_DisconnectAll("VDD_CAP")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_HPM_Lo_Vref_TX_Calib_VB
'# Parameters:
'# Description: calibrating the HPM Vref TX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_HPM_Lo_Vref_TX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code_TX As New SiteDouble
Dim Calib_Voltage_TX As New SiteDouble
Dim Active_Sites(59) As Boolean

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If
    
If TheExec.TesterMode = testModeOnline Then TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("GPIO4").PPMU.Connect
Call HPT_PPMU_ConnectAll("GPIO4")
RetVoltages.ResultType = tlResultTypeParametricValue

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_ID_42_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_SCAN_VALS.PAT").Run("")

'##########################################         initialize       ##########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Calib_Voltage_TX(site) = 0
            Calib_Code_TX(site) = -1
            
        End If
    Next site
End With

'##########################################         1st code To all found      ##########################################

For Code_Number = HPM_LO_Vref_TX_MinCode To HPM_LO_Vref_TX_MaxCode

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1
    
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.000018)
    End If
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, "code_" & CStr(Code_Number), chan_num_GPIO4(site), 0, Result(site), 0.9, unitVolt, 0, unitAmp, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vref_tx_code_" & CStr(Code_Number) & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                'closest to 600mV and in the range 550mV <= Vtarget <= 650 mV
                If (Abs(HPM_LO_Vref_TX_Target_Volt - Result(site)) < Abs(HPM_LO_Vref_TX_Target_Volt - Calib_Voltage_TX(site))) And Result(site) >= (HPM_LO_Vref_TX_Target_Volt - HPM_LO_Vref_TX_Lower_Margin) And Result(site) <= (HPM_LO_Vref_TX_Target_Volt + HPM_LO_Vref_TX_Upper_Margin) Then
                    Calib_Voltage_TX(site) = Result(site)
                    Calib_Code_TX(site) = Code_Number
                End If
                
                If (Result(site) - (HPM_LO_Vref_TX_Target_Volt + HPM_LO_Vref_TX_Upper_Margin)) >= 0 Then  'If we passed 650 mV then we would not find any more codes
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

test_num = test_num + 1

'Running the TX correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_calc_calib_result_RADIO_HPM_LO_LDO_0p2V_TX.Pat").Run("")

'##########################################         RADIO_HPM_LO_LDO_0p2V_TX_calib_code       ##########################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code_TX(site) <> -1 Then
            indexCode = Calib_Code_TX(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            If Calib_Code_TX(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_LDO_0p2V_TX_calib_code", chan_num_Default, 0, Calib_Code_TX(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_LDO_0p2V_TX_calib_code", chan_num_Default, 0, Calib_Code_TX(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vref_tx_RADIO_HPM_LO_LDO_0p2V_TX_calib_code" & " = " & CStr(Calib_Code_TX(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

test_num = test_num + 1

'###########################################        RADIO_HPM_LO_LDO_0p2V_TX_calib_value         ###########################################
If RunVerifyMeas Then
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
       
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetVoltages.Math.Average
        Set Result = AVG_samples.Pins("GPIO4")
    Else
        Result = OfflineFillArray(0.62)
    End If
Else
    Result = Calib_Voltage_TX
End If


'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref\radio_lo_vref_tx_rx_calc_calib_result_RADIO_LO_LDO_0p2V_RX.Pat").Run("")
'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            If Abs(Result(site) - Calib_Voltage_TX(site)) < HPM_LO_Vref_TX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_LDO_0p2V_TX_calib_value", chan_num_GPIO4(site), HPM_LO_Vref_TX_Target_Volt - HPM_LO_Vref_TX_Lower_Margin - HPM_LO_Vref_TX_MeasVariance, Result(site), HPM_LO_Vref_TX_Target_Volt + HPM_LO_Vref_TX_Upper_Margin + HPM_LO_Vref_TX_MeasVariance, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_LDO_0p2V_TX_calib_value", chan_num_GPIO4(site), HPM_LO_Vref_TX_Target_Volt - HPM_LO_Vref_TX_Lower_Margin - HPM_LO_Vref_TX_MeasVariance, Result(site), HPM_LO_Vref_TX_Target_Volt + HPM_LO_Vref_TX_Upper_Margin + HPM_LO_Vref_TX_MeasVariance, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vref_tx_RADIO_HPM_LO_LDO_0p2V_TX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Voltage_TX(site)) > HPM_LO_Vref_TX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vref_tx_RADIO_HPM_LO_LDO_0p2V_TX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Voltage_TX(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref\radio_lo_vref_tx_rx_calc_calib_result_RADIO_LO_LDO_0p2V_TX.Pat").Run("") 'NeedRemove?
'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

'Terminate the calibration
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_CALIB_DONE.PAT").Run("")

'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\general_purpose_write_ADD_4002011cReset.PAT").Load     'NeedRemove?
'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\general_purpose_write_ADD_4002011cReset.PAT").Run("")  'NeedRemove?
If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40060bdc")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_HPM_Lo_Vref_RX_Calib_VB
'# Parameters:
'# Description: calibrating the HPM Vref RX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_HPM_Lo_Vref_RX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

Dim DebugString As String

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.005)
End If
    
TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging ' 10 '10
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

'Measureing code 0
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_ID_44_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_SCAN_VALS.PAT").Run("")


'###########################################       initialize       ###########################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site) but result is always more than 20uA higher so I set it to zero to disable wrong selection of this code
            Calib_Code(site) = -1

        End If
    Next site
End With

'###########################################       code 1 to all found       ###########################################

For Code_Number = HPM_LO_Vref_RX_MinCode To HPM_LO_Vref_RX_MaxCode

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")

    test_num = test_num + 1
    
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
    
                
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.00009)
    End If

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vref_rx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                'closest to 20 uA from below and target higher then 15 uA
                If (Abs(HPM_LO_Vref_RX_Target_Curr - Result(site)) < Abs(HPM_LO_Vref_RX_Target_Curr - Calib_Current(site)) And Result(site) <= HPM_LO_Vref_RX_Target_Curr) And Result(site) >= (HPM_LO_Vref_RX_Target_Curr - HPM_LO_Vref_RX_Lower_Margin) Then    'Finding the correct code
                    Calib_Current(site) = Result(site)
                    Calib_Code(site) = Code_Number
                End If
                
                If (Result(site) - HPM_LO_Vref_RX_Target_Curr) >= 0 Then
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1


If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")


'###########################################        RADIO_HPM_LO_LDO_0p2V_RX_calib_code         ###########################################

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_calc_calib_result_RADIO_HPM_LO_LDO_0p2V_RX.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 31
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            CodeString = "code_" & CStr(Calib_Code(site))
            
            If Calib_Code(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_LDO_0p2V_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_LDO_0p2V_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 31, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                 Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                 'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_hpm_lo_vref_rx_RADIO_HPM_LO_LDO_0p2V_RX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
             End If
            
        End If
    Next site
End With

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

test_num = test_num + 1

'###########################################       RADIO_HPM_LO_LDO_0p2V_RX_calib_value - verify       ###########################################

If RunVerifyMeas Then
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
                
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000035)
    End If
Else
    Result = Calib_Current
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If RunVerifyMeas Then
                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
            End If
            
            If Abs(Result(site) - Calib_Current(site)) < HPM_LO_Vref_RX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_LDO_0p2V_RX_calib_value", chan_num_VddCap(site), HPM_LO_Vref_RX_Target_Curr - HPM_LO_Vref_RX_Lower_Margin - HPM_LO_Vref_RX_MeasVariance, Result(site), HPM_LO_Vref_RX_Target_Curr + HPM_LO_Vref_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_LDO_0p2V_RX_calib_value", chan_num_VddCap(site), HPM_LO_Vref_RX_Target_Curr - HPM_LO_Vref_RX_Lower_Margin - HPM_LO_Vref_RX_MeasVariance, Result(site), HPM_LO_Vref_RX_Target_Curr + HPM_LO_Vref_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_hpm_lo_vref_rx_RADIO_HPM_LO_LDO_0p2V_RX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Current(site)) > HPM_LO_Vref_RX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_hpm_lo_vref_rx_RADIO_HPM_LO_LDO_0p2V_RX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Current(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Terminate the calibration

Call HPT_PPMU_DisconnectAll("VDD_CAP")

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_CALIB_DONE.PAT").Run("")


'Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\general_purpose_write_ADD_4002011cReset.PAT").Load
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\general_purpose_write_ADD_4002011cReset.PAT").Run("")

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40060bdc")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_Lo_Vref_RX_Calib_VB
'# Parameters:
'# Description: calibrating the Vref RX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_Lo_Vref_RX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Code_NVM As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 15, 0 To 59) As Long
Dim ActiveCodes(15) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.85) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (0.05)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

'Measureing code 0
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_ID_39_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_SCAN_VALS.PAT").Run("")

'#########################################          initialize          #########################################


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            Active_Sites(site) = True
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site)
            Calib_Code(site) = -1
            ActiveSitesCounter = ActiveSitesCounter + 1
            
        End If
    Next site
End With


'#########################################          code Firts to ALl found          #########################################

For Code_Number = LO_Vref_RX_MinCode To LO_Vref_RX_MaxCode Step 1

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")

    test_num = test_num + 1

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)

    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result(site) = 0.000059 'A
    End If

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then

                Result(site) = Result(site) - Idd_Radio_Lo(site)
                CodeString = "code_" & CStr(Code_Number)

                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)

                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_rx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If

                If (Abs(LO_Vref_RX_Target_Curr - Result(site)) < Abs(LO_Vref_RX_Target_Curr - Calib_Current(site)) And Result(site) <= LO_Vref_RX_Target_Curr) And Result(site) >= (LO_Vref_RX_Target_Curr - LO_Vref_RX_Lower_Margin) Then
                    Calib_Current(site) = Result(site)
                    Calib_Code(site) = Code_Number
                End If

                If (Result(site) - LO_Vref_RX_Target_Curr) >= 0 Then
                    CalibratedSitesCounter(site) = 1
                End If

            End If
        Next site
    End With
Next Code_Number

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

test_num = test_num + 1


'#########################################          RADIO_LO_LDO_0p2V_RX_calib_code          #########################################

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_calc_calib_result_RADIO_LO_LDO_0p2V_RX.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 15
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
            
            CodeString = "code_" & CStr(Calib_Code(site))
            
            If Calib_Code(site) <> -1 Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_LDO_0p2V_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_LDO_0p2V_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                 Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                 'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_rx_RADIO_LO_LDO_0p2V_RX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
             End If
            
        End If
    Next site
End With

test_num = test_num + 1


TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

'###########################################        RADIO_LO_LDO_0p2V_RX_calib_value - verify        ###########################################

If RunVerifyMeas Then
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
       
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000018) 'A
    End If
Else
    Result = Calib_Current
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If RunVerifyMeas Then
                Result(site) = Result(site) - Idd_Radio_Lo(site)
            End If

            If Abs(Result(site) - Calib_Current(site)) < LO_Vref_RX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_LO_LDO_0p2V_RX_calib_value", chan_num_VddCap(site), LO_Vref_RX_Target_Curr - LO_Vref_RX_Lower_Margin - LO_Vref_RX_MeasVariance, Result(site), LO_Vref_RX_Target_Curr + LO_Vref_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_LO_LDO_0p2V_RX_calib_value", chan_num_VddCap(site), LO_Vref_RX_Target_Curr - LO_Vref_RX_Lower_Margin - LO_Vref_RX_MeasVariance, Result(site), LO_Vref_RX_Target_Curr + LO_Vref_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_rx_RADIO_LO_LDO_0p2V_RX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Current(site)) > LO_Vref_RX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_lo_vref_rx_RADIO_LO_LDO_0p2V_RX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Current(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_calc_calib_result_RADIO_LO_VBP_RX.Pat").Run("")
'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40060AFC")

'Terminate the calibration
Call HPT_PPMU_DisconnectAll("VDD_CAP")

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_CALIB_DONE.PAT").Run("")

'TheExec.Datalog.WriteComment ("After Done")
If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40060AFC")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_HPM_Lo_VBP_RX_Calib_VB
'# Parameters:
'# Description: calibrating the HPM VBP RX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_HPM_Lo_VBP_RX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 15, 0 To 59) As Long
Dim ActiveCodes(15) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.05)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue

'Measureing code 0
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_ID_45_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_SCAN_VALS.PAT").Run("")



'################################################          initialize          ################################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site) but result is always more than 20uA higher so I set it to zero to disable wrong selection of this code
            Calib_Code(site) = -1

        End If
    Next site
End With


'################################################          code 0 To All found          ################################################

For Code_Number = HPM_LO_VBP_RX_MinCode To HPM_LO_VBP_RX_MaxCode

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")
    
    test_num = test_num + 1

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000018)
    End If
    
    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
                CodeString = "code_" & CStr(Code_Number)
                
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)
                
                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_rx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If
                
                'closest to 20 uA from beyond will accept values in 0.000015 <= Vtarget <= 0.000022
                If (Abs(HPM_LO_VBP_RX_Target_Curr - Result(site)) < Abs(HPM_LO_VBP_RX_Target_Curr - Calib_Current(site)) And Result(site) <= HPM_LO_VBP_RX_Target_Curr + HPM_LO_VBP_RX_Upper_Margin) And Result(site) >= (HPM_LO_VBP_RX_Target_Curr - HPM_LO_VBP_RX_Lower_Margin) Then
                    Calib_Current(site) = Result(site)
                    Calib_Code(site) = Code_Number
                End If

                If (HPM_LO_VBP_RX_Target_Curr - Result(site)) >= 0 Then
                    CalibratedSitesCounter(site) = 1
                End If
                
            End If
        Next site
    End With
Next Code_Number

test_num = test_num + 1

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

'################################################          RADIO_HPM_LO_VBP_RX_calib_code          ################################################

'Running the correct code per site to calibrate the voltage and measuring
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_calc_calib_result_RADIO_HPM_LO_VBP_RX.Pat").Run("")

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

For indexCode = 0 To 15
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_config_code_" & CStr(indexCode) & ".Pat").Run("")
        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)
        
    End If
Next indexCode



With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then

            CodeString = "code_" & CStr(Calib_Code(site))
            
            If Calib_Code(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_VBP_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_VBP_RX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                 Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                 'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                 Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_rx_RADIO_HPM_LO_VBP_RX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
             End If
            
        End If
    Next site
End With

test_num = test_num + 1


TheHdw.Digital.Relays.Pins("TMS").Connect (rlyPE)

'There was no delay here
If Debug_Delay_GPIO4 Then
    TheHdw.Wait (0.015) ' there was a delay before
End If

'###########################################        RADIO_HPM_LO_VBP_RX_calib_value         ###########################################
If RunVerifyMeas Then
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
    
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.00004)
    End If
Else
    Result = Calib_Current
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If RunVerifyMeas Then
                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
            End If
            
            If Abs(Result(site) - Calib_Current(site)) < HPM_LO_VBP_RX_MeasVariance Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_VBP_RX_calib_value", chan_num_VddCap(site), Calib_Current(site) - HPM_LO_VBP_RX_MeasVariance, Result(site), Calib_Current(site) + HPM_LO_VBP_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_VBP_RX_calib_value", chan_num_VddCap(site), Calib_Current(site) - HPM_LO_VBP_RX_MeasVariance, Result(site), Calib_Current(site) + HPM_LO_VBP_RX_MeasVariance, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_rx_RADIO_HPM_LO_VBP_RX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            ElseIf Problem_Printout And Abs(Result(site) - Calib_Current(site)) > HPM_LO_VBP_RX_MeasVariance Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB600| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_rx_RADIO_HPM_LO_VBP_RX_Verify_Variance = " & CStr(Abs(Result(site) - Calib_Current(site))) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_calc_calib_result_RADIO_LO_VBP_RX.Pat").Run("")
'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

'Terminate the calibration
'Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_CALIB_DONE.PAT").Run("") ' NeedRemove?

'Terminate the calibration
Call HPT_PPMU_DisconnectAll("VDD_CAP")

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Radio_HPM_Lo_VBP_TX_Calib_VB
'# Parameters:
'# Description: calibrating the HPM VBP TX calib values
'# High Level Flow: initialize -> run all codes till all found -> write results -> config the code for each site -> verify measurment
'#####################################################################################################################################
Function Radio_HPM_Lo_VBP_TX_Calib_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim Code_Number As Double
Dim RetCurrents As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim Calib_Code As New SiteDouble
Dim Calib_Current As New SiteDouble
Dim Active_Sites(59) As Boolean
Dim CodeString As String

Dim SitesPerCode(0 To 31, 0 To 59) As Long
Dim ActiveCodes(31) As Boolean
Dim indexCode As Long
Dim ChanPassArr(59) As Long
Dim i As Long

Dim ActiveSitesCounter As Long
Dim CalibratedSitesCounter(59) As Long

ActiveSitesCounter = 0

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.1)
Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0.9) ' Changed from 850mV to 900mV by Ameet due to D2 sensitivity

If Debug_Delay Then
    TheHdw.Wait (0.2)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If
    

TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
TheHdw.PPMU.samples = PPMU_Averaging
'thehdw.Pins("VDD_CAP").PPMU.Connect
Call HPT_PPMU_ConnectAll("VDD_CAP")
RetCurrents.ResultType = tlResultTypeParametricValue


Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_ID_43_START.PAT").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_SCAN_VALS.PAT").Run("")

'###################################        initialize       ###################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Active_Sites(site) = True
            ActiveSitesCounter = ActiveSitesCounter + 1
            Result(site) = 0
            Calib_Current(site) = 0 ' Ameet 17-2-21 was Result(site) - Idd_Radio_Lo(site) but result is always more than 20uA higher so I set it to zero to disable wrong selection of this code
            Calib_Code(site) = 4    'We will always choose this code
            
        End If
    Next site
End With

'Not running - we always use code 4
'#################################################          code 1 to all found only         #################################################

For Code_Number = HPM_LO_VBP_TX_MinCode To HPM_LO_VBP_TX_MaxCode Step 1

    If SumSitesArray(CalibratedSitesCounter) = ActiveSitesCounter And RunAllCodes = False Then Exit For

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_config_code_" & CStr(Code_Number) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_meas_code_" & CStr(Code_Number) & ".Pat").Run("")

    test_num = test_num + 1

    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)

    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result = OfflineFillArray(0.000018)
    End If

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If Active_Sites(site) = True Then

                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
                Calib_Current(site) = Result(site)
                CodeString = "code_" & CStr(Code_Number)

                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestNoPF, parmPass, CodeString, chan_num_VddCap(site), 0, Result(site), 0.001, unitAmp, 1.1, unitVolt, 0)

                If Debug_Printout Then
                    Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                    'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                    Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_tx_" & CodeString & " = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
                End If


                'if some condition then             | somthing like that
                '   Calib_Code(site) = Code_Number
                '   Calib_Current(site) = Result(site)
                'end if

            End If
        Next site
    End With

Next Code_Number

test_num = test_num + 1

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_calc_calib_result_RADIO_HPM_LO_VBP_TX.Pat").Run("")

'#################################################          RADIO_HPM_LO_VBP_TX_calib_code     #################################################

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True And Calib_Code(site) <> -1 Then
            indexCode = Calib_Code(site)
            ActiveCodes(indexCode) = True
            SitesPerCode(indexCode, site) = chan_num_TMS(site)
        End If
    Next site
End With

'Running the chosen code for all sites sharing same code
For indexCode = 0 To 15
    If ActiveCodes(indexCode) = True Then
    
        For i = 0 To 59: ChanPassArr(i) = SitesPerCode(indexCode, i): Next i
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyPE)
        
        If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
            Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
        End If
        
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_config_code_" & CStr(indexCode) & ".Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_meas_code_" & CStr(indexCode) & ".Pat").Run("")
        
        TheHdw.Digital.Relays.Chans(ChanPassArr).Connect (rlyDisconnect)

    End If
Next indexCode

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If Active_Sites(site) = True Then
        
            CodeString = "code_" & CStr(Calib_Code(site))
        
            If Calib_Code(site) <> -1 Then  'Tomer will update oour patterns for this codes!
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_VBP_TX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_VBP_TX_calib_code", chan_num_Default, 0, Calib_Code(site), 15, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_tx_RADIO_HPM_LO_VBP_TX_calib_code" & " = " & CStr(Calib_Code(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
        
        End If
    Next site
End With

test_num = test_num + 1

If Monitoring_Printout Then Call ReadRegForcePrint2Datalog("40020100")

'#################################################          RADIO_HPM_LO_VBP_TX_calib_value         #################################################
If RunVerifyMeas Then
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
        
    If TheExec.TesterMode = testModeOnline Then
        Set AVG_samples = RetCurrents.Math.Average
        Set Result = AVG_samples.Pins("VDD_CAP")
    Else
        Result(site) = 0.00009
    End If
Else
    Result = Calib_Current
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            If RunVerifyMeas Then
                Result(site) = Result(site) - Idd_Radio_HPM_Lo(site)
            End If
            
            If Result(site) >= HPM_LO_VBP_TX_Target_Curr - HPM_LO_VBP_TX_Lower_Margin And Result(site) <= HPM_LO_VBP_TX_Target_Curr + HPM_LO_VBP_TX_Upper_Margin Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "RADIO_HPM_LO_VBP_TX_calib_value", chan_num_VddCap(site), HPM_LO_VBP_TX_Target_Curr - HPM_LO_VBP_TX_Lower_Margin, Result(site), HPM_LO_VBP_TX_Target_Curr + HPM_LO_VBP_TX_Upper_Margin, unitAmp, 1.1, unitVolt, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "RADIO_HPM_LO_VBP_TX_calib_value", chan_num_VddCap(site), HPM_LO_VBP_TX_Target_Curr - HPM_LO_VBP_TX_Lower_Margin, Result(site), HPM_LO_VBP_TX_Target_Curr + HPM_LO_VBP_TX_Upper_Margin, unitAmp, 1.1, unitVolt, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " radio_HPM_lo_vbp_tx_RADIO_HPM_LO_VBP_TX_calib_value = " & CStr(Result(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

'Updated the D-RAM with the results
'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_calc_calib_result_RADIO_LO_VBP_TX.Pat").Run("") 'NeedRemove?
'TheHdw.Wait (0.005)    'Ameet commented out 22.12.2021

'Terminate the calibration
Call HPT_PPMU_DisconnectAll("VDD_CAP")

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Run("")
End If
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_VBP_TX\radio_hpm_lo_vbp_tx_CALIB_DONE.PAT").Run("")

Call SitesFailes_FinalSetAllFails

End Function


'#####################################################################################################################################
'# Function name: NVM_is_Empty_VB
'# Parameters:
'# Description: checking if the NVM is empty - uses for not writing the NVM twice and killing the chip
'# High Level Flow: run patterns to read specific field from the NVM -> if we can see what written there then it's already burned and we will fail the unit
'#####################################################################################################################################

Function NVM_is_Empty_VB(argc As Long, argv() As String) As Long

Dim site As Long
Dim test_num As Long
Dim TDO_Data(59) As String
Dim PGXSDATA_0(59) As String
Dim PGXSDATA_1(59) As String
Dim Stam(3) As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\check_if_burned_ID_107_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\check_if_burned_CALIB_DONE.Pat").Run("")


'#################################      PGXSDATA_0      #################################
            
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""

            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "0000000000000000000000000000000"
            End If
            
            PGXSDATA_0(site) = Bin2Hex32bits(TDO_Data(site))
            Call TheExec.Datalog.WriteComment("Site " & CStr(site) & " PGXSDATA_0 = " & CStr(PGXSDATA_0(site)))

        End If
    Next site
End With
        
'#################################      PGXSDATA_1      #################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 32, 32)
            Else
                TDO_Data(site) = "00000000000000000000000000000000"
            End If
                
            PGXSDATA_1(site) = Bin2Hex32bits(TDO_Data(site))
            Call TheExec.Datalog.WriteComment("Site " & CStr(site) & " PGXSDATA_1 = " & CStr(PGXSDATA_1(site)))

        End If
    Next site
End With
        
test_num = test_num + 1

'#################################      NVM_is_Empty      #################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If (PGXSDATA_0(site) <> "89BED6AA" And PGXSDATA_1(site) <> "0025428E") Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "NVM_is_Empty ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "NVM_is_Empty ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                
                 If (TheExec.CurrentChanMap = "HPT_New_LB") Then
                    Call Dump_NVM_VB(0, Stam)
                End If
                
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)

            End If
            
        End If
    Next site
End With
                
Call SitesFailes_FinalSetAllFails

End Function

'#####################################################################################################################################
'# Function name: Security_Check_VB
'# Parameters:
'# Description: inner security check
'# High Level Flow: running patterns -> reading results -> checking in range -> writing results
'#####################################################################################################################################
Function Security_Check_VB(argc As Long, argv() As String) As Long

Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble
Dim test_num As Long
Dim TDO_Data(59) As String


TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Security\security_ID_110_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Security\security_CALIB_DONE.Pat").Run("")

'If Debug_Delay Then    'Ameet commented out 22.12.2021
'   TheHdw.Wait (0.05)
'Else
'   TheHdw.Wait (Defult_Delay_sec)
'End If

'###################################            sec_verify_done             ###################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
        
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 1)
            Else
                TDO_Data(site) = "1"
            End If
            
            If TDO_Data(site) = "1" Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "sec_verify_done ", chan_num_TDO(site), 1, 1, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "sec_verify_done ", chan_num_TDO(site), 1, 0, 1, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails
                
End Function

'#####################################################################################################################################
'# Function name: Fail_Of_TempSens_DCDC
'# Parameters:
'# Description: running over the TMP SENSE failed units and failing them - using for burning the NVM even if we failed for future debug
'# High Level Flow: Fail the units that failes in the DCDC test - using global array to pass
'#####################################################################################################################################
Public Function Fail_Of_TempSens_DCDC(argc As Long, argv() As String, Optional Validating_ As Boolean) As Long

Dim site As Long

Dim test_num As Long
Dim DebugString As String

test_num = TheExec.Sites.site(0).TestNumber

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            'It was used to print it with the GPIO4 channel num so I usued it - not sure the default is more legit here
            
            If DCDC_Fails(site) = False Then ' limit fixed to be 600 Ido & tomer 18-2-21 before 1st WS
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "TEMP_SENSE_DCDC_Fail", chan_num_GPIO4(site), 1, 1, 1, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = "Pass"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "TEMP_SENSE_DCDC_Fail", chan_num_GPIO4(site), 1, 0, 1, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
                DebugString = "Fail"
            End If
            
            If Debug_Printout Then
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & CStr(DebugString))
            End If
            
        End If
    Next site
End With


End Function

'#####################################################################################################################################
'# Function name: Fail_Of_WkUp_Ret
'# Parameters:
'# Description: running over the Wkups failed units and failing them - using for burning the NVM even if we failed for future debug
'# High Level Flow: Fail the units that failes in the WKUP tests - using global array to pass
'#####################################################################################################################################
Public Function Fail_Of_WkUp_Ret(argc As Long, argv() As String, Optional Validating_ As Boolean) As Long

Dim site As Long
Dim Chan_num As Long
Dim test_num As Long
Dim DebugString As String

test_num = TheExec.Sites.site(0).TestNumber

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            'It was used to print it with the TDO channel num so I continued - not sure if the default is more legit here
            
            If WkUp_Fails(site) = False Then '
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "WkUp_Ret_Fail", chan_num_TDO(site), 1, 1, 1, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
                DebugString = " WkUp_Ret_Pass"
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "WkUp_Ret_Fail", chan_num_TDO(site), 1, 0, 1, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
                DebugString = " WkUp_Ret_Fail"
            End If
            
            If Debug_Printout Then
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & CStr(DebugString))
            End If
            
        End If
    Next site
End With

End Function

'#####################################################################################################################################
'# Function name: Read_Post_Burn_Flow_Version_VB
'# Parameters:
'# Description: Reading important parametrs after the burning - this are the critical ones
'# High Level Flow: run patterns for reading -> read each parameters and compare to the expected one
'#####################################################################################################################################
Function Read_Post_Burn_Flow_Version_VB(argc As Long, argv() As String) As Long

Dim site As Long
Dim test_num As Long
Dim Chan_num As Long
Dim Calib_Code(59) As Double
Dim TDO_Data(59) As String
Dim FlowVer_Major(59) As String
Dim FlowVer_Minor(59) As String
Dim FlowVer_Local(59) As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm\read_burned_mcu_flow_version_ID_90_START.Pat").Run("")
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm\read_burned_mcu_flow_version_CALIB_DONE.Pat").Run("")

'If Debug_Delay Then    'Ameet commented out 22.12.2021
'    TheHdw.Wait (0.05)
'Else
'    TheHdw.Wait (Defult_Delay_sec)
'End If

'#####################################          burn_flow_version           #####################################

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            FlowVer_Major(site) = ""
            If TheExec.TesterMode = testModeOnline Then
                FlowVer_Major(site) = ReadChanelData(chan_num_TDO(site), 0, 8)
            Else
                FlowVer_Major(site) = "00000100"    '0x4
            End If
            
            FlowVer_Minor(site) = ""
            If TheExec.TesterMode = testModeOnline Then
                FlowVer_Minor(site) = ReadChanelData(chan_num_TDO(site), 8, 8)
            Else
                FlowVer_Minor(site) = "00100000"    '0x20
            End If
            
            FlowVer_Local(site) = FlowVer_Major(site) & FlowVer_Minor(site)
            Calib_Code(site) = CDbl(Bin2Dec(FlowVer_Local(site)))
           
            If Calib_Code(site) = Flow_Version Then '1041 '1025 823
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "burn_flow_version", chan_num_TDO(site), Flow_Version, Calib_Code(site), Flow_Version, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "burn_flow_version", chan_num_TDO(site), Flow_Version, Calib_Code(site), Flow_Version, unitNone, 0, unitNone, 0)
                Sites_Failes(site) = True
'                HPTInfo.Sites.site(site).TestResult = siteFail
'                Call HPT_ReportResult(site, logTestFail)
            End If
        End If
    Next site
End With

Call SitesFailes_FinalSetAllFails
        
End Function

'#####################################################################################################################################
'# Function name: DIODE_IV_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function DIODE_IV_VB(argc As Long, argv() As String) As Long

Dim RetPeriod As Double
Dim test_num As Long
Dim site As Long
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble 'Double
Dim Forced_Current As Double
Dim Delay_Tx_Diode As Double

Dim IV_Loop_Value As Double
Dim IV_Loop_Step As Double
Dim IV_Loop_Start As Double
Dim IV_Loop_Stop As Double
Dim Active_Sites(59) As Boolean

test_num = TheExec.Sites.site(0).TestNumber

IV_Loop_Start = -0.0001
Delay_Tx_Diode = 0.15

TheHdw.Wait (0.005)
TheHdw.PPMU.samples = 10
Forced_Current = 0.00001     'was 0.000015 in V6 V7 target is 0.00005
TheHdw.PPMU.Pins("VDD_DBG").ForceCurrent(ppmu200uA) = IV_Loop_Start
Call TheHdw.PPMU.Pins("VDD_DBG").Connect
TheHdw.Wait (Delay_Tx_Diode)

IV_Loop_Step = 0.00001
IV_Loop_Stop = 0.0001

For IV_Loop_Value = IV_Loop_Start To IV_Loop_Stop Step IV_Loop_Step
    TheHdw.PPMU.Pins("VDD_DBG").ForceCurrent(ppmu200uA) = IV_Loop_Value
    TheHdw.Wait (0.025)
    RetVoltages.ResultType = tlResultTypeParametricValue
    Call TheHdw.Pins("VDD_DBG").PPMU.MeasureVoltages(RetVoltages)

    
    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                Active_Sites(site) = True
                Set AVG_samples = RetVoltages.Math.Average
                Set Result = AVG_samples.Pins("VDD_DBG")
    
               'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
               Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " IV Voltage = " & CStr(Result(site)) & " For current of: " & CStr(IV_Loop_Value))
            
            End If
        Next site
    End With
        
Next IV_Loop_Value
        
Call TheHdw.PPMU.Pins("VDD_DBG").Disconnect

End Function

'eden is here: need to set this test to fit the new changes later.
'#####################################################################################################################################
'# Function name:  capture_test_time_VB
'# Parameters:  none
'# Description: capturing running time of some cummon oparetions
'# High Level Flow:
'#####################################################################################################################################
Function Capture_Test_Time_VB(argc As Long, argv() As String) As Long

Dim ReferenceTime As Double
Dim ElapsedTime As Double
Dim ElapsedTime_Shell As Double

Dim Calib_Code As New SiteDouble
Dim avrageCounter As Double
avrageCounter = 1000

Dim site As Long
Dim test_num As Long
Dim Chan_num As Long
Dim MyResult As Double

Dim RetPinArray_TDI() As String
Dim RetPinArray_TCK() As String
Dim RetPinArray_TMS() As String
Dim Result As New SiteDouble
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim Active_Sites(59) As Boolean
Dim RetPinCnt As Long
Dim RetPinCnt_TDI As Long
Dim RetPinCnt_TCK As Long
Dim RetPinCnt_TMS As Long
Dim AVG_samples As New PinListData

Dim results(50) As Double
Dim results_names(50) As String

'General timers:
results_names(0) = "declerations and respinds - do not use      | General meas |"
results_names(1) = "SetTrigger_SetCapture_ClearFailCount        | General meas |"
results_names(2) = "DecomposePinList_ConnectAllPins_ApplyPower  | General meas |"
results_names(3) = "ModifyLevel VDD_CAP                         | General meas |"
results_names(4) = "ForceCurrent GPIO4                          | General meas |"
results_names(5) = "PPMU.Connect GPIO4                          | General meas |"
results_names(6) = "PPMU.MeasureVoltages GPIO4                  | General meas |"
results_names(7) = "DisconnectPins VDD_DBG                      | General meas |"
results_names(8) = "TheHdw.Wait(0.001)                          | General meas |"
results_names(9) = "ForceVoltage VDD_CAP                        | General meas |"
results_names(10) = "PPMU_Averaging                              | General meas |"
results_names(11) = "tlResultTypeParametricValue                 | General meas |"
results_names(12) = "MeasureCurrents VDD_CAP                     | General meas |"
results_names(13) = "Radio_Clocks_3Patterns                      | General meas |"



'per site timers:

results_names(20) = "AVGsamples_SetResult                       | Per all active sites meas |"
results_names(21) = "Debug_Printout                             | Per all active sites meas |"
results_names(22) = "WriteParametricResult_Pass                 | Per all active sites meas |"
results_names(23) = "WriteParametricResult_Fail                 | Per all active sites meas |"
results_names(24) = "ModifyChanVectorData_10bits                | Per all active sites meas |"
results_names(25) = "ReadChanelData_10bits                      | Per all active sites meas |"
results_names(26) = "Connect_Disconnect_TMS_PerSite             | Per all active sites meas |"
results_names(27) = "Connect_Disconnect_TMS_AllSitesArray       | Per all active sites meas |"


'###################################    declare veriables | General meas |    ###################################

Dim RetPinArray() As String
Dim RetPeriod As Double
test_num = TheExec.Sites.site(0).TestNumber
Call TheExec.DataManager.DecomposePinList("GPIO4", RetPinArray, RetPinCnt)  ' Eden: this is what for?
Dim PrintTime As Double
Dim PatternTime As Double

Dim index As Double
Dim RetCurrents As New PinListData
Dim RetVoltages As New PinListData
Dim Site_counter As Long
Dim i As Long
Dim TDO_Data(59) As String

ElapsedTime = TheExec.Timer(ReferenceTime)
results(0) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################    SetTrigger_SetCapture_ClearFailCount    | General meas |    ###################################

For index = 1 To avrageCounter
    TheHdw.Digital.HRAM.Size = 256
    Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
    Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
    Call TheHdw.Digital.Patgen.ClearFailCount
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(1) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################    DecomposePinList_ConnectAllPins_ApplyPower TMS  | General meas |    ###################################

For index = 1 To avrageCounter
    Call TheExec.DataManager.DecomposePinList("TMS", RetPinArray_TMS, RetPinCnt_TMS)
    Call TheHdw.PinLevels.ConnectAllPins
    Call TheHdw.PinLevels.ApplyPower
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(2) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    ModifyLevel VDD_CAP | General meas |     ###################################

For index = 1 To avrageCounter
    Call TheHdw.PinLevels.Pins("VDD_CAP").ModifyLevel(chVDriveHi, 1.5 * (index / avrageCounter))
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(3) = ElapsedTime
ReferenceTime = TheExec.Timer




'###################################    mod2_shell | General meas - not in results |   ###################################

For index = 1 To avrageCounter
    If index Mod 2 = 0 Then
        'TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
    Else
      '  TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0.00001
    End If
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
ElapsedTime_Shell = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    ForceCurrent GPIO4  | General meas |    ###################################

For index = 1 To avrageCounter
    If index Mod 2 = 0 Then
        TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0
    Else
        TheHdw.PPMU.Pins("GPIO4").ForceCurrent(ppmu20uA) = 0.00001
    End If
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)

ElapsedTime = ElapsedTime - ElapsedTime_Shell
results(4) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    PPMU.Connect GPIO4  | General meas |      ###################################

For index = 1 To avrageCounter
    TheHdw.Pins("GPIO4").PPMU.Connect
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(5) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    PPMU.MeasureVoltages GPIO4  | General meas |    ###################################

For index = 1 To avrageCounter
    Call TheHdw.Pins("GPIO4").PPMU.MeasureVoltages(RetVoltages)
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(6) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    AVGsamples_SetResult | Per site |    ###################################

For index = 1 To avrageCounter

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                Set AVG_samples = RetVoltages.Math.Average
                Set Result = AVG_samples.Pins("GPIO4")
            End If
        Next site
    End With

Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(20) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    DisconnectPins VDD_DBG  | General meas |    ###################################

For index = 1 To avrageCounter
    Call TheHdw.PPMU.Pins("VDD_DBG").Disconnect
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(7) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################     TheHdw.Wait(0.001) | General meas |   ###################################

For index = 1 To avrageCounter
    TheHdw.Wait (0.001)
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(8) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################    ForceVoltage VDD_CAP | General meas |     ###################################

For index = 1 To avrageCounter
    TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu200uA) = 1.1
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(9) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################    PPMU_Averaging  | General meas |     ###################################
For index = 1 To avrageCounter

    TheHdw.PPMU.samples = PPMU_Averaging
    
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(10) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################    connect PPMU    | General meas - already in results |    ###################################

For index = 1 To avrageCounter
    TheHdw.Pins("VDD_CAP").PPMU.Connect
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
ReferenceTime = TheExec.Timer

'###################################    tlResultTypeParametricValue  | General meas |     ###################################

For index = 1 To avrageCounter
    RetCurrents.ResultType = tlResultTypeParametricValue
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(11) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################     MeasureCurrents VDD_CAP | General meas |      ###################################

For index = 1 To avrageCounter
    Call TheHdw.Pins("VDD_CAP").PPMU.MeasureCurrents(RetCurrents)
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(12) = ElapsedTime
ReferenceTime = TheExec.Timer



'###################################     Debug_Printout   | per site |     ###################################

For index = 1 To avrageCounter

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " Capture_Test_Time" & " = " & CStr(results(11)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            
            End If
        Next site
    End With
    
Next index

ElapsedTime = TheExec.Timer(ReferenceTime)
results(21) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################     WriteParametricResult_Pass  | per site |      ###################################

For index = 1 To avrageCounter

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Capture_Time_for_pass ", Chan_num, 0, results(10), 100, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            End If
        Next site
    End With
    
Next index


ElapsedTime = TheExec.Timer(ReferenceTime)
results(22) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################     WriteParametricResult_Fail  | per site |     ###################################


For index = 1 To avrageCounter

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Capture_Time_for_fail ", Chan_num, 1, 0, 1, unitNone, 0, unitNone, 0)
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
            End If
        Next site
    End With
    
Next index


ElapsedTime = TheExec.Timer(ReferenceTime)
results(23) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################     ModifyChanVectorData_10bits  | per site |     ###################################

For index = 1 To avrageCounter / 10

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                Chan_num = HPTInfo.Pins.ChannelFromPinSite(RetPinArray(site), site, True)
                
                TDO_Data(site) = Converted_Temp_Bin     'this one is a global defined in the ext temp function
                For i = 0 To 9 '7
                    'Call TheExec.Datalog.WriteComment("Bit " & CStr(i) & "=" & CStr(Mid(TDI_Data(site), i + 1, 1)))
                    If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").ModifyChanVectorData("modv_radio_clocks_actual_temperature_index", i, Chan_num, Mid(TDO_Data(site), i + 1, 1))
                    Else
                        Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").ModifyChanVectorData("modv_radio_clocks_actual_temperature_index", i * 3, Chan_num, Mid(TDO_Data(site), i + 1, 1))
                    End If
                Next i
            End If
        Next site
    End With

Next index

ElapsedTime = ElapsedTime * 10
ElapsedTime = TheExec.Timer(ReferenceTime)
results(24) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################     Radio_Clocks_3patterns  | General meas |     ###################################

For index = 1 To avrageCounter / 10

    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_ID_50_START.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_SCAN_VALS.Pat").Run("")
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_CALIB_DONE.PAT").Run("")

Next index

ElapsedTime = ElapsedTime * 10
ElapsedTime = TheExec.Timer(ReferenceTime)
results(13) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################     ReadChanelData_10bits  | per site |     ###################################

For index = 1 To avrageCounter / 10

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                
                Chan_num = HPTInfo.Pins.ChannelFromPinSite(RetPinArray(site), site, True)
                TDO_Data(site) = ""
                TDO_Data(site) = ReadChanelData(Chan_num, 0, 10)
        
            End If
        Next site
    End With

Next index

ElapsedTime = ElapsedTime * 10
ElapsedTime = TheExec.Timer(ReferenceTime)
results(25) = ElapsedTime
ReferenceTime = TheExec.Timer


'###################################     Connect_Disconnect_TMS_PerSite  | per site |     ###################################

ReferenceTime = TheExec.Timer

For index = 1 To avrageCounter

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
                            
            TheHdw.Digital.Relays.chan(chan_num_TMS(site)).Connect (rlyPE)
            
            TheHdw.Digital.Relays.chan(chan_num_TMS(site)).Connect (rlyDisconnect)

       Next site
    End With

Next index


ElapsedTime = TheExec.Timer(ReferenceTime)
results(26) = ElapsedTime
ReferenceTime = TheExec.Timer

'###################################     Connect_Disconnect_TMS_AllSitesArray  | General meas |     ###################################

chan_num_TMS(3) = 0
chan_num_TMS(1) = 3000

TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)

For index = 1 To avrageCounter
     
    TheHdw.Digital.Relays.Chans(chan_num_TMS).Connect (rlyPE)
    
    TheHdw.Digital.Relays.Chans(chan_num_TMS).Connect (rlyDisconnect)
                
Next index


ElapsedTime = TheExec.Timer(ReferenceTime)
results(27) = ElapsedTime
ReferenceTime = TheExec.Timer





'###################################        count all the active sites        ###########################

Dim sites_counter As Long
sites_counter = 0

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            sites_counter = sites_counter + 1
        End If
    Next site
End With


'###################################        print all the values        ###########################

TheExec.Datalog.WriteComment ("------> General measurment timers: ")

For index = 0 To 19
    TheExec.Datalog.WriteComment ("------> " & CStr(results_names(index)) & " Time = " & CStr(results(index)) & " [ms]")
Next index

TheExec.Datalog.WriteComment ("------> Per active sites measurment timers: we have " & CStr(sites_counter) & " active sites!")

For index = 20 To 40
    TheExec.Datalog.WriteComment ("------> " & CStr(results_names(index)) & " Time = " & CStr(results(index)) & " [ms]")
Next index

'#########################################      Eden's end      #########################################


End Function


'################################################################################################################################################################################
'################################################################################################################################################################################
'####################################################                                                                       #####################################################
'####################################################                           General Functions                           #####################################################
'####################################################                                                                       #####################################################
'################################################################################################################################################################################
'################################################################################################################################################################################

'#####################################################################################################################################
'# Function name: Keys_Import
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function Keys_Import() As Long

Dim FilePath As String
Dim row_number As Long
Dim LineItems As Variant
Dim ID As String
Dim ID_KEY As String
Dim DATA_KEY As String
Dim GROUP_KEY As String
Dim i As Integer
Dim Key_file_name As String
Dim LineFromFile As String

Key_file_name = Lot_Name & Wafer_Name

FilePath = "..\ID_Files\" & Key_file_name & ".csv" ' move to the mass production mode where we can't change the pattern folder with new ID files. Files are at the ID_Files folder outside the environment

Open FilePath For Input As #1
row_number = 0

Do Until EOF(1) Or row_number > Number_of_Units_For_Touch_Downs
    Line Input #1, LineFromFile
    LineItems = Split(LineFromFile, ",")
    KEYS_ARRAY(row_number).groupId = Mid(LineItems(0), 4, 6)
    KEYS_ARRAY(row_number).tagId = Mid(LineItems(1), 4, 12)
    KEYS_ARRAY(row_number).identityKey = Mid(LineItems(2), 4, 32)
    KEYS_ARRAY(row_number).dataKey = Mid(LineItems(3), 4, 32)
    row_number = row_number + 1
Loop

Close #1

If Debug_Flag = True Then
   ' For i = 0 To (row_number - 1)
    For i = 0 To 5
  '   TheExec.Datalog.WriteComment ("Group ID is: " + KEYS_ARRAY(i).groupId + " , TAG_ID is " + KEYS_ARRAY(i).tagId + " , ID_Key is " + KEYS_ARRAY(i).identityKey + " , Data_Key is " + KEYS_ARRAY(i).dataKey) ' "Data is", KEYS_ARRAY(i).groupId, KEYS_ARRAY(i).tagId, KEYS_ARRAY(i).identityKey, KEYS_ARRAY(i).dataKey
    Next i
End If

Test_Result:
If (row_number >= Number_of_Units_For_Touch_Downs) Then
 
 TheExec.Sites.site(0).TestResult = sitePass
' Call TheExec.Datalog.WriteParametricResult(0, test_num, logTestPass, parmPass, pinname, Chan_num, freq_min, Freq_Value, freq_max, unitHz, 0, unitNone, 0)
End If


End Function


'#####################################################################################################################################
'# Function name: Site0_out_of_Ring_Table_Import
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function Site0_out_of_Ring_Table_Import() As Long

Dim FilePath As String
Dim row_number As Long
Dim LineItems As Variant
Dim ID As String
Dim ID_KEY As String
Dim DATA_KEY As String
Dim GROUP_KEY As String
Dim i As Integer
Dim j As Integer
Dim Key_file_name As String
Dim LineFromFile As String
Dim X_Position As Integer
Dim Y_Position As Integer


'table initialization
For i = 1 To Wafer_X_Size
    For j = 1 To Wafer_Y_Size
        Site0_Lookup_ARRAY(i, j) = 0
    Next j
Next i

FilePath = ".\Patterns\" & "Site0_OutOfRing.csv"

Open FilePath For Input As #1
row_number = 1 ' we shift all index by 1 to corrlate to file index and to check non zero location as most of table is zeros so we don't need to initialize it

Do Until EOF(1)
    Line Input #1, LineFromFile
    LineItems = Split(LineFromFile, ",")
    X_Position = LineItems(0)
    Y_Position = LineItems(1)
    
    Site0_Lookup_ARRAY(X_Position, Y_Position) = 1
   
    row_number = row_number + 1
Loop

Close #1

''' ------------ used for verifiaction by printing all values
'''If Debug_Flag = True Then
'''   ' For i = 0 To Wafer_X_Size
Test_Result:
If (row_number > Number_of_Sites0_out_of_Ring + 1) Then
 
    TheExec.Sites.site(0).TestResult = siteFail
    TheExec.Datalog.WriteComment ("#DB9| Site0 Out of ring File too long")
ElseIf (row_number < Number_of_Sites0_out_of_Ring + 1) Then
    TheExec.Sites.site(0).TestResult = siteFail
    TheExec.Datalog.WriteComment ("#DB8| X Y File too Short")
    
Else
    TheExec.Sites.site(0).TestResult = sitePass
End If

' Call TheExec.Datalog.WriteParametricResult(0, test_num, logTestPass, parmPass, pinname, Chan_num, freq_min, Freq_Value, freq_max, unitHz, 0, unitNone, 0)

End Function


'#####################################################################################################################################
'# Function name: XY_Table_Import
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function XY_Table_Import() As Long

Dim FilePath As String
Dim row_number As Long
Dim LineItems As Variant
Dim ID As String
Dim ID_KEY As String
Dim DATA_KEY As String
Dim GROUP_KEY As String
Dim i As Integer
Dim j As Integer
Dim Key_file_name As String
Dim LineFromFile As String
Dim X_Position As Integer
Dim Y_Position As Integer

Key_file_name = Lot_Name & Wafer_Name

FilePath = ".\Patterns\" & "X-Y_On_Wafer.txt"

Open FilePath For Input As #1
row_number = 1 ' we shift all index by 1 to corrlate to file index and to check non zero location as most of table is zeros so we don't need to initialize it

Do Until EOF(1)
    Line Input #1, LineFromFile
    LineItems = Split(LineFromFile, ",")
    X_Position = LineItems(0)
    Y_Position = LineItems(1)
    
    XY_Lookup_Array(X_Position, Y_Position) = row_number
    row_number = row_number + 1
Loop

Close #1

' ------------ used for verifiaction by printing all values
'If Debug_Flag = True Then
'   ' For i = 0 To (row_number - 1)
'    For i = 0 To 141
'        For j = 0 To 143
'
'            TheExec.Datalog.WriteComment ("index " + CStr(i) + "&" + CStr(j) + ", index = " + CStr(XY_Lookup_Array(i, j)))
'        Next j
'    Next i
'End If

Test_Result:
If (row_number > Number_Of_units_On_Wafer + 1) Then
 
    TheExec.Sites.site(0).TestResult = siteFail
    TheExec.Datalog.WriteComment ("#DB9| X Y File too long")
ElseIf (row_number < Number_Of_units_On_Wafer + 1) Then
    TheExec.Sites.site(0).TestResult = siteFail
    TheExec.Datalog.WriteComment ("#DB8| X Y File too Short")
    
Else
    TheExec.Sites.site(0).TestResult = sitePass
End If

End Function

'#####################################################################################################################################
'# Function name: Reset_Vdd_DBG_VB
'# Parameters:
'# Description: Resseting the VDD_DBG
'# High Level Flow: modify the VDD_DBG to 0 -> wait -> modify the VDD_DBG to 1 -> wait
'#####################################################################################################################################
Public Function Reset_Vdd_DBG_VB(argc As Long, argv() As String) As Long

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 0)
     
If Debug_Delay Then
   TheHdw.Wait (0.1)
Else
   TheHdw.Wait (Defult_Delay_sec)
End If

Call TheHdw.PinLevels.Pins("VDD_DBG").ModifyLevel(chVDriveHi, 1)

If Debug_Delay Then
    TheHdw.Wait (0.5)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

End Function

'#####################################################################################################################################
'# Function name: DoAll_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function DoAll_VB(argc As Long, argv() As String) As Long

Call HPT.HPT_SetDoAll(argv(0))

End Function

'#####################################################################################################################################
'# Function name: Wait_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Function Wait_VB(argc As Long, argv() As String) As Long

TheHdw.Wait (0.1)

End Function

'#####################################################################################################################################
'# Function name: Harvesters_VB
'# Parameters:
'# Description:
'# High Level Flow:
'#####################################################################################################################################
Public Function Harvesters_VB(argc As Long, argv() As String) As Long

On Error Resume Next  'Goto Error_Handler

Dim RetPeriod As Double  ' return period
Dim Vdd_CAP_String As String
Dim Vdd_DBG_String As String
Dim TCK_String As String
Dim site As Long
Dim test_num As Long
Dim RetCurrents As New PinListData 'Dim RetVoltage() As Double
Dim RetVoltages As New PinListData
Dim AVG_samples As New PinListData
Dim Result As New SiteDouble

Dim Harvester_3_voltages(59) As Double
Dim Harvester_5_voltages(59) As Double

Dim FlagsSet As Long
Dim FlagsClear As Long

test_num = TheExec.Sites.site(0).TestNumber
Call SitesFailes_Initialize

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Init.PAT").Run("")

'Force zero on VDD_CAP
TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu20uA) = 0  ' use most accurate range to have more acurate zero current
Call HPT_PPMU_ConnectAll("VDD_CAP")

'Force zero on VDD_DBG
TheHdw.PPMU.Pins("VDD_DBG").ForceVoltage(ppmu20uA) = 0  ' use most accurate range to have more acurate zero current
Call HPT_PPMU_ConnectAll("VDD_DBG")

TheHdw.Wait (0.005) ' discharge capacitors of VDD_DBG and VDD_CAP - TODO check dependence of this time: done no real dependence

' Disconnect all DBG/IO pins to prevent leakge to VDD_CAP (after we have set them all to zero)
TheHdw.Digital.Relays.Pins("VDD_DBG").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("TMS").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("TCK").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("TDI").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("GPIO4").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("GPIO0").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("Power_5P5V").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("Power_4P5V").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("Power_1P15V").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("TDI").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("IND").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("ANT_2").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("ANT_N_1").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("ANT_P_1").Connect (rlyDisconnect)
TheHdw.Digital.Relays.Pins("FS_VDDQ").Connect (rlyDisconnect)

'##########################################      Harvster_Ant_3     ##########################################

TheHdw.PPMU.Pins("VDD_CAP").ForceCurrent(ppmu20uA) = 0
TheHdw.PPMU.samples = PPMU_Averaging
RetCurrents.ResultType = tlResultTypeParametricValue
TheHdw.Wait (0.005)

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Ant3_loop_Differential.PAT").Run("")

'When we want endless loop we use start: Call TheHdw.Digital.Patterns.Pat(PatName2).Start("")
'TheHdw.Wait (0.05) ' Time of Harvesting - TODO chack VDD_CAP voltage and adjust

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureVoltages(RetVoltages)

'stop endless loop pattern      Eden: Do we still need that?
FlagsSet = 0
FlagsClear = cpuA
Call TheHdw.Digital.Patgen.Continue(FlagsSet, FlagsClear)

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.1) 'For Harvester voltages
End If


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Harvester_3_voltages(site) = Result(site)

            If Result(site) >= Harvester_Voltage_Lowest And Result(site) <= Harvester_Voltage_Highest Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Ant_3_Value", chan_num_VddCap(site), Harvester_Voltage_Lowest, Harvester_3_voltages(site), Harvester_Voltage_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Ant_3_Value", chan_num_VddCap(site), Harvester_Voltage_Lowest, Harvester_3_voltages(site), Harvester_Voltage_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                'Call TheHdw.Digital.Timing.chan(TheHdw.Pins(TCK_String).ChanFromSite(site)).readTimeSetPeriod("T0", RetPeriod, 1)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " " & "Harvesters_Ant_3_Value" & " = " & CStr(Harvester_3_voltages(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With

test_num = test_num + 1

'##########################################      Harvster_Ant_5     ##########################################

'Force zero on VDD_CAP
TheHdw.PPMU.Pins("VDD_CAP").ForceVoltage(ppmu20uA) = 0  ' use most accurate range to have more acurate zero current
'Call HPT_PPMU_ConnectAll("VDD_CAP")

TheHdw.Wait (0.001) ' discharge capacitors of VDD_DBG and VDD_CAP - TODO check dependence of this time done no real dependence

TheHdw.PPMU.Pins("VDD_CAP").ForceCurrent(ppmu20uA) = 0
TheHdw.PPMU.samples = PPMU_Averaging

'Call HPT_PPMU_ConnectAll("VDD_CAP") '- Already connected
RetCurrents.ResultType = tlResultTypeParametricValue
TheHdw.Wait (0.001)

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Ant5_loop_Differential.PAT").Run("")

Call TheHdw.Pins("VDD_CAP").PPMU.MeasureVoltages(RetVoltages)

If TheExec.TesterMode = testModeOnline Then
    Set AVG_samples = RetVoltages.Math.Average
    Set Result = AVG_samples.Pins("VDD_CAP")
Else
    Result = OfflineFillArray(0.1) 'For Harvester voltages
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            Harvester_5_voltages(site) = Result(site)

            If Result(site) >= Harvester_Voltage_Lowest And Result(site) <= Harvester_Voltage_Highest Then
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestPass, parmPass, "Ant_5_Value", chan_num_VddCap(site), Harvester_Voltage_Lowest, Harvester_5_voltages(site), Harvester_Voltage_Highest, unitVolt, 0, unitAmp, 0)
                HPTInfo.Sites.site(site).TestResult = sitePass
                Call HPT_ReportResult(site, logTestPass)
            Else
                Call TheExec.Datalog.WriteParametricResult(site, test_num, logTestFail, parmLow, "Ant_5_Value", chan_num_VddCap(site), Harvester_Voltage_Lowest, Harvester_5_voltages(site), Harvester_Voltage_Highest, unitVolt, 0, unitAmp, 0)
                Sites_Failes(site) = True
            End If
            
            If Debug_Printout Then
                Vdd_CAP_String = "VDD_CAP_" & CStr(site): Vdd_DBG_String = "VDD_DBG_" & CStr(site): TCK_String = "TCK_" & CStr(site)
                Call TheExec.Datalog.WriteComment("#DB| " & CStr(Site_Die_ID(site).tagId) & " ULT = " & ULT(site) & " Site = " & CStr(site) & " " & "Harvesters_Ant_5_Value" & " = " & CStr(Harvester_5_voltages(site)) & " VDD_CAP = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_CAP_String).readPinLevels(chVDriveHi), 3))) & " Vdd_DBG = " & (CStr(Round(TheHdw.PinLevels.Pins(Vdd_DBG_String).readPinLevels(chVDriveHi), 3))) & " Freq = " & CStr(Round((RetPeriod), 0)))
            End If
            
        End If
    Next site
End With


Call HPT_PPMU_DisconnectAll("VDD_CAP")
Call HPT_PPMU_DisconnectAll("VDD_DBG")

Call SitesFailes_FinalSetAllFails

End Function

