Datalog report
06/15/2023 09:38:05
      Prog Name:    PIXIE_D4_WS_02p05_Rev1p56_New_ID_Files_Path_V3.xls
       Job Name:    WS_Pixie_D4_HPT_CJTAG
            Lot:    P0XTST
       Operator:    Administrator
      Test Mode:    
      Node Name:    TERADYNEJ9018
      Part Type:     
    Channel map:    HPT_New_LB
    Environment:    

    Site Number:    
         0,  1,  2,  3,  4

    Device#: 13

Wiliot---> Test Mode 
Facility Id is: WILIOT
Running index of TD = 13
#DB123| ULT  |ULT = P0XTST_34_122_67 |Site = 2|touchDown_index = 13
#DB999 prober temp is 
#DB888 Actual temp was set to -777

 Number     Site  Result   Test Name                 Pin                   Channel   Low            Measured       High           Force          Loc
 52          2    PASS     Get_Wafer_Info            tagId                 3000      0.0000         1.9390 K       0.0000         0.0000         0         
 105         2    PASS     HPT_Continuity            TCK                   123       -900.0000 mV   -765.7774 mV   0.0000 V       -100.0000 uA   0         
 106         2    PASS     HPT_Continuity            GPIO4                 122       -900.0000 mV   -456.3702 mV   0.0000 V       -100.0000 uA   0         
 107         2    PASS     HPT_Continuity            VDD_CAP               120       -900.0000 mV   -344.6546 mV   0.0000 V       -100.0000 uA   0         
 150         2    PASS     HPT_PS_Short              Power_4P5V            13        -500.0000 uA   -478.1201 nA   500.0000 uA    150.0000 mV    0         
 151         2    PASS     HPT_PS_Short              VDD_DBG               50        -500.0000 uA   -828.4042 nA   500.0000 uA    150.0000 mV    0         
 152         2    PASS     HPT_PS_Short              VDD_CAP               120       -500.0000 uA   973.0570 nA    500.0000 uA    150.0000 mV    0         
 1120        2    PASS     Harvesters                Ant_3_Value           120       7.0000 mV      7.5061 mV      550.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 Harvesters_Ant_3_Value = 7.50608844264046E-03 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 1121        2    FAIL     Harvesters                Ant_5_Value           120       7.0000 mV      4.3786 mV      550.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 Harvesters_Ant_5_Value = 4.37855159154027E-03 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 161         2    PASS     Ext_Temp                  Measured_temp_MSB     51        0.0000         1.0000         256.0000       0.0000         0         
 162         2    PASS     Ext_Temp                  Measured_temp_LSB     51        0.0000         5.0000         127.0000       0.0000         0         
 163         2    PASS     Ext_Temp                  Measured_temp_Point   51        0.0000         812.5000 m     1.0000         0.0000         0         
 164         2    PASS     Ext_Temp                  Measured_temp         3000      0.0000 C       21.8125 C      85.0000 C      0.0000         0         
#DB999 prober temp is -777
#D888 PC-Sensor temp is 21.8125
 165         2    PASS     Ext_Temp                  Temp_Source           3000      3.0000         3.0000         3.0000         0.0000         0         
 166         2    PASS     Ext_Temp                  Measured_Temperature  3000      -40.0000       21.8125        85.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 TEMP_SENSE_CTAT_Measured_Temperature = 21.8125 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 167         2    PASS     Ext_Temp                  Converted_Temp_Code   3000      0.0000         247.0000       500.0000       0.0000         0         

 Number     Site  Result   Test Name                 Pattern
 180         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_JTAG_RESET_ESCAPE_patched
 181         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_JTAG_RESET_patched
 182         2    FAIL     HPT_Reset_Power_On        RESET_AND_POWER_ON_JTAG_READ_IDCODE_patched
 183         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_CJTAG_OSCAN1_patched
 184         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_CJTAG_READ_IDCODE
 185         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_PIXIE_RESET
 186         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_PIXIE_HALT
 187         2    PASS     HPT_Reset_Power_On        RESET_AND_POWER_ON_TURN_ON_POWER_DOMAINS
 206         2    PASS     Read_SRID                 read_srid             36        1.0280 K       1.0280 K       1.0280 K       0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 Read_SRID_read_srid = 1028 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 207         2    PASS     Read_SRID                 flow_version          36        1.2960 K       1.2960 K       1.2960 K       0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 Read_SRID_Flow_Version_Read_SRID = 1296 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 209         2    PASS     Read_SRID                 Group_ID              3000      0.0000         3.0000         100.0000       0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 Read_SRID_Group_ID = 3 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 210         2    PASS     Verify_MCU_Clocks         verify_load_mcu_for_clocks_calibs_ID_101_START
 211         2    PASS     Verify_MCU_Clocks         verify_load_mcu_for_clocks_calibs_enable_soc_clk_in
 212         2    PASS     Verify_MCU_Clocks         verify_load_mcu_for_clocks_calibs_done_verificaion
 213         2    PASS     Verify_MCU_Clocks         verify_load_mcu_for_clocks_calibs_CALIB_DONE
Site 2 PGXSDATA_0 = 2C785E83
Site 2 PGXSDATA_1 = 82B18204
 216         2    PASS     NVM_is_Empty              NVM_is_Empty          36        1.0000         1.0000         1.0000         0.0000         0         
 221         2    N/A      VDD_0p85_active           code_2                122       600.0000 mV    766.2465 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_2 = 0.766246528519547 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 222         2    N/A      VDD_0p85_active           code_1                122       600.0000 mV    780.9460 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_1 = 0.780945951719718 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 223         2    N/A      VDD_0p85_active           code_14               122       600.0000 mV    816.9126 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_14 = 0.81691262550737 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 224         2    N/A      VDD_0p85_active           code_15               122       600.0000 mV    839.7436 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_15 = 0.839743644520402 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 225         2    N/A      VDD_0p85_active           code_12               122       600.0000 mV    852.8793 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_12 = 0.852879299295022 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 226         2    N/A      VDD_0p85_active           code_13               122       600.0000 mV    865.0767 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_13 = 0.865076693014313 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 227         2    N/A      VDD_0p85_active           code_8                122       600.0000 mV    873.2083 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_8 = 0.873208288827174 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 228         2    N/A      VDD_0p85_active           code_26               122       600.0000 mV    919.8086 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_code_26 = 0.919808587908566 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 229         2    PASS     VDD_0p85_active           vdd_trim_verify_nvm_calib_code  3000      0.0000         2.0000         26.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_vdd_trim_verify_nvm_calib_code = 2 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 230         2    PASS     VDD_0p85_active           vdd_trim_verify_nvm_calib_valu  122       730.0000 mV    769.9996 mV    930.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_vdd_trim_verify_nvm_calib_value = 0.769999572740867 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 231         2    PASS     VDD_0p85_active           vdd_trim_active_calib_code3000      0.0000         8.0000         26.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_vdd_trim_active_calib_code = 8 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 232         2    PASS     VDD_0p85_active           vdd_trim_active_calib_value122       835.0000 mV    871.9573 mV    925.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_vdd_trim_active_calib_value = 0.871957274086734 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 233         2    PASS     VDD_0p85_active           verify_VDD_CAP_960    122       810.0000 mV    863.2002 mV    950.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_verify_VDD_CAP_960 = 0.863200170903653 VDD_CAP = 0.96 Vdd_DBG = 1 Freq = 0
 234         2    PASS     VDD_0p85_active           def_psu_ret_ldo_digital_vdd_tr  36        0.0000         2.0000         7.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_def_psu_ret_ldo_digital_vdd_trim_idx = 2 VDD_CAP = 0.96 Vdd_DBG = 1 Freq = 0
 235         2    PASS     VDD_0p85_active           def_psu_active_ldo_digital_vdd  36        0.0000         2.0000         7.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 VDD_0p85_active_def_psu_active_ldo_digital_vdd_trim_idx = 2 VDD_CAP = 0.96 Vdd_DBG = 1 Freq = 0
 250         2    PASS     system_clocks             mss_done              36        1.0000         1.0000         1.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_mss_done = 1 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 251         2    PASS     system_clocks             calib_success         36        0.0000         15.0000        31.0000        0.0000         0         
 252         2    PASS     system_clocks             calib_success_pass    3000      0.0000         1.0000         2.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_calib_success_pass = 1 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_calib_success = 15 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 253         2    PASS     system_clocks             def_nvm_prog_soc_clk_freq36        0.0000 hz      957.0000 hz    1.0230 Khz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_nvm_prog_soc_clk_freq = 957 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 254         2    PASS     system_clocks             def_ret_soc_clk_div_ctl36        0.0000         1.0000         3.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_ret_soc_clk_div_ctl = 1 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 255         2    PASS     system_clocks             def_ret_soc_clk_cap_ctl36        0.0000         1.0000         7.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_ret_soc_clk_cap_ctl = 1 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 256         2    PASS     system_clocks             def_ret_soc_boot_clk_div_ctl36        0.0000         1.0000         3.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_ret_soc_boot_clk_div_ctl = 1 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 257         2    PASS     system_clocks             soc_clk_freq          36        800.0000 Khz   957.8120 Khz   1.0000 Mhz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_soc_clk_freq = 957812 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 258         2    PASS     system_clocks             def_ret_rtc_coarse_ctl36        0.0000         5.0000         15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_ret_rtc_coarse_ctl = 5 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 259         2    PASS     system_clocks             rtc_clk_freq          36        750.0000 hz    1.0750 Khz     1.5000 Khz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_rtc_clk_freq = 1075 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 260         2    PASS     system_clocks             def_ret_nrgdet_adv_clk_coarse36        0.0000         8.0000         15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_def_ret_nrgdet_adv_clk_coarse = 8 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 261         2    PASS     system_clocks             ret_nrgdet_adv_clk_coarse36        0.0000         13.0000        15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_ret_nrgdet_adv_clk_coarse = 13 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 262         2    PASS     system_clocks             WURX_adv_30_clk_freq  36        35.0000 Khz    40.6250 Khz    50.0000 Khz    0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_WURX_adv_30_clk_freq = 40625 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 263         2    PASS     system_clocks             WURX_adv_100_clk_freq 36        80.0000 Khz    100.7810 Khz   120.0000 Khz   0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 system_clocks_WURX_adv_100_clk_freq = 100781 VDD_CAP = 1.4 Vdd_DBG = 1 Freq = 0
 300         2    PASS     Radio_Calib_Init          radio_calib_init_for_refgens_ID_30_START
 301         2    FAIL     Radio_Calib_Init          radio_calib_init_for_refgens_CALIB_DONE
 311         2    N/A      radio_refgen_0p8V         code_1                122       0.0000 V       759.9915 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p8V_code_1 = 0.759991454817347 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 312         2    N/A      radio_refgen_0p8V         code_2                122       0.0000 V       837.8671 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p8V_code_2 = 0.837867122409741 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 313         2    PASS     radio_refgen_0p8V         RADIO_0p8V_LDO_calib_code3000      0.0000         2.0000         15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p8V_RADIO_0p8V_LDO_calib_code = 2 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 314         2    PASS     radio_refgen_0p8V         RADIO_0p8V_LDO_calib_value122       800.0000 mV    835.0523 mV    875.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p8V_RADIO_0p8V_LDO_calib_value = 0.835052339243751 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 331         2    N/A      radio_refgen_0p6V         code_0_code_2         122       0.0000 V       600.4871 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p6V_code_0_code_2 = 0.600487075411237 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 332         2    N/A      radio_refgen_0p6V         code_0_code_3         122       0.0000 V       618.3140 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p6V_code_0_code_3 = 0.618314035462508 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 333         2    PASS     radio_refgen_0p6V         RADIO_0p6V_SYM_LDO_ret_radio_r  3000      0.0000         0.0000         1.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_ret_radio_refgen_0p6_trim_x2_calib_code = 0 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 334         2    PASS     radio_refgen_0p6V         RADIO_0p6V_SYM_LDO_calib_code3000      0.0000         3.0000         31.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_calib_code = 3 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 335         2    PASS     radio_refgen_0p6V         RADIO_0p6V_SYM_LDO_calib_value  122       600.0000 mV    616.7503 mV    655.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p6V_RADIO_0p6V_SYM_LDO_calib_value = 0.616750267036958 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 832         2    N/A      radio_refgen_0p74V        code_0_code_1         122       0.0000 V       735.5967 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p74V_code_0_code_1 = 0.735596667378765 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 833         2    N/A      radio_refgen_0p74V        code_0_code_2         122       0.0000 V       752.4854 mV    900.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p74V_code_0_code_2 = 0.752485366374706 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 834         2    PASS     radio_refgen_0p74V        RADIO_0p74V_LDO_ret_radio_refg  3000      0.0000         0.0000         1.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p74V_RADIO_0p74V_LDO_ret_radio_refgen_0p74_trim_x2_calib_code = 0 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 835         2    PASS     radio_refgen_0p74V        RADIO_0p74V_LDO_calib_code3000      0.0000         2.0000         31.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p74V_RADIO_0p74V_LDO_calib_code = 2 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 836         2    PASS     radio_refgen_0p74V        RADIO_0p74V_LDO_calib_value122       740.0000 mV    754.0491 mV    795.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_refgen_0p74V_RADIO_0p74V_LDO_calib_value = 0.754049134800256 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 370         2    PASS     radio_aux_mode            verif_verify_0p8V_LDO 122       795.0000 mV    836.6161 mV    910.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_aux_mode_verif_verify_0p8V_LDO = 0.836616107669301 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 371         2    PASS     radio_aux_mode            verif_verify_0p74V_LDO122       735.0000 mV    754.3619 mV    810.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_aux_mode_verif_verify_0p74V_LDO = 0.754361888485366 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 372         2    PASS     radio_aux_mode            verif_verify_0p6V_AUX_LDO122       550.0000 mV    607.3677 mV    670.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_aux_mode_verif_verify_0p6V_AUX_LDO = 0.607367656483657 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 390         2    PASS     radio_lcaux_mode          verif_verify_0p8V_LDO 122       795.0000 mV    836.6161 mV    910.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_mode_verif_verify_0p8V_LDO = 0.836616107669301 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 391         2    PASS     radio_lcaux_mode          verif_verify_0p74V_LDO122       690.0000 mV    746.5430 mV    790.0000 mV    0.0000 A       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_mode_verif_verify_0p74V_LDO = 0.746543046357616 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 1020        2    PASS     radio_lcaux_idac_calib    verify_current_LCAUX_ZERO_CURR  120       0.0000 A       62.9383 uA     120.0000 uA    1.1000 V       0         
 1021        2    N/A      radio_lcaux_idac_calib    verify_current_LCAUX_ZERO_CURR  120       0.0000 A       0.0000 A       1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_verify_current_LCAUX_ZERO_CURRENT = 6.29382781456953E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1022        2    N/A      radio_lcaux_idac_calib    code_0_code_1         120       0.0000 A       3.3527 uA      1.0000 mA      1.1000 V       0         
 1023        2    N/A      radio_lcaux_idac_calib    code_0_code_1_delta_idac120       0.0000 A       3.3527 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_1 = 3.35271950437941E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_1_delta_idac = 3.35271950437941E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1024        2    N/A      radio_lcaux_idac_calib    code_0_code_2         120       0.0000 A       10.7337 uA     1.0000 mA      1.1000 V       0         
 1025        2    N/A      radio_lcaux_idac_calib    code_0_code_2_delta_idac120       0.0000 A       5.3669 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_2 = 1.07337064729759E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_2_delta_idac = 5.36685323648793E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1026        2    N/A      radio_lcaux_idac_calib    code_0_code_3         120       0.0000 A       14.1365 uA     1.0000 mA      1.1000 V       0         
 1027        2    N/A      radio_lcaux_idac_calib    code_0_code_3_delta_idac120       0.0000 A       4.7122 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_3 = 1.41364665669729E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_3_delta_idac = 4.71215552232429E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1028        2    N/A      radio_lcaux_idac_calib    code_0_code_4         120       0.0000 A       18.5150 uA     1.0000 mA      1.1000 V       0         
 1029        2    N/A      radio_lcaux_idac_calib    code_0_code_4_delta_idac120       0.0000 A       4.6288 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_4 = 1.85150181585131E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_4_delta_idac = 4.62875453962829E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1030        2    N/A      radio_lcaux_idac_calib    code_0_code_5         120       0.0000 A       34.5280 uA     1.0000 mA      1.1000 V       0         
 1031        2    N/A      radio_lcaux_idac_calib    code_0_code_5_delta_idac120       0.0000 A       6.9056 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_5 = 3.45280068361461E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_5_delta_idac = 6.90560136722923E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1032        2    N/A      radio_lcaux_idac_calib    code_0_code_6         120       0.0000 A       39.1317 uA     1.0000 mA      1.1000 V       0         
 1033        2    N/A      radio_lcaux_idac_calib    code_0_code_6_delta_idac120       0.0000 A       6.5220 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_6 = 3.91317410809656E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_6_delta_idac = 6.5219568468276E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1034        2    N/A      radio_lcaux_idac_calib    code_0_code_7         120       0.0000 A       42.3343 uA     1.0000 mA      1.1000 V       0         
 1035        2    N/A      radio_lcaux_idac_calib    code_0_code_7_delta_idac120       0.0000 A       6.0478 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_7 = 4.23343388164922E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_0_code_7_delta_idac = 6.04776268807031E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1036        2    N/A      radio_lcaux_idac_calib    code_1_code_1         120       0.0000 A       11.0089 uA     1.0000 mA      1.1000 V       0         
 1037        2    N/A      radio_lcaux_idac_calib    code_1_code_1_delta_idac120       0.0000 A       5.5045 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_1 = 1.10089297158727E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_1_delta_idac = 5.50446485793634E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1038        2    N/A      radio_lcaux_idac_calib    code_1_code_2         120       0.0000 A       38.4562 uA     1.0000 mA      1.1000 V       0         
 1039        2    N/A      radio_lcaux_idac_calib    code_1_code_2_delta_idac120       0.0000 A       9.6140 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_2 = 3.8456193121128E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_2_delta_idac = 9.61404828028199E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1040        2    N/A      radio_lcaux_idac_calib    code_1_code_3         120       0.0000 A       40.9582 uA     1.0000 mA      1.1000 V       0         
 1041        2    N/A      radio_lcaux_idac_calib    code_1_code_3_delta_idac120       0.0000 A       6.8264 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_3 = 4.09582226020081E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_3_delta_idac = 6.82637043366802E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1042        2    N/A      radio_lcaux_idac_calib    code_1_code_4         120       0.0000 A       48.4643 uA     1.0000 mA      1.1000 V       0         
 1043        2    N/A      radio_lcaux_idac_calib    code_1_code_4_delta_idac120       0.0000 A       6.0580 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_4 = 4.84643110446486E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_4_delta_idac = 6.05803888058107E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1044        2    N/A      radio_lcaux_idac_calib    code_1_code_5         120       0.0000 A       53.1181 uA     1.0000 mA      1.1000 V       0         
 1045        2    N/A      radio_lcaux_idac_calib    code_1_code_5_delta_idac120       0.0000 A       5.3118 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_5 = 5.31180858790857E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_5_delta_idac = 5.31180858790857E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1046        2    N/A      radio_lcaux_idac_calib    code_1_code_6         120       0.0000 A       64.5774 uA     1.0000 mA      1.1000 V       0         
 1047        2    N/A      radio_lcaux_idac_calib    code_1_code_6_delta_idac120       0.0000 A       5.3814 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_6 = 6.45773809015168E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_6_delta_idac = 5.38144840845973E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1048        2    N/A      radio_lcaux_idac_calib    code_1_code_7         120       0.0000 A       68.6056 uA     1.0000 mA      1.1000 V       0         
 1049        2    N/A      radio_lcaux_idac_calib    code_1_code_7_delta_idac120       0.0000 A       4.9004 uA      1.0000 mA      1.1000 V       0         
 1050        2    N/A      radio_lcaux_idac_calib    Avg_Delta             3000      0.0000 A       5.7952 uA      1.0000 mA      1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_7 = 6.86056483657338E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_code_1_code_7_delta_idac = 4.90040345469527E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_avg_delta = 5.79517047203415E-06 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1051        2    N/A      radio_lcaux_idac_calib    selected_code_x2      3000      0.0000         0.0000         1.0000         0.0000         0         
 1052        2    N/A      radio_lcaux_idac_calib    selected_code         3000      1.0000         5.0000         7.0000         1.1000         0         
 1053        2    N/A      radio_lcaux_idac_calib    selected_code_value   3000      0.0000 A       40.3232 uA     80.0000 uA     1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_selected_code_x2 = 0 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_selected_code = 5 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_selected_code_value = 4.03231773081803E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1054        2    PASS     radio_lcaux_idac_calib    ret_lcaux_idac_x2_ctl 3000      0.0000         0.0000         1.0000         0.0000         0         
 1055        2    PASS     radio_lcaux_idac_calib    ret_lcaux_idac_ctl    3000      1.0000         6.0000         7.0000         0.0000         0         
 1056        2    PASS     radio_lcaux_idac_calib    calib_value           120       0.0000 A       44.9269 uA     80.0000 uA     1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_ret_lcaux_idac_x2_ctl = 0 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_ret_lcaux_idac_ctl = 6 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_calib_value = 4.49269115529997E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 1060        2    PASS     radio_lcaux_idac_calib    verify_calib_value    120       0.0000 A       43.6509 uA     80.0000 uA     1.1000 V       0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_lcaux_idac_calib_verify_calib_value = 4.36508765177509E-05 VDD_CAP = 1.1 Vdd_DBG = 0.9 Freq = 0
 410         2    PASS     radio_clocks              mss_done              36        1.0000         1.0000         1.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_mss_done = 1 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 411         2    PASS     radio_clocks              calib_success         36        15.0000        15.0000        15.0000        0.0000         0         
 412         2    PASS     radio_clocks              calib_success_pass    3000      0.0000         1.0000         2.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_calib_success = 15 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_calib_success_pass = 1 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 413         2    FAIL     radio_clocks              rx_sym_clk_freq       36        990.0000 Khz   4.2950 Ghz     1.0100 Mhz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_rx_sym_clk_freq = 4294967295 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 414         2    FAIL     radio_clocks              tx_sym_clk_freq       36        990.0000 Khz   4.2950 Ghz     1.0100 Mhz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_tx_sym_clk_freq = 4294967295 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 415         2    FAIL     radio_clocks              aux_clk_freq          36        650.0000 Khz   4.2950 Ghz     1.5000 Mhz     0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_aux_clk_freq = 4294967295 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 416         2    FAIL     radio_clocks              dcdc_clk_freq         36        20.0000 Mhz    4.2950 Ghz     30.0000 Mhz    0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_dcdc_clk_freq = 4294967295 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 417         2    PASS     radio_clocks              def_ret_sym_freq_word_c36        0.0000         15.0000        15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_def_ret_sym_freq_word_c = 15 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 418         2    PASS     radio_clocks              def_ret_sym_freq_word_f36        0.0000         31.0000        31.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_def_ret_sym_freq_word_f = 31 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 419         2    PASS     radio_clocks              def_ret_aux_div_ctl   36        0.0000         3.0000         3.0000         0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_def_ret_aux_div_ctl = 3 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 420         2    PASS     radio_clocks              def_ret_aux_measure   36        0.0000         1.0486 M       1.0486 M       0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_def_ret_aux_measure = 1048575 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0
 421         2    PASS     radio_clocks              def_ret_radio_dcdc_ctl36        0.0000         15.0000        15.0000        0.0000         0         
#DB| 000d40000793 ULT = P0XTST_34_122_67 Site = 2 radio_clocks_def_ret_radio_dcdc_ctl = 15 VDD_CAP = 1.1 Vdd_DBG = 1 Freq = 0

 Site Failed tests/Executed tests
------------------------------------
    2        54       132

 Site    Sort     Bin
------------------------------------
    2        41         5
=========================================================================
