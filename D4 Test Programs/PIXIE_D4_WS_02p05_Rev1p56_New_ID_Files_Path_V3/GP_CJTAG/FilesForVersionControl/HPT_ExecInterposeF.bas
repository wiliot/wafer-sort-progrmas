Attribute VB_Name = "HPT_ExecInterposeF"

' Revision History:
' Date        Description
' 2009-12-04  Leo Di Bello Commented out reference rebuild in HPT_PreRun to avoid global variables to be reset
' 09/14/05    Boopathi P Integrated HPT changes got from Sau Lau for Shmoo
' 09/12/05    Boopathi P Integrated HPT changes got from Thomas Fanther in 3.40.14

' =====================================================
' IMPORTANT:
' The "HPT_ .." functions below are to be run to setup the HPT environement
' and prepare the flow execution.
' They should be copied and placed in the specified Exec Interpose Functions,
' in the user VBA project
' =====================================================

Option Explicit

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function OnProgramLoaded() As Long
  Dim i As Long

  ' temporary solution to make APMU template work with 3.40.09
Call ReLoad_HPT_Reference

  ' Used to build virtual HPT Site and Flow structures
Call HPT_Init

If HPT_is_ON = False Then
    For i = 1 To Application.CommandBars.count
        If Application.CommandBars.Item(i).name = "HPT" Then
            HPTBar.Delete
            Exit For
        End If
    Next i
End If


Call TheHdw.Digital.ACCalExcludePins("ALL_PINS")
' Debug_Dump_Initial = True ' righr now logic supports only the Final 'To perform selective dump during wafer sort at the begining of the TP and at the begining of the WS but not on the first
Debug_Dump_Final = True  'To perform selective dump during wafer sort at the end of the TPS but not on the first


OnProgramLoaded = TL_SUCCESS

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function OnProgramValidated() As Long
 Dim i As Long

  ' temporary solution to make APMU template work with 3.40.09
  ' Call ReLoad_HPT_Reference ' Do not execute this call if you are not useing V3.40.09, since it will reset your global variables

  ' Used to build virtual HPT Site and Flow structures
Call HPT_Init

If HPT_is_ON = False Then
  For i = 1 To Application.CommandBars.count
    If Application.CommandBars.Item(i).name = "HPT" Then
      HPTBar.Delete
      Exit For
    End If
  Next i
End If

'Eden: Adding that we will read the Wiliot_ID as the global counter for us - we will read him once and then will write him +1 after every running of the WS program.
' this was a bug: ID_Counter_FilePath = ".\Patterns\Unit_Index.txt"
ID_Counter_FilePath = "..\Wiliot_Tester_ID_Index\Unit_Index.txt" ' this is correct Index is common for all versions -Ameet 23-3-22
Dim ID_FromFile As String

Open ID_Counter_FilePath For Input As #1
Do Until EOF(1)
    Line Input #1, ID_FromFile
    Wiliot_ID_File_Index = MyStringToInt(ID_FromFile)
    'MsgBox (Wiliot_ID_File_Index)
Loop
Close #1

'Verify under develop
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_ID_10_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_soc_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_rtc_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_verify_freq_wurx_clk.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_verify_CALIB_DONE.Pat").Load

'Harvetsers
Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Init.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Ant3_loop_Differential.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\Harvester\Harvester_Ant5_loop_Differential.PAT").Load

'Loading patterns
Call TheHdw.Digital.Patterns.Pat(".\Patched_Patterns\RESET_AND_POWER_ON_JTAG_RESET_ESCAPE_patched.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patched_Patterns\RESET_AND_POWER_ON_JTAG_RESET_patched.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patched_Patterns\RESET_AND_POWER_ON_JTAG_READ_IDCODE_patched.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patched_Patterns\RESET_AND_POWER_ON_CJTAG_OSCAN1_patched.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\RESET_AND_POWER_ON_CJTAG_READ_IDCODE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\RESET_AND_POWER_ON_PIXIE_RESET.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\RESET_AND_POWER_ON_PIXIE_HALT.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\RESET_AND_POWER_ON_TURN_ON_POWER_DOMAINS.Pat").Load


'VDD 0.85V Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_ID_0_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_SCAN_VALS.PAT").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(2) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(2) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(1) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(1) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(14) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(14) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(15) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(15) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(12) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(12) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(13) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(13) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(8) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(8) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(26) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(26) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_config_code_" & CStr(25) & ".PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_meas_code_" & CStr(25) & ".PAT").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_verify_VDD_CAP_960.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_calc_calib_result_vdd_trim_verify_nvm.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_calc_calib_result_vdd_trim_active.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\0p85_Cal\VDD_0p85_active_CALIB_DONE.PAT").Load

'System Clocks Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_ID_10_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\System_Clocks\system_clocks_CALIB_DONE.PAT").Load

'Temp Sens Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_ID_5_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_SCAN_VALS.PAT").Load

For i = 0 To 9
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_meas_code_" & CStr(i) & ".PAT").Load
Next i

For i = 16 To 25
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_meas_code_" & CStr(i) & ".PAT").Load
Next i

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_calc_calib_result_TEMP_SENSE_DCDC.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_verify_temp_sens_dcdc_vsup_reg.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sens\TEMP_SENSE_DCDC_CALIB_DONE.PAT").Load

'Radio Refgen 0.8V
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_ID_31_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_SCAN_VALS.PAT").Load
For i = 0 To 15
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_calc_calib_result_RADIO_0p8V_LDO.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p8\radio_refgen_0p8V_CALIB_DONE.PAT").Load

'Radio Refgen 0.6V
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_ID_32_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_SCAN_VALS.PAT").Load
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_config_code_0_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_meas_code_0_" & CStr(i) & ".PAT").Load
Next i
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_config_code_1_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_meas_code_1_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_calc_calib_result_RADIO_0p6V_SYM_LDO.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p6\radio_refgen_0p6V_CALIB_DONE.PAT").Load

'Radio Refgen 0.74V
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_ID_33_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_SCAN_VALS.PAT").Load
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_config_code_0_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_meas_code_0_" & CStr(i) & ".PAT").Load
Next i
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_config_code_1_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_meas_code_1_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_calc_calib_result_RADIO_0p74V_LDO.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Refgen_0p74\radio_refgen_0p74V_CALIB_DONE.PAT").Load

'Radio Aux Mode verification
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_ID_34_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p6V_AUX_LDO.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p8V_LDO.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Aux_Mode\radio_aux_mode_verif_verify_0p74V_LDO.PAT").Load

'Radio LC Aux Mode verification
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_ID_35_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_verify_0p8V_LDO.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Mode\radio_lcaux_mode_verif_verify_0p74V_LDO.PAT").Load

'Radio Clocks Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_ID_50_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_CALIB_DONE.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Clocks\radio_clocks_temperature_write.Pat").Load


'Radio Lo Init
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_ID_36_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_verify_current_RADIO_SYM_MODE_CURRENT.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Init\radio_lo_init_and_meas_CALIB_DONE.PAT").Load

'Radio Lo Vref RX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_ID_39_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_SCAN_VALS.PAT").Load
For i = 0 To 15
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_calc_calib_result_RADIO_LO_LDO_0p2V_RX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_RX\radio_lo_vref_rx_CALIB_DONE.PAT").Load

'Radio Lo Vref TX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_ID_37_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_SCAN_VALS.PAT").Load
For i = 0 To 15
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_calc_calib_result_RADIO_LO_LDO_0p2V_TX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_Vref_TX\radio_lo_vref_tx_CALIB_DONE.PAT").Load

'Radio Lo VBP TX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_ID_38_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_SCAN_VALS.PAT").Load
For i = 1 To 31 Step 2
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_calc_calib_result_RADIO_LO_VBP_TX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_TX\radio_lo_vbp_tx_CALIB_DONE.PAT").Load

'Radio Lo VBP RX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_ID_40_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_SCAN_VALS.PAT").Load
For i = 1 To 31 Step 2
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_calc_calib_result_RADIO_LO_VBP_RX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Lo_VBP_RX\radio_lo_vbp_rx_CALIB_DONE.PAT").Load

'Wkup Level Ret Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_ID_61_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_wkup_end_of_idle.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_run_internal_calib.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_read_calib_success.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret\wkup_level_ret_calib_CALIB_DONE.Pat").Load

'Wkup Level Ret Active Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_ID_62_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_wkup_end_of_idle.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Ret_Active\wkup_level_ret_calib_in_active_CALIB_DONE.Pat").Load

'Wkup Level Active Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_ID_63_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_wkup_end_of_idle.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Wkup_Level_Active\wkup_level_active_calib_CALIB_DONE.Pat").Load

'BUI Vstart Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_ID_62_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_set_high_vdd_cap.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_end_of_idle.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\BUI_Vstart_Ref\bui_vstart_ref_CALIB_DONE.Pat").Load

''''''''RVL Offset Calibration
'''''''Call TheHdw.Digital.Patterns.Pat(".\Patterns\RVL_Offset\rvl_offset_ID_63_START.Pat").Load
'''''''Call TheHdw.Digital.Patterns.Pat(".\Patterns\RVL_Offset\rvl_offset_set_high_vdd_cap.Pat").Load
'''''''Call TheHdw.Digital.Patterns.Pat(".\Patterns\RVL_Offset\rvl_offset_CALIB_DONE.Pat").Load

'Read SRID

Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\WR_RD_REG_TEST_ID_100_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\WR_RD_REG_TEST_CALIB_DONE.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\SRID\READ_SRID_ID_105_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\SRID\READ_SRID_CALIB_DONE.Pat").Load

'WR_RD in the Read SRID INIT patterns:
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_soc_clk_in.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_analog_cfg.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_dram_net_go.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_load_mcu_clocks_calib.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_ret_from_gnvm.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\wr_rd\INIT_CALIB_DONE.Pat").Load

'Temp Sense CTAT Calibration
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_ID_6_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_run_internal_calib.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_read_calib_success.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_CTAT\TEMP_SENSE_CTAT_CALIB_DONE.Pat").Load

'Verify Device Parameters
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_ID_98_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_DATA_KEY.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_ID_KEY.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_PGXDID.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_wafer_info.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\verify_device_specific_params_wiliot_uid.Pat").Load

'Write Device Parameters
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_ID_98_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").Load

'NVM New Splited patterns
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_ID_80_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_soc_clk_in.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_init_cfg_init.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_pre_erase_cfg_init.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_0.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_4.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_6.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_erase_sector_7.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_post_nvm_prog_erase.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\erase_nvm_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_ID_81_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_soc_clk_in.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_pre_nvm_prog_cfg.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_gnvm.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_0_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_2_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_4_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_sect_6_7.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_post_nvm_prog_erase.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\burn_nvm_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_ID_86_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_set_external_vee.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_ID_87_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_set_external_vee.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_ID_88_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_set_external_vee.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_ID_89_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_set_external_vee.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_internal_vdd_low_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_internal_vdd_low_ID_85_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\check_if_burned_ID_107_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\check_if_burned_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\read_burned_mcu_flow_version_ID_90_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\read_burned_mcu_flow_version_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_high_init_configs.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_0_vdd_low_init_configs.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_high_init_configs.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\NVM\verify_nvm_programming_vee_external_m0p7_vdd_low_init_configs.Pat").Load
'ROM CRC
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\reset_post_nvm_burn_ID_92_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\RESET_AND_POWER_ON_POST_NVM_BURN.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\reset_post_nvm_burn_CALIB_DONE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\rom_crc_check_ID_90_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\ROM\rom_crc_check_CALIB_DONE.Pat").Load

'Temperature Sensor Internal Voltage
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_ID_7_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_SCAN_VALS.Pat").Load
'Ameet 29-3-2022 not in patterns folder
'''For i = 0 To 7
'''    Call thehdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_config_code_" & CStr(i) & ".PAT").Load
'''    Call thehdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_meas_code_" & CStr(i) & ".PAT").Load
'''Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_calc_calib_result_TMP_SNS_VOLTAGE_TEMPERATURE.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Temp_Sense_Internal_Voltage\temp_sense_internal_voltage_CALIB_DONE.Pat").Load

'Post NVM Verfication
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_ID_82_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_init_config.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_disable_ecc.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_high_vdd_trim.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_low_vdd_trim.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_set_external_vee.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_reset_counters.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_0_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_2_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_4_5_no_gnvm.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_verify_sect_6_7.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_read_error_rates.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\nvm_verification\post_ws_nvm_verification_CALIB_DONE.Pat").Load

'Radio HPM Lo Init
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_ID_41_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_verify_current_RADIO_HPM_SYM_MODE_CURRENT.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Init\radio_hpm_lo_init_and_meas_CALIB_DONE.PAT").Load

'Radio HPM Lo Vref RX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_ID_44_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_SCAN_VALS.PAT").Load
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_calc_calib_result_RADIO_HPM_LO_LDO_0p2V_RX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_RX\radio_hpm_lo_vref_rx_CALIB_DONE.PAT").Load

'Radio HPM Lo Vref TX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_ID_42_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_SCAN_VALS.PAT").Load
For i = 0 To 31
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_calc_calib_result_RADIO_HPM_LO_LDO_0p2V_TX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_Hpm_Lo_Vref_TX\radio_hpm_lo_vref_tx_CALIB_DONE.PAT").Load

'Radio HPM Lo VBP TX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_ID_43_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_SCAN_VALS.PAT").Load
For i = 0 To 15
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_calc_calib_result_RADIO_HPM_LO_VBP_TX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_TX\radio_hpm_lo_vbp_tx_CALIB_DONE.PAT").Load

'Radio HPM Lo VBP RX
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_ID_45_START.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_SCAN_VALS.PAT").Load
For i = 0 To 15
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_config_code_" & CStr(i) & ".PAT").Load
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_meas_code_" & CStr(i) & ".PAT").Load
Next i
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_calc_calib_result_RADIO_HPM_LO_VBP_RX.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_HPM_Lo_VBP_RX\radio_hpm_lo_vbp_rx_CALIB_DONE.PAT").Load

'Security Check
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Security\security_ID_110_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Security\security_CALIB_DONE.PAT").Load


'LC_Aux_Idac
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_ID_46_START.pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_SCAN_VALS.PAT").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_verify_current_LCAUX_ZERO_CURRENT.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_4.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_6.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_0_7.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_4.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_6.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_config_code_1_7.Pat").Load

'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_0.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_4.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_6.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_0_7.Pat").Load

'Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_0.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_1.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_2.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_3.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_4.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_5.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_6.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_meas_code_1_7.Pat").Load

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_calc_calib_result_LCAUX_IDAC_CALIB.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_LC_Aux_Idac\radio_lcaux_idac_calib_CALIB_DONE.Pat").Load


'Radio_FLL_Measuremet_Of_LO_Frequency

Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_ID_45_START.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_SCAN_VALS.Pat").Load
Call TheHdw.Digital.Patterns.Pat(".\Patterns\Radio_FLL_Meas_Lo\radio_fll_meas_lo_CALIB_DONE.Pat").Load

'General
If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then          'CJTAG_Flag = False Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\JTAG_Reset.Pat").Load
End If


Call TheHdw.Digital.ACCalExcludePins("ALL_PINS")

Call Site0_out_of_Ring_Table_Import


'ID and ID file handling variables
    ID_Key_Index = 0
    First_Wafer = True
    X_Y_Table_Loaded = False
    Current_Wafer_ID = "77" ' to force uploade of ID file
    Lot_Name = "AMEETL"
    Wafer_Name = "AMEETW"
    Valid_Prober_temp = False
    
    Touch_Down_Index = 0


'VDD_0p85_Lookup_Array = Array(14, 15, 12, 13, 8, 26, 25)
'myArray = Array(14, 15, 12, 13, 8, 26, 25)
OnProgramValidated = TL_SUCCESS

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function OnProgramStarted() As Long

    ' Used to init site results and prepare datalogging
    Call HPT_PreRun
    OnProgramStarted = TL_SUCCESS

    ' Clear Isolate Site toolbar
    Call HPT_ClearIsolateToolbar
    If Print_Time Then
        TheExec.Datalog.WriteComment ("---> Touchdown test started at " & CStr(Now) & " <----")
    End If
End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function OnProgramEnded() As Long

    If HPT_is_ON = False Then Call TheExec.Datalog.ExtendSiteReportingEndTestRun

    ' Clear Isolate Site toolbar
    Call HPT_ClearIsolateToolbar
    If Print_Time Then
        TheExec.Datalog.WriteComment ("---> Touchdown test ended at " & CStr(Now) & " <----")
    End If

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Sub ReLoad_HPT_Reference()

On Error Resume Next
Dim i As Integer
Dim RemoveWhat As String
RemoveWhat = "HPT"
    'RemoveReference Workbooks(1).VBProject, "HPT"

    With Workbooks(1).VBProject
        On Error GoTo removept
        For i = .references.count To 1 Step -1      ' remove broken references
            If .references(i).IsBroken Then
removept:      .references.Remove .references(i)
            Else
                If UCase(.references(i).name) = UCase(RemoveWhat) Then
                    .references.Remove .references(i)
                End If
            End If
        Next i

    End With

    With Workbooks(1).VBProject
     Debug.Print "Hallo"
        .references.AddFromFile "HPT.xla"
    End With


End Sub


