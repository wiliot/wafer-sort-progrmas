Attribute VB_Name = "General_Functions"

Option Explicit
' Privte Declare Function AllucateUID Lib "C:\Packages\wiliot-debug-jtag-tool\PixieIntegration\src\PixieTools.py" Alias TesterAllucateUID (ByVal packetVersion As String, ByVal flowVersion As String, ByVal prefix As String, ByVal env As String) As String

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function ShellRun(sCmd As String) As String

    'Run a shell command, returning the output as a string

    Dim oShell As Object
    Set oShell = CreateObject("WScript.Shell")

    'run command
    Dim oExec As Object
    Dim oOutput As Object
    Set oExec = oShell.Exec(sCmd)
    Set oOutput = oExec.StdOut

    'handle the results as they are written to and read from the StdOut object
    Dim s As String
    Dim sLine As String
    While Not oOutput.AtEndOfStream
        sLine = oOutput.ReadLine
        If sLine <> "" Then s = s & sLine & vbCrLf
    Wend

    ShellRun = s

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Function AllucateUID(ByVal Flow_Version As String, ByVal packet_version As String, ByVal prefix As String, ByVal env As String) As String
    Dim RetVal As String
    Dim cmd As String
    RetVal = 0
    cmd = "python C:\Packages\wiliot-debug-jtag-tool\PixieIntegration\src\PixieTools.py -fv " + Flow_Version + " -pv " + packet_version + " -p " + prefix + " -env " + env
    RetVal = ShellRun(cmd)
    
'    Call TheExec.Datalog.WriteComment("die ID = " & CStr(RetVal))
'    With Workbooks(1).VBProject
'    Debug.Print RetVal
'    End With
    AllucateUID = RetVal
End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Function Bin2Hex32bits(BinNum As String) As String

Dim BinLen As Double
Dim MidBimNum As String
Dim i As Double
Dim j As Double
Dim HexNum As Double

BinLen = Len(BinNum)

If BinLen < 32 Then
    For i = BinLen To 32 - 1
      BinNum = "0" & BinNum
    Next i
End If

For j = 1 To 32 Step 4
  MidBimNum = Mid(BinNum, j, 4)
  HexNum = 0
  For i = 4 To 1 Step -1 'BinLen
    If Mid(MidBimNum, i, 1) And 1 Then 'BinNum
      HexNum = HexNum + 2 ^ Abs(i - 4) 'BinLen
    End If
  Next i

  Bin2Hex32bits = Bin2Hex32bits & Hex(HexNum)
Next j

End Function
'#####################################################################################################################################
'# Function name: Bin2Dec
'# Parameters: Binary number as string like "LLHHL"
'# Description: runnng over this stering and converting it to a decimal number returned
'#####################################################################################################################################
Function Dec2Bin(ByVal DecimalIn As Variant, Optional NumberOfBits As Variant) As String

Dec2Bin = ""
DecimalIn = Int(CDec(DecimalIn))
Do While DecimalIn <> 0
   Dec2Bin = Format$(DecimalIn - 2 * Int(DecimalIn / 2)) & Dec2Bin
   DecimalIn = Int(DecimalIn / 2)
Loop
If Not IsMissing(NumberOfBits) Then
   If Len(Dec2Bin) > NumberOfBits Then
      Dec2Bin = "Error = Number exceeds specified bit size"
   Else
      Dec2Bin = Right$(String$(NumberOfBits, "0") & Dec2Bin, NumberOfBits)
   End If
End If

End Function

'#####################################################################################################################################
'# Function name: Bin2DecDouble_LH
'# Parameters: binary number as string
'# Description: returning the decimal number as double
'#####################################################################################################################################
Function Bin2DecDouble_LH(ByRef BinNum As String) As Double

Dim BinLen As Double
Dim i As Double
Dim DecNum As Double

DecNum = 0
BinLen = Len(BinNum)

For i = BinLen To 1 Step -1
  If Mid(BinNum, i, 1) = "H" Then
    DecNum = DecNum + 2 ^ Abs(i - BinLen)
  End If
Next i

Bin2DecDouble_LH = DecNum

End Function

'#####################################################################################################################################
'# Function name: Bin2DecDouble
'# Parameters: binary number as string
'# Description: returning the decimal number as double
'#####################################################################################################################################
Function Bin2DecDouble(ByRef BinNum As String) As Double

Dim BinLen As Double
Dim i As Double
Dim DecNum As Double

DecNum = 0
BinLen = Len(BinNum)

For i = BinLen To 1 Step -1
  If Mid(BinNum, i, 1) = "1" Then
    DecNum = DecNum + 2 ^ Abs(i - BinLen)
  End If
Next i

Bin2DecDouble = DecNum

End Function

'#####################################################################################################################################
'# Function name: Bin2Dec
'# Parameters: binary number as string
'# Description: returning the decimal number as string
'#####################################################################################################################################
Function Bin2Dec(BinNum As String) As String

Dim BinLen As Double
Dim i As Double
Dim DecNum As Double

DecNum = 0
BinLen = Len(BinNum)

For i = BinLen To 1 Step -1
  If Mid(BinNum, i, 1) And 1 Then
    DecNum = DecNum + 2 ^ Abs(i - BinLen)
  End If
Next i

Bin2Dec = CStr(DecNum)

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function ConvertHexBin(hexStr As String) As String
    Dim B As String
    Dim i As Integer
    
    For i = 1 To Len(hexStr)

Select Case LCase(Mid(hexStr, i, 1))

    Case "0"
    B = B + "0000"
    Case "1"
    B = B + "0001"
    Case "2"
    B = B + "0010"
    Case "3"
    B = B + "0011"
    Case "4"
    B = B + "0100"
    Case "5"
    B = B + "0101"
    Case "6"
    B = B + "0110"
    Case "7"
    B = B + "0111"
    Case "8"
    B = B + "1000"
    Case "9"
    B = B + "1001"
    Case "a"
    B = B + "1010"
    Case "b"
    B = B + "1011"
    Case "c"
    B = B + "1100"
    Case "d"
    B = B + "1101"
    Case "e"
    B = B + "1110"
    Case "f"
    B = B + "1111"
    End Select
Next i

ConvertHexBin = B

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Function MyStringToInt(SringNum As String) As Long

Dim StringLen As Long
Dim i As Long
Dim DecNum As Long
Dim NonZeroDigit As Boolean


DecNum = 0
StringLen = Len(SringNum)

For i = 1 To StringLen Step 1
  DecNum = DecNum * 10
  NonZeroDigit = False
  
  If Mid(SringNum, i, 1) = "0" Then
    DecNum = DecNum
    NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "1" Then
      DecNum = DecNum + 1
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "2" Then
      DecNum = DecNum + 2
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "3" Then
      DecNum = DecNum + 3
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "4" Then
      DecNum = DecNum + 4
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "5" Then
      DecNum = DecNum + 5
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "6" Then
      DecNum = DecNum + 6
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "7" Then
      DecNum = DecNum + 7
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "8" Then
      DecNum = DecNum + 8
      NonZeroDigit = True
  ElseIf Mid(SringNum, i, 1) = "9" Then

  End If


If NonZeroDigit = False Then
 DecNum = DecNum / 10
End If


Next i

MyStringToInt = DecNum

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function Get_Wafer_Info_VB(argc As Long, argv() As String, Optional Validating_ As Boolean) As Long

Dim site As Long

Dim ID_KEY_0_Inv As String
Dim ID_KEY_1_Inv  As String
Dim ID_KEY_2_Inv  As String
Dim ID_KEY_3_Inv  As String

Dim Data_KEY_0_Inv  As String
Dim Data_KEY_1_Inv  As String
Dim Data_KEY_2_Inv  As String
Dim Data_KEY_3_Inv  As String

Dim Group_ID_Inv  As String

Dim TDI_Data(59) As String
Dim i As Long
Dim Tester_Special_Lot As Long
Dim Myindex As Long
Dim PrintTagID As String
Dim DriverTemp As String
Dim MyChar As String

Dim GoodDriverInput As Boolean
Dim CheckSite0 As Boolean
Dim NotInitializedYet As Boolean
Dim DriverTempIsReadAlready As Boolean
Dim facility As String

Dim X_Site0 As Double
Dim Y_Site0 As Double
Dim row_number As Double
Dim test_num As Long

CheckSite0 = True
NotInitializedYet = True


test_num = TheExec.Sites.site(0).TestNumber

'Checking which Facility we are and setting the Tempreture according

facility = TheExec.Datalog.Setup.LotSetup.FacilityID
Call TheExec.Datalog.WriteComment("Facility Id is: " & CStr(facility))

If StrComp(facility, "WINSTEK") = 0 Then
    FixedTempreture = FixedTempreture_WINSTEK
ElseIf StrComp(facility, "ASECL") = 0 Then
    FixedTempreture = FixedTempreture_ASE
Else
    FixedTempreture = FixedTempreture_DEFAULT
End If






'#############     From here the adding for global channel nums       #############

'Definig the globlas channels array for the entire touchdown

Dim RetPinArray_TMS() As String
Dim RetPinCnt_TMS As Long
Dim RetPinArray_TDO() As String
Dim RetPinCnt_TDO As Long
Dim RetPinArray_TDI() As String
Dim RetPinCnt_TDI As Long
Dim RetPinArray_GPIO4() As String
Dim RetPinCnt_GPIO4 As Long
Dim RetPinArray_VddCap() As String
Dim RetPinCnt_VddCap As Long
Dim RetPinArray_VddDbg() As String
Dim RetPinCnt_VddDbg As Long

'chan_num_VddDbg

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheExec.DataManager.DecomposePinList("TDI", RetPinArray_TDI, RetPinCnt_TDI)
    Call TheExec.DataManager.DecomposePinList("TDO", RetPinArray_TDO, RetPinCnt_TDO)
    Call TheExec.DataManager.DecomposePinList("TMS", RetPinArray_TMS, RetPinCnt_TMS)
    Call TheExec.DataManager.DecomposePinList("GPIO4", RetPinArray_GPIO4, RetPinCnt_GPIO4)
    Call TheExec.DataManager.DecomposePinList("VDD_CAP", RetPinArray_VddCap, RetPinCnt_VddCap)
    Call TheExec.DataManager.DecomposePinList("VDD_DBG", RetPinArray_VddDbg, RetPinCnt_VddDbg)
Else
    Call TheExec.DataManager.DecomposePinList("TMS", RetPinArray_TDI, RetPinCnt_TDI)
    Call TheExec.DataManager.DecomposePinList("TMS", RetPinArray_TDO, RetPinCnt_TDO)
    Call TheExec.DataManager.DecomposePinList("TMS", RetPinArray_TMS, RetPinCnt_TMS)
    Call TheExec.DataManager.DecomposePinList("GPIO4", RetPinArray_GPIO4, RetPinCnt_GPIO4)
    Call TheExec.DataManager.DecomposePinList("VDD_CAP", RetPinArray_VddCap, RetPinCnt_VddCap)
    Call TheExec.DataManager.DecomposePinList("VDD_DBG", RetPinArray_VddDbg, RetPinCnt_VddDbg)

End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        
        chan_num_TMS(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_TMS(site), site, True))
        chan_num_TDO(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_TDO(site), site, True))
        chan_num_TDI(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_TDI(site), site, True))
        chan_num_GPIO4(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_GPIO4(site), site, True))
        chan_num_VddCap(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_VddCap(site), site, True))
        chan_num_VddDbg(CInt(site)) = CLng(HPTInfo.Pins.ChannelFromPinSite(RetPinArray_VddDbg(site), site, True))
        
    Next site
End With

chan_num_Default = 3000

'For verification:
If PrintChanNum Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
        
            Call TheExec.Datalog.WriteComment("Site= " & CStr(site) & " chan_num_TMS= " & CStr(chan_num_TMS(site)) & " chan_num_GPIO4= " & CStr(chan_num_GPIO4(site)) & " chan_num_VddCap= " & CStr(chan_num_VddCap(site)) & " chan_num_TDO= " & CStr(chan_num_TDO(site)) & " chan_num_TDI= " & CStr(chan_num_TDI(site)) & " chan_num_VddDbg= " & CStr(chan_num_VddDbg(site)))
            
        Next site
    End With

End If

'#############     Till here the adding for global channel nums       #############

DriverTempIsReadAlready = False

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
   
            If TheExec.RunMode = runModeDebug Then
            
                X_coor(site) = site + 120
                Y_coor(site) = site + 65
                
                wafer_id = TheExec.Datalog.Setup.WaferSetup.ID
                wafer_id = Left(wafer_id, 2)
                
                If MyStringToInt(wafer_id) < 10 Then
                    wafer_id = Right(wafer_id, 1)
                End If
                
                lot_id = TheExec.Datalog.Setup.LotSetup.LotID
                lot_id = Left(lot_id, 6)
            
                If Print_ULT Then
                     Touch_Down_Index = Touch_Down_Index + 1
                     ULT(site) = lot_id & "_" & CStr(wafer_id) & "_" & CStr(X_coor(site)) & "_" & CStr(Y_coor(site))
                     Call TheExec.Datalog.WriteComment("#DB123| ULT " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & "|touchDown_index = " & CStr(Touch_Down_Index))
                 End If
                 
            Else
            
                X_coor(site) = TheExec.Datalog.Setup.WaferSetup.GetXCoord(site)
                Y_coor(site) = TheExec.Datalog.Setup.WaferSetup.GetYCoord(site)
                 wafer_id = TheExec.Datalog.Setup.WaferSetup.ID
                 
                 If Len(wafer_id) > 9 Then  ' to support different UI output A.L. 8-12-21
                    wafer_id = Mid(wafer_id, 8, 2)
                End If
                 
                wafer_id = Left(wafer_id, 2)
                
                If Left(wafer_id, 1) = "0" Then
                    wafer_id = Right(wafer_id, 1)
                End If
                
                lot_id = TheExec.Datalog.Setup.LotSetup.LotID
                lot_id = Left(lot_id, 6)
                
                If Print_ULT Then
                     Touch_Down_Index = Touch_Down_Index + 1
                     ULT(site) = lot_id & "_" & CStr(wafer_id) & "_" & CStr(X_coor(site)) & "_" & CStr(Y_coor(site))
                     Call TheExec.Datalog.WriteComment("#DB123| ULT " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & "|touchDown_index = " & CStr(Touch_Down_Index))
                 End If

            End If
            
            'Check if site0 has out of ring units - once per touchdown
            
            If CheckSite0 Then
                'calc X-Y of site 0 - can be outside of the wafer itsef
                X_Site0 = X_coor(site) - (Int(site / 10) * 2)
                Y_Site0 = Y_coor(site) + (site Mod 10) * 2
                
                    
                If (X_Site0 < 1) Or (Y_Site0 > Wafer_Y_Size) Then ' we can place the prober on the left or below the wafer and exceed the limits of the X-Y in the array (we cant place it above or on the right since then no real units will be tested)
                    OutOf_Wafer_touchDown = True
                    CheckSite0 = False
                
                    Else ' coordinate of site0 are within the X-Y indexes of the array
                        
                        If Site0_Lookup_ARRAY(X_Site0, Y_Site0) = 1 Then
                            OutOf_Wafer_touchDown = True
                            CheckSite0 = False
                        Else
                            OutOf_Wafer_touchDown = False
                            CheckSite0 = False
                        End If
                    End If
            End If
        End If
    Next site
End With

Tester_Special_Lot = InStr(1, lot_id, "P0X")

If (Current_Wafer_ID <> wafer_id) Or (Lot_Name <> lot_id) Then 'First_Wafer Or different wafer or different lot or combination
    Wafer_Name = "_W" & CStr(wafer_id)
    Lot_Name = lot_id
    First_Wafer = False
    Current_Wafer_ID = wafer_id
    ID_Key_Index = 0
    If X_Y_Table_Loaded Then
    ' Do nothing
    Else
        Call XY_Table_Import
        X_Y_Table_Loaded = True
    End If
    
    Touch_Down_Index = 1
    
    Call Keys_Import
    
    'Addition to update the write device params pattern

    Tester_Special_Lot = InStr(1, lot_id, "P0X")

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If (.site(site).Active = True) And (NotInitializedYet) Then

                NotInitializedYet = False
                Site_Die_ID(site).Index_On_Wafer = ID_Key_Index
                
                If (TheExec.CurrentChanMap = "HPT_New_LB" And TheExec.EnableWord("NVM")) Or Tester_Special_Lot <> 0 Then
                    Site_Die_ID(site).tagId = KEYS_ARRAY(Wiliot_ID_File_Index).tagId
                    Site_Die_ID(site).groupId = KEYS_ARRAY(site).groupId
                    ID_KEY_0 = ConvertHexBin(Mid(KEYS_ARRAY(site).identityKey, 25, 8))
                    ID_KEY_1 = ConvertHexBin(Mid(KEYS_ARRAY(site).identityKey, 17, 8))
                    ID_KEY_2 = ConvertHexBin(Mid(KEYS_ARRAY(site).identityKey, 9, 8))
                    ID_KEY_3 = ConvertHexBin(Mid(KEYS_ARRAY(site).identityKey, 1, 8))
                    
                    Data_KEY_0 = ConvertHexBin(Mid(KEYS_ARRAY(site).dataKey, 25, 8))
                    Data_KEY_1 = ConvertHexBin(Mid(KEYS_ARRAY(site).dataKey, 17, 8))
                    Data_KEY_2 = ConvertHexBin(Mid(KEYS_ARRAY(site).dataKey, 9, 8))
                    Data_KEY_3 = ConvertHexBin(Mid(KEYS_ARRAY(site).dataKey, 1, 8))
                    
                    Group_ID = KEYS_ARRAY(site).groupId '& "05" - Ameet 7-11-21 we had this 05 and it should not be used any more
                    Group_ID = ConvertHexBin(Group_ID)
                Else
                
                    Site_Die_ID(site).tagId = KEYS_ARRAY(XY_Lookup_Array(X_coor(site), Y_coor(site))).tagId  ' was  KEYS_ARRAY(ID_Key_Index).tagId
                    Site_Die_ID(site).groupId = KEYS_ARRAY(XY_Lookup_Array(X_coor(site), Y_coor(site))).groupId  ' was  KEYS_ARRAY(ID_Key_Index).tagId
                    Site_Die_ID(site).identityKey = KEYS_ARRAY(XY_Lookup_Array(X_coor(site), Y_coor(site))).identityKey  ' was  KEYS_ARRAY(ID_Key_Index).tagId
                    Site_Die_ID(site).dataKey = KEYS_ARRAY(XY_Lookup_Array(X_coor(site), Y_coor(site))).dataKey  ' was  KEYS_ARRAY(ID_Key_Index).tagId
            
                   ID_KEY_0 = ConvertHexBin(Mid(Site_Die_ID(site).identityKey, 25, 8))
                   ID_KEY_1 = ConvertHexBin(Mid(Site_Die_ID(site).identityKey, 17, 8))
                   ID_KEY_2 = ConvertHexBin(Mid(Site_Die_ID(site).identityKey, 9, 8))
                   ID_KEY_3 = ConvertHexBin(Mid(Site_Die_ID(site).identityKey, 1, 8))
                   
                   Data_KEY_0 = ConvertHexBin(Mid(Site_Die_ID(site).dataKey, 25, 8))
                   Data_KEY_1 = ConvertHexBin(Mid(Site_Die_ID(site).dataKey, 17, 8))
                   Data_KEY_2 = ConvertHexBin(Mid(Site_Die_ID(site).dataKey, 9, 8))
                   Data_KEY_3 = ConvertHexBin(Mid(Site_Die_ID(site).dataKey, 1, 8))
                   
                   Group_ID = KEYS_ARRAY(row_number).groupId ' & "05" - Ameet 7-11-21 we don't need this 05 any longer
                   Group_ID = ConvertHexBin(Group_ID)
                   
                End If
                   
                If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then  'in CJTAG we need to input the NOT Data to the TDI (TMS)
                    
                    ID_KEY_0_Inv = Cjtag_Inverse(ID_KEY_0, 32)
                    ID_KEY_1_Inv = Cjtag_Inverse(ID_KEY_1, 32)
                    ID_KEY_2_Inv = Cjtag_Inverse(ID_KEY_2, 32)
                    ID_KEY_3_Inv = Cjtag_Inverse(ID_KEY_3, 32)
                    
                    Data_KEY_0_Inv = Cjtag_Inverse(Data_KEY_0, 32)
                    Data_KEY_1_Inv = Cjtag_Inverse(Data_KEY_1, 32)
                    Data_KEY_2_Inv = Cjtag_Inverse(Data_KEY_2, 32)
                    Data_KEY_3_Inv = Cjtag_Inverse(Data_KEY_3, 32)
                    
                    Group_ID_Inv = Cjtag_Inverse(Group_ID, 24)
                        
                End If
            
                ULT(site) = lot_id & "_" & CStr(wafer_id) & "_" & CStr(X_coor(site)) & "_" & CStr(Y_coor(site))
                
                If Site_Die_ID(site).tagId = "0" Or Site_Die_ID(site).tagId = "" Then ' XY_Lookup_Array(X_coor(site), Y_Coor(site)) = 0
                  Call TheExec.Datalog.WriteComment("#DB9| " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & " iligal ID number or value")
                   HPTInfo.Sites.site(site).TestResult = siteFail
                End If
                
                If XY_Lookup_Array(X_coor(site), Y_coor(site)) = 0 Then  '
                  Call TheExec.Datalog.WriteComment("#DB667| " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & " iligal location for lookuparray")
                   'HPTInfo.sites.site(site).TestResult = siteFail
                End If
       End If
    Next site
End With

'updating the pattern for all sites but can we do it for all at once???
With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
          
            TheExec.Datalog.WriteComment ("from file ID_KEY_0 = " & CStr(ID_KEY_0))
            TDI_Data(site) = ID_KEY_0_Inv
            
            For i = 0 To 31
                
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_0", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_0", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
        
            TDI_Data(site) = ID_KEY_1_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_1", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_1", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
        
            TDI_Data(site) = ID_KEY_2_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_2", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_2", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
       
            TDI_Data(site) = ID_KEY_3_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_3", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_id_key_3", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
       
            TDI_Data(site) = Data_KEY_0_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_0", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_0", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
       
            TDI_Data(site) = Data_KEY_1_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_1", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_1", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
       
            TDI_Data(site) = Data_KEY_2_Inv
             
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_2", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_2", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
       
            TDI_Data(site) = Data_KEY_3_Inv
            
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_3", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_data_key_3", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1))
                End If
            Next i
            
            
            TDI_Data(site) = Group_ID_Inv
            
            For i = 0 To 23 'Ameet 7-11-21 we changed the loop from 32 bit to 24 bits size of Group-ID (was 32 with the old "05")
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_PGXDID", i, chan_num_TDI(site), Mid(TDI_Data(site), 23 - i + 1, 1))
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("modv_write_device_specific_params_def_PGXDID", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 23 - i + 1, 1))
                End If
            Next i
        Next site
    End With
    
    Debug_Dump_Final = True  'To perform selective dump during wafer sort at the end of the TPS but not on the first
    
End If

DriverTemp = ""

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True And (DriverTempIsReadAlready = False) Then
            DriverTemp = TheExec.Datalog.Setup.LotSetup.PartText(site) 'This is string variable
            DriverTempIsReadAlready = True
        End If
    Next site
End With

Call TheExec.Datalog.WriteComment("#DB999 prober temp is " & DriverTemp)

'LEN LEFT Right
If DriverTemp <> "" Then
 
    GoodDriverInput = True
    
    For i = 1 To Len(DriverTemp)
    
        MyChar = Mid(DriverTemp, i, 1)
        
        If (MyChar = ".") Or ((MyChar <= "9") And (MyChar >= "0")) Then
            MyChar = MyChar
        Else
            GoodDriverInput = False
        End If
        
    Next i
Else
    GoodDriverInput = False
End If

If GoodDriverInput = True Then
    Call TheExec.Datalog.WriteParametricResult(site, 0, logTestPass, parmPass, "Measured_Prober_temp", 0, 0, CDbl(DriverTemp), 85, unitCustom, 0, unitNone, 0, , "C")
End If
    
If GoodDriverInput Then

    Actual_Prober_Temp = CDbl(DriverTemp)
    Call TheExec.Datalog.WriteComment("#DB888 Actual temp is " & CStr(Actual_Prober_Temp))
        
'        If Actual_Prober_Temp > 31 Or Actual_Prober_Temp < 20 Then
'           Stop ' stop production if we are very far from target of 25C
'           BOX_ID_global = InputBox("TemperatureOutOfSpec 20C till 31C : if temperature in range enter it now, if not fix situation")
'           DriverTemp = BOX_ID_global
'           'Call WriteLogSTD("Box_ID", site, BOX_ID_global)
'           If DriverTemp <> "" Then
'                GoodDriverInput = True
'                 For i = 1 To Len(DriverTemp)
'                    MyChar = Mid(DriverTemp, i, 1)
'                    If (MyChar = ".") Or ((MyChar <= "9") And (MyChar >= "0")) Then
'                        MyChar = MyChar
'                    Else
'                        GoodDriverInput = False
'                    End If
'                Next i
'            Else
'                GoodDriverInput = False
'            End If
'
'            If GoodDriverInput = True Then
'                Call TheExec.Datalog.WriteParametricResult(site, 0, logTestPass, parmPass, "Measured_Prober_temp", 0, 0, CDbl(DriverTemp), 85, unitCustom, 0, unitNone, 0, , "C")
'            End If
'        End If
        
    Else
        Actual_Prober_Temp = -777
        Call TheExec.Datalog.WriteComment("#DB888 Actual temp was set to " & CStr(Actual_Prober_Temp))
    End If
        
'Else
'    Actual_Temp = 26.5
'End If
'Call TheExec.Datalog.WriteParametricResult(0, test_num + 1, logTestPass, parmPass, "Prober Temp", 0, 0, Actual_Temp, 85, unitNone, 0, unitNone, 0)
'HPTInfo.sites.site(0).TestResult = sitePass

'Eden: those are for using the external script that gets ID from cloud - we are not using it since it's diffrent then production flow
'Dim Flow_Version As String
'Dim packet_version As String
'Dim prefix As String
'Dim env As String
'
'Flow_Version = "4.f"
'packet_version = "2.1"
'prefix = "d3d30000"
'env = "/test"

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            Site_Die_ID(site).Index_On_Wafer = ID_Key_Index
'            If TheExec.CurrentChanMap = "HPT_New_LB" And TheExec.EnableWord("NVM") Then
'                Dim ID As String
'                ID = ""
'                ID = AllucateUID(Flow_Version, packet_version, prefix, env)
'                Site_Die_ID(site).tagId = ID
'            End If

            If (TheExec.CurrentChanMap = "HPT_New_LB" And TheExec.EnableWord("NVM")) And Tester_Special_Lot <> 0 Then
                Site_Die_ID(site).tagId = KEYS_ARRAY(Wiliot_ID_File_Index).tagId
                Wiliot_ID_File_Index = Wiliot_ID_File_Index + 1

                Open ID_Counter_FilePath For Output As #1
                    Write #1, Wiliot_ID_File_Index
                Close #1
            Else
                 Site_Die_ID(site).tagId = KEYS_ARRAY(XY_Lookup_Array(X_coor(site), Y_coor(site))).tagId  ' was  KEYS_ARRAY(ID_Key_Index).tagId
                 
                 If Site_Die_ID(site).tagId = "" Then
                    Call TheExec.Datalog.WriteComment("#DB111| Site ID is empty!!!!!!!! " & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & " iligal ID number or value")
                    Stop
                 End If

            End If
      
            ULT(site) = lot_id & "_" & CStr(wafer_id) & "_" & CStr(X_coor(site)) & "_" & CStr(Y_coor(site))
            PrintTagID = ConvertHexBin(Right(Site_Die_ID(site).tagId, 7))
            PrintTagID = Bin2Dec(PrintTagID)
            
            Call TheExec.Datalog.WriteParametricResult(site, test_num + 2, logTestPass, parmPass, "tagId", chan_num_Default, 0, CDbl(PrintTagID), 0, unitNone, 0, unitNone, 0)
            Call TheExec.Datalog.PRRFill(site, CStr(Site_Die_ID(site).tagId))
            
            HPTInfo.Sites.site(site).TestResult = sitePass
            
            If Site_Die_ID(site).tagId = "0" Or Site_Die_ID(site).tagId = "" Then ' XY_Lookup_Array(X_coor(site), Y_Coor(site)) = 0
                Call TheExec.Datalog.WriteComment("#DB9| " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & " iligal ID number or value")
                Stop
                HPTInfo.Sites.site(site).TestResult = siteFail
            End If
            
            If XY_Lookup_Array(X_coor(site), Y_coor(site)) = 0 Then  '
                Call TheExec.Datalog.WriteComment("#DB666| " & CStr(Site_Die_ID(site).tagId) & " |ULT = " & ULT(site) & " |Site = " & CStr(site) & " iligal location for lookuparray")
                'HPTInfo.sites.site(site).TestResult = siteFail
            End If
            
            ID_Key_Index = ID_Key_Index + 1
            
        End If
    Next site
End With

End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function name: DecimalToBinary
'Parameters: lngDecimalNum
'Description: Return string from decimal long number.Notation is [LSB..MSB]
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function DecimalToBinary(ByVal lngDecimalNum As Long) As String
    Dim i As Long
    On Error GoTo errorhandler
    

    DecimalToBinary = Trim(Str(lngDecimalNum Mod 2))
    '"\" returns an integer result
    lngDecimalNum = lngDecimalNum \ 2

    Do While lngDecimalNum <> 0
        DecimalToBinary = Trim(Str(lngDecimalNum Mod 2)) & DecimalToBinary
        lngDecimalNum = lngDecimalNum \ 2
    Loop
    
    For i = Len(DecimalToBinary) To 7
        DecimalToBinary = "0" & DecimalToBinary
    Next i
    
    DecimalToBinary = StrReverse(DecimalToBinary)
    Exit Function
errorhandler:
    TheExec.Datalog.WriteComment ("Function: DecimalToBinary Error: " & err.Description & "Error number: " & err.Number)
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Function name: ArrayStringToInt
'Parameters: lngDecimalNum
'Description: Return string from decimal long number.Notation is [LSB..MSB]
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Function VectorToInt(string_array() As Variant, start_offset As Integer, end_offset As Integer) As Integer
    Dim i As Long
    On Error GoTo errorhandler

     If (start_offset > end_offset) Then
                err.Raise Number:=vbObjectError + 513, _
              Description:="Start offset is bigger then end_offset"
            End If
            
    VectorToInt = 0
    For i = start_offset To end_offset
        If (string_array(i) = "H") Then
            VectorToInt = VectorToInt + 2 ^ (i - start_offset)
        Else
            If (string_array(i) <> "L") Then
                err.Raise Number:=vbObjectError + 513, _
              Description:="Vector is not H or L"
            End If
        End If
    Next i
    
    Exit Function
errorhandler:
    TheExec.Datalog.WriteComment ("Function: VectorToInt Error: " & err.Description & "Error number: " & err.Number)
End Function

Public Function Only_JTAG_ReadRegForcePrint2Datalog(AddrHex As String) As String

Dim site As Long
Dim ii As Long
Dim jj As Long
Dim Addr As String
Dim test_num As Long

Dim TDO_Data(59) As String
Dim TDI_Data(59) As String
Dim Init_Len As Long

Dim RegData(59) As String
Dim Verification_Retries As Long


TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Addr = ConvertHexBin(AddrHex)
Init_Len = Len(Addr)

If Init_Len < 32 Then
    For ii = Init_Len To 31
       Addr = "0" & Addr
    Next ii
End If

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Load


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then

            TDI_Data(site) = Addr
            
            For ii = 0 To 31
                'Call TheExec.Datalog.WriteComment("Bit " & CStr(i) & "=" & CStr(Mid(TDI_Data(site), i + 1, 1)))
                Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").ModifyChanVectorData("Read_Addr", ii, chan_num_TDI(site), Mid(TDI_Data(site), 31 - ii + 1, 1))
            Next ii

'            For i = 0 To 31
'                'Call TheExec.Datalog.WriteComment("Bit " & CStr(i) & "=" & CStr(Mid(TDI_Data(site), i + 1, 1)))
'                Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("wf_uid_upper", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
'            Next i
            
        End If
    Next site
End With

Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Run("")

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "11110100010000011010000111101110"
            End If
            
            RegData(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("Site " & CStr(site) & ", Addr = " & CStr(AddrHex) & ", Data = " & CStr(RegData(site)))
       End If
    Next site
End With


End Function

Function ReadRegForcePrint2Datalog(AddrHex As String) As String

Dim site As Long

Dim TDO_Data(59) As String
Dim TDI_Data(59) As String

Dim i As Long
Dim ii As Long
Dim Init_Len As Long

Dim Addr As String
Dim RegData(59) As String

Dim LSB_ID As String
Dim LSB_ID_Bin As String


'Addr = ConvertHexBin(AddrHex)
'Init_Len = Len(Addr)
'If Init_Len < 32 Then
'    For ii = Init_Len To 31
'       Addr = "0" & Addr
'    Next ii
'End If
'
'AddrHex = Addr

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_Patterns_JTAG\GP_Read.PAT").Load
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\gp_read.PAT").Load
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
                
            LSB_ID = ConvertHexBin(AddrHex)
            Init_Len = Len(LSB_ID)
            
            If Init_Len < 32 Then
                For ii = Init_Len To 31
                   LSB_ID = "0" & LSB_ID
                Next ii
            End If
                        
            If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then  'in CJTAG we need to input the NOT Data to the TDI (TMS)
                For i = 1 To 32
                    If Mid(LSB_ID, i, 1) = "1" Then
                        Mid(LSB_ID, i, 1) = "0"
                    Else
                        Mid(LSB_ID, i, 1) = "1"
                    End If
                Next i
            End If
           
            
            TDI_Data(site) = LSB_ID

            If TheExec.TesterMode = testModeOnline Then
                For i = 0 To 31
                    If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                        Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    Else
                        Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    End If
                Next i
             Else:
               i = i
            End If

        End If
    Next site
End With

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").Run("")
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Run("")
End If
TheHdw.Wait (0.005)

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "00011110101101110111110100010101"
            End If
            
            RegData(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("Site " & CStr(site) & ", Addr = " & CStr(AddrHex) & ", Data = " & CStr(RegData(site)))
       End If
    Next site
End With


End Function

Function ReadSiteRegForcePrint2Datalog(AddrHex As String, site As Long) As String


Dim TDO_Data(59) As String
Dim TDI_Data(59) As String

Dim i As Long
Dim ii As Long
Dim Init_Len As Long

Dim Addr As String
Dim RegData(59) As String

Dim LSB_ID As String
Dim LSB_ID_Bin As String


TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_Patterns_JTAG\GP_Read.PAT").Load
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\gp_read.PAT").Load
End If

With HPTInfo.Sites
    If .site(site).Active = True Then
            
        LSB_ID = ConvertHexBin(AddrHex)
        Init_Len = Len(LSB_ID)
        
        If Init_Len < 32 Then
            For ii = Init_Len To 31
               LSB_ID = "0" & LSB_ID
            Next ii
        End If
                    
        If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then  'in CJTAG we need to input the NOT Data to the TDI (TMS)
            For i = 1 To 32
                If Mid(LSB_ID, i, 1) = "1" Then
                    Mid(LSB_ID, i, 1) = "0"
                Else
                    Mid(LSB_ID, i, 1) = "1"
                End If
            Next i
        End If
       
        
        TDI_Data(site) = LSB_ID

        If TheExec.TesterMode = testModeOnline Then
            For i = 0 To 31
                If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                Else
                    Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                End If
            Next i
         Else:
           i = i
        End If

    End If
End With

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").Run("")
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Run("")
End If
TheHdw.Wait (0.1)

With HPTInfo.Sites
    If .site(site).Active = True Then
        
        TDO_Data(site) = ""
        
        If TheExec.TesterMode = testModeOnline Then
            TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
        Else
            TDO_Data(site) = "00011110101101110111110100010101"
        End If
        
        RegData(site) = Bin2Hex32bits(TDO_Data(site))
        
        Call TheExec.Datalog.WriteComment("Site " & CStr(site) & ", Addr = " & CStr(AddrHex) & ", Data = " & CStr(RegData(site)))
   End If
End With

ReadSiteRegForcePrint2Datalog = RegData(site)


End Function



Function ReadRegWithoutPrint2Datalog(AddrHex As String) As String

Dim site As Long

Dim TDO_Data(59) As String
Dim TDI_Data(59) As String

Dim i As Long
Dim ii As Long
Dim Init_Len As Long

Dim Addr As String
Dim RegData(59) As String

Dim LSB_ID As String
Dim LSB_ID_Bin As String


'Addr = ConvertHexBin(AddrHex)
'Init_Len = Len(Addr)
'If Init_Len < 32 Then
'    For ii = Init_Len To 31
'       Addr = "0" & Addr
'    Next ii
'End If
'
'AddrHex = Addr

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_Patterns_JTAG\GP_Read.PAT").Load
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\gp_read.PAT").Load
End If

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
                
            LSB_ID = ConvertHexBin(AddrHex)
            Init_Len = Len(LSB_ID)
            
            If Init_Len < 32 Then
                For ii = Init_Len To 31
                   LSB_ID = "0" & LSB_ID
                Next ii
            End If
                        
            If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then  'in CJTAG we need to input the NOT Data to the TDI (TMS)
                For i = 1 To 32
                    If Mid(LSB_ID, i, 1) = "1" Then
                        Mid(LSB_ID, i, 1) = "0"
                    Else
                        Mid(LSB_ID, i, 1) = "1"
                    End If
                Next i
            End If
           
            
            TDI_Data(site) = LSB_ID

            If TheExec.TesterMode = testModeOnline Then
                For i = 0 To 31
                    If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                        Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    Else
                        Call TheHdw.Digital.Patterns.Pat(".\GP_CJTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    End If
                Next i
             Else:
               i = i
            End If

        End If
    Next site
End With

If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").Run("")
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Run("")
End If


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "00011110101101110111110100010101"
            End If
            
            RegData(site) = Bin2Hex32bits(TDO_Data(site))
            
            Call TheExec.Datalog.WriteComment("Site " & CStr(site) & ", Addr = " & CStr(AddrHex) & ", Data = " & CStr(RegData(site)))
       End If
    Next site
End With


End Function



Public Function CJTAG_ReadRegForcePrint2Datalog_based_On_JTAG(AddrHex As String) As String

Dim site As Long
Dim ii As Long
Dim i As Long
Dim jj As Long
Dim Addr As String
Dim test_num As Long
Dim Tmp_Data As String
Dim TDO_Data(59) As String
Dim TDI_Data(59) As String
Dim Init_Len As Long
Dim RegData(59) As String

TheHdw.Digital.HRAM.Size = 256
Call TheHdw.Digital.HRAM.SetTrigger(trigSTV, False, 0, False)
Call TheHdw.Digital.HRAM.SetCapture(captSTV, False)
Call TheHdw.Digital.Patgen.ClearFailCount

test_num = TheExec.Sites.site(0).TestNumber

Addr = ConvertHexBin(AddrHex)
Init_Len = Len(Addr)
If Init_Len < 32 Then
    For ii = Init_Len To 31
       Addr = "0" & Addr
    Next ii
End If



If TheExec.CurrentJob = "WS_Pixie_D3_HPT_CJTAG" Then  'in CJTAG we need to input the NOT Data to the TDI (TMS)
    For i = 1 To 32
        If Mid(Addr, i, 1) = "1" Then
            Mid(Addr, i, 1) = "0"
        Else
            Mid(Addr, i, 1) = "1"
        End If
    Next i
End If


If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_Patterns_JTAG\GP_Read.PAT").Load
Else
    Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_CJTAG\GP_Read.PAT").Load
End If

Call TheHdw.PinLevels.ConnectAllPins
Call TheHdw.PinLevels.ApplyPower

TheHdw.Wait (0.005)


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDI_Data(site) = Addr

            If TheExec.TesterMode = testModeOnline Then
            
                For i = 0 To 31
                    'Call TheExec.Datalog.WriteComment("Bit " & CStr(i) & "=" & CStr(Mid(TDI_Data(site), i + 1, 1)))
                    If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
                        Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_Patterns_JTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    Else
                        Call TheHdw.Digital.Patterns.Pat(".\Patterns\GP_CJTAG\GP_Read.PAT").ModifyChanVectorData("Read_Addr", i * 3, chan_num_TDI(site), Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
                    End If
                Next i
                
            Else:
               i = i
            End If
'
'            For i = 0 To 31
'                'Call TheExec.Datalog.WriteComment("Bit " & CStr(i) & "=" & CStr(Mid(TDI_Data(site), i + 1, 1)))
'                Call TheHdw.Digital.Patterns.Pat(".\Patterns\Device_Parameters\write_device_specific_params_CALIB_DONE.Pat").ModifyChanVectorData("wf_uid_upper", i, Chan_num, Mid(TDI_Data(site), 31 - i + 1, 1)) ' ToDo change to the lable inside the pattern not wf_temp
'            Next i

        End If
    Next site
End With



If TheExec.CurrentJob <> "WS_Pixie_D3_HPT_CJTAG" Then
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns_JTAG\GP_Read.PAT").Run("")
Else
    Call TheHdw.Digital.Patterns.Pat(".\GP_Patterns\GP_Read.PAT").Run("")
End If


With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
            
            TDO_Data(site) = ""
            
            If TheExec.TesterMode = testModeOnline Then
                TDO_Data(site) = ReadChanelData(chan_num_TDO(site), 0, 32)
            Else
                TDO_Data(site) = "11111111111111111111111111111111"
            End If
            
            RegData(site) = Bin2Hex32bits(TDO_Data(site))
            
           ' Call TheExec.Datalog.WriteComment("Site " & CStr(site) & ", Addr = " & CStr(AddrHex) & ", Data = " & CStr(RegData(site)))
       End If
    Next site
End With


End Function

'#####################################################################################################################################
'# Function name:ReadChanelData_LH
'# Parameters: channel_num , starting index, length to read
'# Description: reading from the HRAM the selected channel for the given cycles
'# High Level Flow:
'#####################################################################################################################################
Function ReadChanelData_LH(Chan_num As Long, startIdx As Long, length As Long) As String
Dim readout As String
Dim i As Long
readout = ""

For i = length + startIdx - 1 To startIdx Step -1
    readout = readout & TheHdw.Digital.HRAM.Chans(Chan_num).PinData(CLng(i))
Next i
ReadChanelData_LH = readout
End Function

'#####################################################################################################################################
'# Function name:ReadChanelData
'# Parameters: channel_num , starting index, length to read
'# Description: reading from the HRAM the selected channel for the given cycles
'# High Level Flow:
'#####################################################################################################################################
Function ReadChanelData(Chan_num As Long, startIdx As Long, length As Long) As String

Dim readout As String
Dim i As Long
readout = ""

For i = length + startIdx - 1 To startIdx Step -1
 If TheHdw.Digital.HRAM.Chans(Chan_num).PinData(CLng(i)) = "H" Then
        readout = readout & "1"
    Else
        readout = readout & "0"
    End If
Next i
ReadChanelData = readout

End Function

'#####################################################################################################################################
'# Function name:OfflineFillArray
'# Parameters: value As Double, length As Double
'# Description: returning array in the wanted length with the wanted value
'# High Level Flow:
'#####################################################################################################################################
Function OfflineFillArray(value As Double)
Dim retArray(59) As Double
Dim site As Long

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        retArray(site) = value
    Next site
End With

OfflineFillArray = retArray

End Function

'#####################################################################################################################################
'# Function name:SumSitesArray
'# Parameters: value As Double, length As Double
'# Description: returning array in the wanted length with the wanted value
'# High Level Flow:
'#####################################################################################################################################
Function SumSitesArray(arr() As Long) As Long
Dim sum As Long
Dim i As Long

sum = 0

For i = 0 To 59
    sum = sum + arr(i)
Next i

SumSitesArray = sum

End Function

'#####################################################################################################################################
'# Function name:DebugDelay_func
'# Parameters: Debug_Delay As Boolean, Defult_Delay_sec As Long, Special_Delay_sec As Long
'# Description: will delay for default if the flag is false, otherwise it will wait for special time
'#####################################################################################################################################
Public Function DebugDelay_function(Debug_Delay As Boolean, Defult_Delay_sec As Double, Special_Delay_sec As Double)

If Debug_Delay Then
    TheHdw.Wait (Special_Delay_sec)
Else
    TheHdw.Wait (Defult_Delay_sec)
End If

End Function

'#####################################################################################################################################
'# Function name:Cjtag_Inverse
'# Parameters: word to inverse as string and length
'# Description: returning the inverse of the given word in binary string
'# High Level Flow:
'#####################################################################################################################################
Function Cjtag_Inverse(word As String, length As Long) As String
Dim i As Long
Dim ret_word As String

ret_word = word

For i = 1 To length
    If Mid(word, i, 1) = "1" Then
        Mid(ret_word, i, 1) = "0"
    Else
        Mid(ret_word, i, 1) = "1"
    End If
Next i

Cjtag_Inverse = ret_word

End Function



'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function HPT_PPMU_ConnectAll(name As String)

Dim site As Long

If StrComp(name, "VDD_CAP") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_VddCap(site)).Connect
            End If
        Next site
    End With

ElseIf StrComp(name, "GPIO4") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_GPIO4(site)).Connect
            End If
        Next site
    End With
    
ElseIf StrComp(name, "VDD_DBG") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_VddDbg(site)).Connect
            End If
        Next site
    End With


End If

TheHdw.Wait (0.005)

End Function


'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function HPT_PPMU_DisconnectAll(name As String)

Dim site As Long

If StrComp(name, "VDD_CAP") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_VddCap(site)).Disconnect
            End If
        Next site
    End With

ElseIf StrComp(name, "GPIO4") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_GPIO4(site)).Disconnect
            End If
        Next site
    End With

ElseIf StrComp(name, "VDD_DBG") = 0 Then

    With HPTInfo.Sites
        For site = 0 To .ExistingCount - 1
            If .site(site).Active = True Then
                TheHdw.PPMU.Chans(chan_num_VddDbg(site)).Disconnect
            End If
        Next site
    End With



End If

TheHdw.Wait (0.005)

End Function


'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function SitesFailes_Initialize()

Dim loop_index As Long

For loop_index = 0 To Number_of_Sites - 1
    Sites_Failes(loop_index) = False
Next loop_index

End Function

'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function SitesFailes_FailSite(site As Long)

Sites_Failes(site) = True

End Function


'#####################################################################################################################################
'# Function name:
'# Parameters:
'# Description:
'#####################################################################################################################################
Public Function SitesFailes_FinalSetAllFails()

Dim site As Long

With HPTInfo.Sites
    For site = 0 To .ExistingCount - 1
        If .site(site).Active = True Then
        
            If Sites_Failes(site) Then
                HPTInfo.Sites.site(site).TestResult = siteFail
                Call HPT_ReportResult(site, logTestFail)
            End If
        
        End If
    Next site
End With

End Function
