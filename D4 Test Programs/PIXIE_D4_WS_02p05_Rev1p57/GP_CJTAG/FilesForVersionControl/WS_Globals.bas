Attribute VB_Name = "WS_Globals"

Public X_Y_Table_Loaded As Boolean ' to upload X-Y table into memory only once
Public Debug_Dump_Final  'To perform selective dump during wafer sort at the end of the TPS but not on the first

Public Const Debug_Printout = True 'To print the #DB printouts for Ameet
Public Const Problem_Printout = True 'True 'TrueTo print the #DB printouts for Ameet
Public Const Simulate_60_Site = False 'To disable - enable the 60 site simulation - adding more 57 dummy operations on site2 Ameet.L. 28-7-21
Public Const Print_ULT = False 'To print ULT before continuity in get wafer infor Ameet.L. 23-8-21 - but it complicates analysis later on since there is no ID
Public Const Print_Time = False 'To print times and timers that yovav made
Public Const Debug_Flag = False
Public Const PrintChanNum = False

Public Const PPMU_Averaging = 1

Public Const Debug_Delay = False 'True
Public Const Monitoring_Printout = False 'True 'The readout of some registers to fix the problematic TR read in LO tests - redundent after JTAG fix A.L. 25-7-21
Public Const Debug_Delay_GPIO4 = False ' True ' Reguired only in VDD_085 redundent
Public Const JTAG_DisConnect_Delay = 0.001 ' Regular value was 5msec
Public Const Vdd_Cap_Change_Delay = 0.035 ' Required for stabilization of PS and DUT
Public Const Defult_Delay_sec = 0.005

Public OutOf_Wafer_touchDown As Boolean
Public Const New_DLL_Temp_Reading = False

Public Const Number_of_Sites = 60
Public Const Number_of_Rows = 10
Public Const Number_of_Columns = 6
Public Const Wafer_Y_Size = 146 ' be ware that if it is more than 255 we need to alocate 9 bits in the X/Y information that we program into the chip
Public Const Wafer_X_Size = 262 ' be ware that if it is more than 255 we need to alocate 9 bits in the X/Y information that we program into the chip
Public Const Number_Of_units_On_Wafer = 30200 ' change to 30200 as all new wafers will have this value - Ameet L. 28-7-21
Public Const Number_of_Touch_Downs = 572 ' margin over 572 ' change to 30200 as all new wafers will have this value - Ameet L. 28-7-21
Public Const Number_of_Sites0_out_of_Ring = 148

Public Const Number_of_Units_For_Touch_Downs = 33500 ' From ASE

Type DIE_DATA_TYPE
    groupId As String
    tagId As String
    identityKey As String
    dataKey As String
End Type

Type TEST_DATA_LINE
    field_name As String
    start_index As Double
    length As Double
    pass_type As String
    LogTestParam As String
    pass_values_num As Double
    pass_values(5) As String
    pass_range_low As Double
    pass_range_high As Double
    lo_limit As Double
    hi_limit As Double
    MeasUnit As UnitType
    ForceValue As Double
    ForceUnit As String
    IOC As Double
    offline_value As String
End Type


Public KEYS_ARRAY(Number_of_Units_For_Touch_Downs) As DIE_DATA_TYPE
Public XY_Lookup_Array(Wafer_X_Size, Wafer_Y_Size) As Integer
Public Site0_Lookup_ARRAY(Wafer_X_Size, Wafer_Y_Size) As Integer

Public Lot_Name As String
Public Wafer_Name As String
Public Current_Wafer_ID As String

Public ID_Key_Index As Long
Public First_Wafer As Boolean
Public Touch_Down_Index As Long

Public UID_MSB_lower(59) As String
Public UID_LSB_upper(59) As String
Public Physical_info(59) As String

Public ID_KEY_0 As String
Public ID_KEY_1 As String
Public ID_KEY_2 As String
Public ID_KEY_3 As String

Public Data_KEY_0 As String
Public Data_KEY_1 As String
Public Data_KEY_2 As String
Public Data_KEY_3 As String
Public Group_ID As String

Public Const Group_ID_ForCompare = "000002FD"

Public X_coor(Number_of_Sites - 1) As Long
Public Y_coor(Number_of_Sites - 1) As Long

Public lot_id As String
Public wafer_id As String
Public ULT(Number_of_Sites - 1) As String

Type SITE_TYPE
    Index_On_Wafer As Long
    tagId As String
    groupId As String
    identityKey As String
    dataKey As String
End Type

Public BOX_ID_global As String
Public Site_Die_ID(Number_of_Sites - 1) As SITE_TYPE

Public Wiliot_ID_File_Index As Long
Public ID_Counter_FilePath As String

'Tempreture globals
Public Actual_Temp(Number_of_Sites - 1) As Double
Public Actual_Prober_Temp As Double
Public Valid_Prober_temp As Boolean
Public Former_good_Prober_Temp As Double
Public Fix_Temp As Boolean

Public Converted_Temp As Double
Public Converted_Temp_Bin As String

'PC_TempSense_Offset
Public Const PC_TempSense_Offset = 1.5

Public Const Min_WS_Temp = 22
Public Const Max_WS_Temp = 28

'#########################################################      'Per test Paramenters:      #########################################################

Public Const RunAllCodes = False
Public Const RunVerifyMeas = True

'#####      Ext_Temp      #####
Public FixedTempreture As Double
Public Const FixedTempreture_DEFAULT = 25
Public Const FixedTempreture_WINSTEK = 25
Public Const FixedTempreture_ASE = 28

'#####      RefGen 0p8      #####
Public Const RefGen_0p8_MinCode = 1
Public Const RefGen_0p8_MaxCode = 15
Public Const RefGen_0p8_MeasVariance = 0.01 '0.1 chnaged for site 2 in Wiliot

Public Const RefGen_0p8_Target_Volt = 0.805
Public Const RefGen_0p8_Upper_Margin = 0.075

'#####      RefGen 0p6      #####
Public Const RefGen_0p6_MinCode = 2 'Limiting code of refgen06 to be >= code#2 (Tomer request for 4.1 release 29-3-2022)
Public Const RefGen_0p6_MaxCode = 31
Public Const RefGen_0p6_MeasVariance = 0.01 '0.1 chnaged for site 2 in Wiliot

Public Const RefGen_0p6_Target_Volt = 0.605
Public Const RefGen_0p6_Upper_Margin = 0.045

'#####      RefGen 0p74      #####
Public Const RefGen_0p74_MinCode = 1
Public Const RefGen_0p74_MaxCode = 31
Public Const RefGen_0p74_MeasVariance = 0.01

Public Const RefGen_0p74_Target_Volt = 0.745
Public Const RefGen_0p74_Upper_Margin = 0.045

'#####      Radio_Aux_Mode      #####
Public Const Radio_Aux_Mode_0p8V_LDO_Lowest = 0.795
Public Const Radio_Aux_Mode_0p8V_LDO_Highest = 0.91
Public Const Radio_Aux_Mode_0p74V_LDO_Lowest = 0.735
Public Const Radio_Aux_Mode_0p74V_LDO_Highest = 0.81
Public Const Radio_Aux_Mode_0p6V_LDO_Lowest = 0.55
Public Const Radio_Aux_Mode_0p6V_LDO_Highest = 0.67

'#####      Radio_LO_Init      #####
Public Const Radio_LO_Init_SYM_MODE_CURRENT_Lowest = 0
Public Const Radio_LO_Init_SYM_MODE_CURRENT_Highest = 0.0001

'#####      Radio_HPM_LO_Init      #####
Public Const Radio_HPM_LO_Init_SYM_MODE_CURRENT_Lowest = 0
Public Const Radio_HPM_LO_Init_SYM_MODE_CURRENT_Highest = 0.0001

'#####      Radio_LC_Aux_Mode      #####
Public Const Radio_LC_Aux_Mode_0p8V_LDO_Lowest = 0.795
Public Const Radio_LC_Aux_Mode_0p8V_LDO_Highest = 0.91
Public Const Radio_LC_Aux_Mode_0p74V_LDO_Lowest = 0.69
Public Const Radio_LC_Aux_Mode_0p74V_LDO_Highest = 0.79


'#####      Harvester limits      #####
Public Const Harvester_Voltage_Lowest = 0
Public Const Harvester_Voltage_Highest = 0.8



'#####      LO_VBP_RX      #####
Public Const LO_VBP_RX_MinCode = 5
Public Const LO_VBP_RX_MaxCode = 31
Public Const LO_VBP_RX_MeasVariance = 0.000003

Public Const LO_VBP_RX_Target_Curr = 0.000011
Public Const LO_VBP_RX_Upeer_Margin = 0.000001
Public Const LO_VBP_RX_Lower_Margin = 0.000004

'#####      LO_VBP_TX      #####
Public Const LO_VBP_TX_MinCode = 3
Public Const LO_VBP_TX_MaxCode = 31
Public Const LO_VBP_TX_MeasVariance = 0.000003

Public Const LO_VBP_TX_Target_Curr = 0.000022
Public Const LO_VBP_TX_Lower_Margin = 0.0000015

'#####      LO_Vref_RX      #####
Public Const LO_Vref_RX_MinCode = 0
Public Const LO_Vref_RX_MaxCode = 15
Public Const LO_Vref_RX_MeasVariance = 0.000003

Public Const LO_Vref_RX_Target_Curr = 0.000011
Public Const LO_Vref_RX_Lower_Margin = 0.000004


'#####      LO_Vref_TX      #####
Public Const LO_Vref_TX_MinCode = 4
Public Const LO_Vref_TX_MaxCode = 15
Public Const LO_Vref_TX_MeasVariance = 0.01 '0.1 chnaged for site 2 in Wiliot

Public Const LO_Vref_TX_Target_Volt = 0.23
Public Const LO_Vref_TX_Upper_Margin = 0.02


'#####      HPM_LO_VBP_RX      #####                    Eden: All Done. Sagi/Tomer -  now its closest to 20 uA from below and will take values in 15 uA to 20 uA, set variance we didnt have one
Public Const HPM_LO_VBP_RX_MinCode = 3
Public Const HPM_LO_VBP_RX_MaxCode = 15
Public Const HPM_LO_VBP_RX_MeasVariance = 0.000003

Public Const HPM_LO_VBP_RX_Target_Curr = 0.00002
Public Const HPM_LO_VBP_RX_Lower_Margin = 0.000005
Public Const HPM_LO_VBP_RX_Upper_Margin = 0.000002

'#####      HPM_LO_VBP_TX      #####
Public Const HPM_LO_VBP_TX_MinCode = 4
Public Const HPM_LO_VBP_TX_MaxCode = 4
Public Const HPM_LO_VBP_TX_MeasVariance = 0.000003

Public Const HPM_LO_VBP_TX_Target_Curr = 0.00006
Public Const HPM_LO_VBP_TX_Lower_Margin = 0.00005
Public Const HPM_LO_VBP_TX_Upper_Margin = 0.00002

'#####      HPM_LO_Vref_RX      #####
Public Const HPM_LO_Vref_RX_MinCode = 3
Public Const HPM_LO_Vref_RX_MaxCode = 31
Public Const HPM_LO_Vref_RX_MeasVariance = 0.000003    'we had from above and from below values

Public Const HPM_LO_Vref_RX_Target_Curr = 0.00002
Public Const HPM_LO_Vref_RX_Lower_Margin = 0.00005


'#####      HPM_LO_Vref_TX      #####
Public Const HPM_LO_Vref_TX_MinCode = 10
Public Const HPM_LO_Vref_TX_MaxCode = 31
Public Const HPM_LO_Vref_TX_MeasVariance = 0.01         'Eden: we didn't have variance only wider range to choose from

Public Const HPM_LO_Vref_TX_Target_Volt = 0.6
Public Const HPM_LO_Vref_TX_Lower_Margin = 0.05
Public Const HPM_LO_Vref_TX_Upper_Margin = 0.05

'#####      Temp_SENSE_DCDC      #####
Public Temp_Sens_DCDC_Lookup_Array
Public Const Temp_Sens_DCDC_MeasVariance = 0.025 '0.025

Public Const Temp_Sens_DCDC_Lowest_Target = 0.6
Public Const Temp_Sens_DCDC_Middle_Target = 0.62
Public Const Temp_Sens_DCDC_Highest_Target = 0.64

Public Const Temp_Sens_DCDC_VsupReg_Lowest_Target = 0.62
Public Const Temp_Sens_DCDC_VsupReg_Lower_Margin = 0.02

Public Const Temp_Sens_DCDC_VsupReg_Highest_Target = 0.8
Public Const Temp_Sens_DCDC_VsupReg_Upper_Margin = 0.005

'#####      VDD_0p85_Calib      #####
Public VDD_0p85_Lookup_Array
Public Const VDD_0p85_NVM_MeasVariance = 0.06
Public Const VDD_0p85_Active_MeasVariance = 0.06

Public Const VDD_0p85_Target_Volt = 0.89
Public Const VDD_0p85_Lower_Margin = 0.05
Public Const VDD_0p85_Upper_Margin = 0.03

Public Const VDD_0p85_NVM_Target_Volt = 0.77
Public Const VDD_0p85_NVM_Lower_Margin = 0.035

Public Const VDD_0p85_VerifyVDD_Lower_Target_Volt = 0.81
Public Const VDD_0p85_VerifyVDD_Upper_Target_Volt = 0.95

'#####      LC_AUX_IDAC      #####
Public LC_AUX_IDAC_Lookup_Array
Public Const LC_AUX_IDAC_MeasVariance = 0.000015 'Need to set according to Avg + 3/4 sigma (aroud 6.8-7.5 uA)

'Globals for zero current
Public Idd_Radio_Lo(59) As Double
Public Idd_Radio_HPM_Lo(59) As Double

'Arrays for fails
Public DCDC_Fails(Number_of_Sites - 1) As Boolean ' to fail the test at the end
Public WkUp_Fails(Number_of_Sites - 1) As Boolean ' to fail the etst at the end
Public Sites_Failes(Number_of_Sites - 1) As Boolean

'ChanelVeriables per site - defiend as globals.
Public chan_num_TMS(59) As Long
Public chan_num_TDO(59) As Long
Public chan_num_TDI(59) As Long
Public chan_num_GPIO4(59) As Long
Public chan_num_VddCap(59) As Long
Public chan_num_VddDbg(59) As Long
Public chan_num_Default As Long

'Global variables per TP
Public Const Flow_Version = 1075 '0x4.33
Public Const NVM_CRC = "00CEADF8"
Public Const SRID = 1027


Public Const LO_Freq_Lowest = 0
Public Const LO_Freq_Highest = 4096

